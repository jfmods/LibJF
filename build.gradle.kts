plugins {
    id("jfmod")
}

allprojects { group = "io.gitlab.jfronny.libjf" }
subprojects { version = rootProject.version }

jfMod {
    minecraftVersion = libs.versions.minecraft
    yarn(libs.versions.yarn.get())
    loaderVersion = libs.versions.fabric.loader
    fabricApiVersion = libs.versions.fabric.api

    modrinth {
        projectId = "libjf"
        optionalDependencies.add("fabric-api")
    }
    curseforge {
        projectId = "482600"
        optionalDependencies.add("fabric-api")
    }
}

base {
    archivesName = "libjf"
}

allprojects {
    if (!rootProject.jfMod.isMod(this)) return@allprojects

    dependencies {
        modLocalRuntime(libs.modmenu)
        modLocalRuntime("net.fabricmc.fabric-api:fabric-screen-api-v1")
        modLocalRuntime("net.fabricmc.fabric-api:fabric-key-binding-api-v1")

        modLocalRuntime("net.fabricmc.fabric-api:fabric-command-api-v2")
        modLocalRuntime("net.fabricmc.fabric-api:fabric-networking-api-v1")
        modLocalRuntime("net.fabricmc.fabric-api:fabric-resource-loader-v0")
        modLocalRuntime("net.fabricmc.fabric-api:fabric-lifecycle-events-v1")
        compileOnly(libs.bundles.commons)
    }
}
