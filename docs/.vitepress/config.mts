import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
    title: "LibJF",
    description: "Documentation for the LibJF library",
    themeConfig: {
        // https://vitepress.dev/reference/default-theme-config
        nav: [
            { text: 'Modrinth', link: 'https://modrinth.com/mod/libjf' },
            { text: 'CurseForge', link: 'https://www.curseforge.com/minecraft/mc-mods/libjf' }
        ],

        sidebar: [
            {
                text: 'Modules',
                collapsed: false,
                items: [
                    { text: 'libjf-base', link: '/libjf-base.md' },
                    {
                        text: 'Config',
                        collapsed: false,
                        link: '/config/index.md',
                        items: [
                            { text: 'libjf-config-core-v2', link: '/config/libjf-config-core-v2.md' },
                            { text: 'libjf-config-compiler-plugin-v2', link: '/config/libjf-config-compiler-plugin-v2.md' },
                            { text: 'libjf-config-commands', link: '/config/libjf-config-commands.md' },
                            { text: 'libjf-config-ui-tiny', link: '/config/libjf-config-ui-tiny.md' }
                        ]
                    },
                    { text: 'libjf-devutil', link: '/libjf-devutil.md' },
                    { text: 'libjf-data-v0', link: '/libjf-data-v0.md' },
                    { text: 'libjf-data-manipulation-v0', link: '/libjf-data-manipulation-v0.md' },
                    { text: 'libjf-translate-v1', link: '/libjf-translate-v1.md' },
                    { text: 'libjf-unsafe-v0', link: '/libjf-unsafe-v0.md' },
                    { text: 'libjf-web-v1', link: '/libjf-web-v1.md' }
                ]
            }
        ],

        socialLinks: [
            { icon: 'github', link: 'https://git.frohnmeyer-wds.de/JfMods/LibJF' }
        ],

        search: {
            provider: "local"
        },
    },
    outDir: '../public',
    base: '/JfMods/LibJF/',
    mpa: true,
    lang: 'en-US',
    sitemap: {
        hostname: 'https://pages.frohnmeyer-wds.de/JfMods/LibJF/'
    }
})
