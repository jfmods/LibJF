# libjf-config-compiler-plugin-v2
This annotation processor provides finds classes annotated with `@JfConfig` and produces the necessary code and registration for them.
Using this is the recommended way to use libjf-config.

::: warning
If `libjf-unsafe` is present, configs may be accessed early.

This can lead to issues if your config class accesses other classes that are not yet intended to be accessed.

Make sure to only access classes that are safe to access early (e.g. JDK classes).
:::

Please note that your `fabric.mod.json` must reference the generated class, not your original config.
If you plan on accessing your config before `onInitialize`, it is recommended that you call `.ensureInitialized()` on your
generated class from your config classes static initializer as seen in the example.

The code necessary for using this annotation processor is available [here](./)