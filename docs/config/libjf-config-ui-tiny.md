# libjf-config-ui-tiny
This module provides an automatically registered, TinyConfig-based UI for all mods using libjf-config.
Embedding this is recommended when developing client-side mods.
libjf-config-ui-tiny implements the config-core-provided `ConfigScreenFactory`, so you can `ConfigScreenFactory.getInstance().create(config, parent).get()` to obtain a screen for your config.