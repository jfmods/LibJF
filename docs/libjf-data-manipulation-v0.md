# libjf-data-manipulation-v0
libjf-data-manipulation-v0 provides code for modifying existing resources

### RecipeUtil
RecipeUtil provides to methods to end users:

- `removeRecipe` blocks minecraft from loading recipes using the specified tag
- ~~`removeRecipeFor` blocks minecraft from loading recipes producing the specified output. (Look at the log to find out which ones it blocks)~~ **removed for 1.19.4**

### UserResourceEvents
UserResourceEvents provides four events (CONTAINS, FIND_RESOURCE, OPEN, OPEN_ROOT) which get called every time
the corresponding method is called on and ResourcePack. They allow modifying the resulting data.
This is used in respackopts to manipulate resource loading.
To temporarily disable these hooks, call the "disable" function, which invokes the lambda it is passed and disables the hooks
while it is running

### Examples
```java
@Override
public void onInitialize() {
    // This should prevent resource packs from doing anything if my hooks are working and
    UserResourceEvents.OPEN.register((type, id, previous, pack) -> {
        if (pack instanceof DirectoryResourcePack) {
            LibJf.LOGGER.info(pack.getName() + " opened " + type.name() + "/" + id.toString());
        }
        return previous.get();
    });
    UserResourceEvents.CONTAINS.register((type, id, previous, pack) -> {
        if (pack instanceof DirectoryResourcePack) {
            return false;
        }
        return previous.get();
    });
    RecipeUtil.removeRecipeFor(Items.DIAMOND_SWORD);
}
```