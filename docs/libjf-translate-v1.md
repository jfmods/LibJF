# libjf-translate-v1
libjf-translate-v1 provides a utility class for translating strings through user-configurable services.

To use this, first obtain a TranslateService instance. You can use `TranslateService.getConfigured()` to do so.
Please be aware that due to the nature of java generics, using var instead of a specific type for instances is recommended.
You can also directly access implementations, however, this is not recommended and is not subject to the API stability promise.
The TranslateService interface exposes all relevant functionality.
