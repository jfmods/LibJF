import com.github.jengelman.gradle.plugins.shadow.transformers.ServiceFileTransformer
import com.github.jengelman.gradle.plugins.shadow.transformers.TransformerContext
import org.codehaus.plexus.util.IOUtil
import java.io.ByteArrayOutputStream

plugins {
    id("jfmod.module")
}

base {
    archivesName = "libjf-base"
}

dependencies {
    include(modImplementation("net.fabricmc.fabric-api:fabric-lifecycle-events-v1")!!)
    shadow(libs.bundles.commons)
}

// workaround to ensure our custom System.LoggerFinder is initialized early
// ideally, we would use the configuration API, but that seems to cause a class loading issue with fabric loader
tasks.shadowJar {
    val path = "META-INF/services/java.lang.System\$LoggerFinder"
    val field = ServiceFileTransformer::class.java.getDeclaredField("serviceEntries").apply { isAccessible = true }
    exclude(path)
    transform(object: ServiceFileTransformer() {
        private val serviceEntries get() = field.get(this) as MutableMap<String, Any>
        override fun transform(context: TransformerContext?) {
            super.transform(context)
            (serviceEntries[path] as ByteArrayOutputStream).run {
                reset()
                file("src/main/resources/$path").inputStream().use { IOUtil.copy(it, this) }
            }
        }
    })
}