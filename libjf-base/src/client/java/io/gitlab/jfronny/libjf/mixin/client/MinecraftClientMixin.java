package io.gitlab.jfronny.libjf.mixin.client;

import io.gitlab.jfronny.libjf.coprocess.CoProcessManager;
import net.minecraft.client.MinecraftClient;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(MinecraftClient.class)
public class MinecraftClientMixin {
    @Inject(method = "stop", at = @At(value = "INVOKE", target = "Ljava/lang/System;exit(I)V"))
    private void onStop(CallbackInfo ci) {
        CoProcessManager.stop();
    }

    @Inject(method = "stop", at = @At(value = "TAIL"))
    private void onStopTail(CallbackInfo ci) {
        CoProcessManager.stop();
    }

    @Inject(method = "printCrashReport(Lnet/minecraft/client/MinecraftClient;Ljava/io/File;Lnet/minecraft/util/crash/CrashReport;)V", at = @At(value = "INVOKE", target = "Ljava/lang/System;exit(I)V"))
    private static void onCrashReport(MinecraftClient client, java.io.File file, net.minecraft.util.crash.CrashReport report, CallbackInfo ci) {
        CoProcessManager.stop();
    }
}
