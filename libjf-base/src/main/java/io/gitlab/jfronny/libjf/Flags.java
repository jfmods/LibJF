package io.gitlab.jfronny.libjf;

import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;
import net.fabricmc.loader.api.metadata.CustomValue;
import org.jetbrains.annotations.ApiStatus;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.function.BiFunction;

@ApiStatus.Internal
public class Flags {
    public static Set<StringFlag> getStringFlags(String name) {
        return getFlags(name,
                (source, value) -> new StringFlag(source, value),
                (source, value) -> new StringFlag(source, value.getAsString()));
    }

    public static Set<BooleanFlag> getBoolFlags(String name) {
        return getFlags(name,
                (source, value) -> new BooleanFlag(source, true),
                (source, value) -> new BooleanFlag(source, value.getAsBoolean()));
    }

    private static <T> Set<T> getFlags(String name, BiFunction<String, String, T> makeFromProp, BiFunction<String, CustomValue, T> makeFromFmj) {
        Set<T> flags = new LinkedHashSet<>();
        String propName = "libjf." + name;
        if (System.getProperty(propName) != null)
            flags.add(makeFromProp.apply("System Property", System.getProperty(propName)));
        for (ModContainer mod : FabricLoader.getInstance().getAllMods()) {
            if (!mod.getMetadata().containsCustomValue("libjf")) continue;
            CustomValue.CvObject co = mod.getMetadata().getCustomValue("libjf").getAsObject();
            if (!co.containsKey(name)) continue;
            flags.add(makeFromFmj.apply("Mod: " + mod.getMetadata().getId(), co.get(name)));
        }
        return flags;
    }

    public record StringFlag(String source, String value) {
    }

    public record BooleanFlag(String source, boolean value) {
    }
}
