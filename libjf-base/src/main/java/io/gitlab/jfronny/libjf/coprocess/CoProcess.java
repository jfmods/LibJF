package io.gitlab.jfronny.libjf.coprocess;

public interface CoProcess {
    void start();
    void stop();
}
