package io.gitlab.jfronny.libjf.coprocess;

import io.gitlab.jfronny.libjf.LibJf;
import net.fabricmc.api.EnvType;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import net.fabricmc.loader.api.FabricLoader;

import java.io.Closeable;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class CoProcessManager {
    private static final List<CoProcess> coProcesses = new ArrayList<>();

    static {
        coProcesses.addAll(FabricLoader.getInstance().getEntrypoints(LibJf.MOD_ID + ":coprocess", CoProcess.class));
        if (FabricLoader.getInstance().getEnvironmentType() == EnvType.SERVER) ServerLifecycleEvents.SERVER_STOPPED.register(server -> CoProcessManager.stop());
        Runtime.getRuntime().addShutdownHook(new Thread(CoProcessManager::stop));
    }

    private static final AtomicBoolean started = new AtomicBoolean(false);

    public static void start() {
        if (started.getAndSet(true)) return;
        for (CoProcess coProcess : coProcesses) {
            coProcess.start();
        }
    }

    public static void stop() {
        if (!started.getAndSet(false)) return;
        for (Iterator<CoProcess> iter = coProcesses.iterator(); iter.hasNext(); ) {
            CoProcess coProcess = iter.next();
            coProcess.stop();
            if (coProcess instanceof Closeable cl) {
                try {
                    cl.close();
                } catch (IOException e) {
                    LibJf.LOGGER.error("Could not close co-process", e);
                }
            }
            iter.remove();
        }
    }
}
