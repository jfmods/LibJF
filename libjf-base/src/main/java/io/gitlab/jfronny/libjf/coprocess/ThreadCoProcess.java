package io.gitlab.jfronny.libjf.coprocess;

public abstract class ThreadCoProcess implements CoProcess, Runnable {
    private final boolean isDaemon;
    private Thread th = null;
    private boolean closed = true;

    protected ThreadCoProcess() {
        this(false);
    }

    protected ThreadCoProcess(boolean isDaemon) {
        this.isDaemon = isDaemon;
    }

    @Override
    public void start() {
        if (th != null) stop();
        closed = false;
        th = new Thread(this, getClass().getSimpleName());
        th.setDaemon(isDaemon);
        th.start();
    }

    @Override
    public void stop() {
        if (th == null) return;
        closed = true;
        try {
            th.join();
        } catch (InterruptedException e) {
            throw new RuntimeException("Could not join co-process thread", e);
        }
        th = null;
    }

    @Override
    public void run() {
        while (!closed) {
            executeIteration();
        }
    }

    public abstract void executeIteration();
}
