package io.gitlab.jfronny.libjf.log;

import io.gitlab.jfronny.commons.logger.HotswapLoggerFinder;
import io.gitlab.jfronny.commons.logger.LeveledLoggerFinder;
import io.gitlab.jfronny.commons.logger.SystemLoggerPlus;
import org.jetbrains.annotations.Nullable;

public class EarlyLoggerSetup extends LeveledLoggerFinder {
    static {
        // When a logger is used before the game is initialized, SLF4J is not yet available.
        // To support that, this implementation which redirects to Fabric Loader's internal logging abstraction is used instead.
        // After the game is initialized, something calls LibJF.setup (usually preEntry), which replaces this factory with a SLF4J-based one.
        HotswapLoggerFinder.updateAllStrategies((name, module, level) -> new LoaderPlatformLogger(name));
    }

    private final LeveledLoggerFinder delegate = new HotswapLoggerFinder();

    @Override
    public SystemLoggerPlus getLogger(String name, Module module, @Nullable System.Logger.Level level) {
        return delegate.getLogger(name, module, level);
    }
}
