package io.gitlab.jfronny.libjf.log;

import io.gitlab.jfronny.commons.logger.SystemLoggerPlus;

import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;

/**
 * Based on SLF4JBridgeHandler, modified for LibJF.
 */
public class JULBridge extends Handler {
    public static void install() {
        var rootLogger = getRootLogger();
        rootLogger.addHandler(new JULBridge());
        boolean bridgeFound = false;
        for (Handler handler : rootLogger.getHandlers()) {
            if (handler instanceof JULBridge) {
                bridgeFound = true;
                continue;
            }
            rootLogger.removeHandler(handler);
        }
        if (!bridgeFound) {
            // Someone is wrapping our handlers. No matter, just add it again.
            rootLogger.addHandler(new JULBridge());
        }
    }

    private static java.util.logging.Logger getRootLogger() {
        return LogManager.getLogManager().getLogger("");
    }

    private JULBridge() {
    }

    @Override
    public void flush() {
        // Do nothing
    }

    @Override
    public void close() {
        // Do nothing
    }

    private final Module module = JULBridge.class.getClassLoader().getUnnamedModule();
    private final EarlyLoggerSetup loggerFinder = new EarlyLoggerSetup();

    private SystemLoggerPlus getLoggerFor(LogRecord record) {
        return loggerFinder.getLogger(Objects.requireNonNullElse(record.getLoggerName(), ""), module);
    }

    private static System.Logger.Level getLevel(LogRecord record) {
        System.Logger.Level level;
        if (record.getLevel().equals(Level.OFF)) level = System.Logger.Level.OFF;
        else if (record.getLevel().equals(Level.ALL)) level = System.Logger.Level.ALL;
        else if (record.getLevel().intValue() >= Level.SEVERE.intValue()) {
            level = System.Logger.Level.ERROR;
        } else if (record.getLevel().intValue() >= Level.WARNING.intValue()) {
            level = System.Logger.Level.WARNING;
        } else if (record.getLevel().intValue() >= Level.INFO.intValue()) {
            level = System.Logger.Level.INFO;
        } else {
            level = System.Logger.Level.DEBUG;
        }
        return level;
    }

    @Override
    public void publish(LogRecord record) {
        if (record == null) return;
        SystemLoggerPlus logger = getLoggerFor(record);
        System.Logger.Level level = getLevel(record);
        String message = record.getMessage();
        ResourceBundle bundle = null;
        if (message == null) message = "";
        else bundle = record.getResourceBundle();
        Object[] params = record.getParameters();
        Throwable thrown = record.getThrown();
        if (thrown == null) logger.log(level, bundle, message, params);
        else logger.log(level, bundle, message, thrown, params);
    }
}
