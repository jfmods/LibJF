package io.gitlab.jfronny.libjf.log;

import io.gitlab.jfronny.commons.logger.CompactLogger;
import net.fabricmc.loader.impl.util.log.Log;
import net.fabricmc.loader.impl.util.log.LogCategory;
import net.fabricmc.loader.impl.util.log.LogLevel;

public class LoaderPlatformLogger implements CompactLogger {
    private final LogCategory category;

    public LoaderPlatformLogger(String context, String... names) {
        this.category = LogCategory.createCustom(context, names);
    }

    @Override
    public String getName() {
        return category.name;
    }

    @Override
    public Level getLevel() {
        for (Level value : Level.values()) {
            if (value == Level.ALL || value == Level.OFF) continue;
            if (isLoggable(value)) return value;
        }
        return Level.INFO; // should not happen, but if it does, INFO is a reasonable default
    }

    @Override
    public void log(Level level, String message) {
        Log.log(jplLevelToFabricLevel(level), category, message);
    }

    @Override
    public void log(Level level, String message, Throwable throwable) {
        Log.log(jplLevelToFabricLevel(level), category, message, throwable);
    }

    @Override
    public boolean isLoggable(Level level) {
        if (level == Level.ALL) return true;
        if (level == Level.OFF) return false;
        return Log.shouldLog(jplLevelToFabricLevel(level), category);
    }

    private LogLevel jplLevelToFabricLevel(Level jplLevel) {
        return switch (jplLevel) {
            case TRACE -> LogLevel.TRACE;
            case DEBUG -> LogLevel.DEBUG;
            case INFO -> LogLevel.INFO;
            case WARNING -> LogLevel.WARN;
            case ERROR -> LogLevel.ERROR;
            // should not happen, but if it does, INFO is a reasonable default
            default -> LogLevel.INFO;
        };
    }
}
