package io.gitlab.jfronny.libjf.mixin.server;

import io.gitlab.jfronny.libjf.coprocess.CoProcessManager;
import net.minecraft.server.Main;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(Main.class)
public class MainMixin {
    @Unique private static boolean libjf$started = false;

    @Inject(method = "main", at = @At(value = "INVOKE_ASSIGN", target = "Lnet/minecraft/server/MinecraftServer;startServer(Ljava/util/function/Function;)Lnet/minecraft/server/MinecraftServer;"))
    private static void onStartServer(String[] args, CallbackInfo ci) {
        libjf$started = true;
    }

    @Inject(method = "main", at = @At("RETURN"))
    private static void onMain(String[] args, CallbackInfo ci) {
        if (!libjf$started) CoProcessManager.stop();
    }
}
