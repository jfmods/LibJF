package io.gitlab.jfronny.libjf.serialize;

import io.gitlab.jfronny.commons.serialize.emulated.DataElement;
import net.fabricmc.loader.api.metadata.CustomValue;

import java.util.Map;

public class FabricLoaderDataMapper {
    public static DataElement toGson(CustomValue customValue) {
        if (customValue == null) return null;
        return switch (customValue.getType()) {
            case OBJECT -> {
                DataElement.Object jo = new DataElement.Object();
                for (Map.Entry<String, CustomValue> value : customValue.getAsObject())
                    jo.members().put(value.getKey(), toGson(value.getValue()));
                yield jo;
            }
            case ARRAY -> {
                DataElement.Array jo = new DataElement.Array();
                for (CustomValue value : customValue.getAsArray())
                    jo.elements().add(toGson(value));
                yield jo;
            }
            case STRING -> new DataElement.Primitive.String(customValue.getAsString());
            case NUMBER -> new DataElement.Primitive.Number(customValue.getAsNumber());
            case BOOLEAN -> new DataElement.Primitive.Boolean(customValue.getAsBoolean());
            case NULL -> new DataElement.Null();
        };
    }
}
