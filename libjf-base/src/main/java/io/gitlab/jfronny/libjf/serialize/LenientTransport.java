package io.gitlab.jfronny.libjf.serialize;

import io.gitlab.jfronny.commons.serialize.json.JsonReader;
import io.gitlab.jfronny.commons.serialize.json.JsonTransport;
import io.gitlab.jfronny.commons.serialize.json.JsonWriter;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

public class LenientTransport extends JsonTransport {
    @Override
    public JsonWriter createWriter(Writer target) throws IOException {
        return SerializationMode.asConfig(super.createWriter(target));
    }

    @Override
    public JsonReader createReader(Reader source) {
        return SerializationMode.asConfig(super.createReader(source));
    }
}
