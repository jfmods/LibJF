package io.gitlab.jfronny.libjf.serialize;

import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.json.JsonWriter;

public class SerializationMode {
    public static <TEx extends Exception, Reader extends SerializeReader<TEx, Reader>> Reader asConfig(Reader reader) {
        reader.setLenient(true)
                .setSerializeSpecialFloatingPointValues(true);
        return reader;
    }

    public static <TEx extends Exception, Writer extends SerializeWriter<TEx, Writer>> Writer asConfig(Writer writer) {
        writer.setLenient(true)
                .setSerializeSpecialFloatingPointValues(true)
                .setSerializeNulls(true);
        if (writer instanceof JsonWriter jw) {
            jw.setIndent("    ")
                    .setNewline("\n")
                    .setOmitQuotes(true)
                    .setCommentUnexpectedNames(true);
        }
        return writer;
    }
}
