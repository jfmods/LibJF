import io.gitlab.jfronny.scripts.deployDebug
import io.gitlab.jfronny.scripts.deployRelease

plugins {
    `java-platform`
    id("jf.maven-publish")
}

publishing {
    publications {
        register("mavenJava", MavenPublication::class) {
            from(components["javaPlatform"])
        }
    }
}
tasks.publish { dependsOn(tasks.build) }
tasks.deployDebug.dependsOn(tasks.publish)
tasks.deployRelease.dependsOn(tasks.deployDebug)

tasks.withType(GenerateModuleMetadata::class) {
    enabled = true
}

dependencies {
    constraints {
        for (proj in rootProject.allprojects) {
            if (proj == project) {
                continue
            }
            if (proj.name == "libjf-catalog") {
                continue
            }

            api(project(proj.path))
        }
    }
}