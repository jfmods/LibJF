import io.gitlab.jfronny.scripts.deployDebug
import io.gitlab.jfronny.scripts.deployRelease

plugins {
    `version-catalog`
    id("jf.maven-publish")
}

publishing {
    publications {
        register("mavenJava", MavenPublication::class) {
            from(components["versionCatalog"])
        }
    }
}
tasks.publish { dependsOn(tasks.build) }
tasks.deployDebug.dependsOn(tasks.publish)
tasks.deployRelease.dependsOn(tasks.deployDebug)

tasks.withType(GenerateModuleMetadata::class) {
    enabled = true
}

tasks.register("configureCatalog") {
    doFirst {
        doConfigureCatalog()
    }
}
tasks.named("generateCatalogAsToml") {
    dependsOn("configureCatalog")
}

fun doConfigureCatalog() {
    for (proj in rootProject.allprojects) {
        if (proj == project) {
            continue
        }

        var catalogName = proj.name
        catalogName = when (catalogName) {
            "libjf-base" -> "base"
            "libjf-bom" -> "bom"
            "libjf" -> "libjf"
            else -> catalogName.substring("libjf-".length)
        }

        catalog {
            versionCatalog {
                library(catalogName, "$group:${proj.name}:${proj.version}")
            }
        }
    }
}