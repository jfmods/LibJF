import io.gitlab.jfronny.scripts.*

plugins {
    id("jfmod.module")
}

base {
    archivesName = "libjf-config-commands"
}

dependencies {
    api(devProject(":libjf-base"))
    api(devProject(":libjf-config-core-v2"))
    include(modImplementation("net.fabricmc.fabric-api:fabric-command-api-v2")!!)
}
