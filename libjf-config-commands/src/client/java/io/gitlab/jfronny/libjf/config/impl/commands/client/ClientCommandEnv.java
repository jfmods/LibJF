package io.gitlab.jfronny.libjf.config.impl.commands.client;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.ArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.builder.RequiredArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import io.gitlab.jfronny.libjf.config.impl.commands.CommandEnv;
import net.fabricmc.fabric.api.client.command.v2.ClientCommandManager;
import net.fabricmc.fabric.api.client.command.v2.FabricClientCommandSource;

public record ClientCommandEnv(CommandDispatcher<FabricClientCommandSource> dispatcher) implements CommandEnv<FabricClientCommandSource> {
    @Override
    public LiteralArgumentBuilder<FabricClientCommandSource> literal(String name) {
        return ClientCommandManager.literal(name);
    }

    @Override
    public <T> RequiredArgumentBuilder<FabricClientCommandSource, T> argument(String name, ArgumentType<T> type) {
        return ClientCommandManager.argument(name, type);
    }

    @Override
    public boolean isOperator(FabricClientCommandSource source) {
        return true;
    }

    @Override
    public void sendFeedback(CommandContext<FabricClientCommandSource> context, net.minecraft.text.Text text, boolean broadcast) {
        context.getSource().sendFeedback(text);
    }

    @Override
    public void sendError(CommandContext<FabricClientCommandSource> context, net.minecraft.text.Text text) {
        context.getSource().sendError(text);
    }
}
