package io.gitlab.jfronny.libjf.config.impl.commands.client;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.config.impl.commands.JfConfigCommand;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.command.v2.ClientCommandRegistrationCallback;

public class ClientConfigCommand implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        ClientCommandRegistrationCallback.EVENT.register((dispatcher, registryAccess) -> JfConfigCommand.register(new ClientCommandEnv(dispatcher), LibJf.MOD_ID + "c"));
    }
}
