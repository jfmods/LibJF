package io.gitlab.jfronny.libjf.config.impl.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.ArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.builder.RequiredArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import net.minecraft.command.CommandSource;
import net.minecraft.text.Text;

public interface CommandEnv<S extends CommandSource> {
    LiteralArgumentBuilder<S> literal(String name);
    <T> RequiredArgumentBuilder<S, T> argument(String name, ArgumentType<T> type);

    boolean isOperator(S source);

    CommandDispatcher<S> dispatcher();
    void sendFeedback(CommandContext<S> context, Text text, boolean broadcast);
    void sendError(CommandContext<S> context, Text text);
}
