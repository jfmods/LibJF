package io.gitlab.jfronny.libjf.config.impl.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.arguments.*;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.DynamicCommandExceptionType;
import io.gitlab.jfronny.commons.throwable.ThrowingRunnable;
import io.gitlab.jfronny.commons.throwable.ThrowingSupplier;
import io.gitlab.jfronny.libjf.config.api.v2.*;
import io.gitlab.jfronny.libjf.config.api.v2.type.Type;
import net.minecraft.command.CommandSource;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;

import java.util.function.Consumer;
import java.util.function.Function;

public class JfConfigCommand {
    private static final String MOD_ID = "libjf-config-commands";

    private static MutableText text(String text) {
        return Text.literal("[" + MOD_ID + "] " + text);
    }

    public static <S extends CommandSource> void register(CommandEnv<S> env, String name) {
        LiteralArgumentBuilder<S> c_root = env.literal(name);
        LiteralArgumentBuilder<S> c_config = env.literal("config")
                .requires(env::isOperator)
                .executes(context -> {
                    MutableText text = text("Loaded configs for:");
                    ConfigHolder.getInstance().getRegistered().forEach((s, config) -> text.append("\n- " + s));
                    env.sendFeedback(context, text, false);
                    return Command.SINGLE_SUCCESS;
                });
        LiteralArgumentBuilder<S> c_reload = env.literal("reload").executes(context -> {
            ConfigHolder.getInstance().getRegistered().forEach((mod, config) -> config.load());
            env.sendFeedback(context, text("Reloaded configs"), true);
            return Command.SINGLE_SUCCESS;
        });
        LiteralArgumentBuilder<S> c_reset = env.literal("reset").executes(context -> {
            env.sendError(context, text("Please specify a config to reset"));
            return Command.SINGLE_SUCCESS;
        });
        ConfigHolder.getInstance().getRegistered().forEach((id, config) -> {
            c_reload.then(env.literal(id).executes(context -> {
                config.load();
                env.sendFeedback(context, text("Reloaded config for " + id), true);
                return Command.SINGLE_SUCCESS;
            }));
            registerEntries(env, config, Naming.get(id), c_config, c_reset, cns -> {
                LiteralArgumentBuilder<S> c_instance = env.literal(id);
                cns.accept(c_instance);
                return c_instance;
            });
        });
        env.dispatcher().register(c_root.then(c_config.then(c_reload).then(c_reset)));
    }

    private static <S extends CommandSource> void registerEntries(
            CommandEnv<S> env,
            ConfigCategory config,
            Naming naming,
            LiteralArgumentBuilder<S> c_config,
            LiteralArgumentBuilder<S> c_reset,
            Function<Consumer<LiteralArgumentBuilder<S>>, LiteralArgumentBuilder<S>> pathGen
    ) {
        c_config.then(pathGen.apply(cns -> {
            cns.executes(context -> {
                env.sendFeedback(context, text("").append(naming.name()).append(" is a category"), false);
                return Command.SINGLE_SUCCESS;
            });
            for (EntryInfo<?> entry : config.getEntries()) {
                registerEntry(env, config, naming.entry(entry.getName()), cns, entry);
            }
        }));
        c_reset.then(pathGen.apply(cns -> {
            cns.executes(context -> {
                config.reset();
                env.sendFeedback(context, text("Reset config for ").append(naming.name()), true);
                return Command.SINGLE_SUCCESS;
            });
            config.getPresets().forEach((id2, preset) -> {
                cns.then(env.literal(id2).executes(context -> {
                    preset.run();
                    env.sendFeedback(context, text("Loaded preset " + id2 + " for ").append(naming.name()), true);
                    return Command.SINGLE_SUCCESS;
                }));
            });
        }));
        config.getCategories().forEach((id2, cfg) -> {
            registerEntries(env, cfg, naming.category(id2), c_config, c_reset, cns -> {
                return pathGen.apply(cns1 -> {
                    LiteralArgumentBuilder<S> c_instance2 = env.literal(id2);
                    cns.accept(c_instance2);
                    cns1.then(c_instance2);
                });
            });
        });
    }

    private static final DynamicCommandExceptionType eType = new DynamicCommandExceptionType(o -> {
        if (o instanceof Throwable throwable) {
            return Text.literal("Could not execute command: " + throwable.getMessage());
        } else return Text.literal("Could not execute command");
    });

    private static <T, S extends CommandSource> void registerEntry(
            CommandEnv<S> env,
            ConfigCategory config,
            Naming.Entry naming,
            LiteralArgumentBuilder<S> cns,
            EntryInfo<T> entry
    ) {
        if (!entry.supportsRepresentation()) return;
        LiteralArgumentBuilder<S> c_entry = env.literal(entry.getName()).executes(context -> {
            Text visualized = visualizeOption(entry, naming, tryRun(entry::getValue));
            env.sendFeedback(context, text("The value of ").append(naming.name()).append(" is ").append(visualized), false);
            return Command.SINGLE_SUCCESS;
        });
        ArgumentType<?> type = getType(entry);
        if (type != null) {
            c_entry.then(env.argument("value", type).executes(context -> {
                @SuppressWarnings("unchecked") T value = context.getArgument("value", (Class<T>) entry.getValueType().asClass());
                tryRun(() -> {
                    entry.setValue(value);
                    config.getRoot().write();
                });
                env.sendFeedback(context, text("Set ").append(naming.name()).append(" to ")
                        .append(visualizeOption(entry, naming, value)), true);
                return Command.SINGLE_SUCCESS;
            }));
        } else if (entry.getValueType().isEnum()) {
            for (T value : ((Type.TEnum<T>)entry.getValueType()).options()) {
                c_entry.then(env.literal(value.toString()).executes(context -> {
                    tryRun(() -> {
                        entry.setValue(value);
                        config.getRoot().write();
                    });
                    env.sendFeedback(context, text("Set ").append(naming.name()).append(" to ")
                            .append(visualizeOption(entry, naming, value)), true);
                    return Command.SINGLE_SUCCESS;
                }));
            }
        }
        cns.then(c_entry);
    }

    private static <T> Text visualizeOption(EntryInfo<T> entry, Naming.Entry naming, T value) {
        Type type = entry.getValueType();
        if (value == null) return naming.nullValue();
        else if (type.isEnum()) return naming.enumValue(type, value);
        else if (type.isBool()) return naming.boolValue((Boolean) value);
        else return Text.literal(String.valueOf(value));
    }

    private static <T> ArgumentType<?> getType(EntryInfo<T> info) {
        Type type = info.getValueType();
        if (type.isInt()) return IntegerArgumentType.integer((int) info.getMinValue(), (int) info.getMaxValue());
        else if (type.isLong()) return LongArgumentType.longArg((long) info.getMinValue(), (long) info.getMaxValue());
        else if (type.isFloat()) return FloatArgumentType.floatArg((float) info.getMinValue(), (float) info.getMaxValue());
        else if (type.isDouble()) return DoubleArgumentType.doubleArg(info.getMinValue(), info.getMaxValue());
        else if (type.isString()) return StringArgumentType.greedyString();
        else if (type.isBool()) return BoolArgumentType.bool();
        else return null;
    }

    private static <T> T tryRun(ThrowingSupplier<T, ?> supplier) throws CommandSyntaxException {
        return supplier.orThrow(eType::create).get();
    }

    private static void tryRun(ThrowingRunnable<?> supplier) throws CommandSyntaxException {
        supplier.orThrow(eType::create).run();
    }
}
