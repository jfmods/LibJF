package io.gitlab.jfronny.libjf.config.impl.commands.server;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.ArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.builder.RequiredArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import io.gitlab.jfronny.libjf.config.impl.commands.CommandEnv;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;

public record ServerCommandEnv(CommandDispatcher<ServerCommandSource> dispatcher) implements CommandEnv<ServerCommandSource> {
    @Override
    public LiteralArgumentBuilder<ServerCommandSource> literal(String name) {
        return CommandManager.literal(name);
    }

    @Override
    public <T> RequiredArgumentBuilder<ServerCommandSource, T> argument(String name, ArgumentType<T> type) {
        return CommandManager.argument(name, type);
    }

    @Override
    public boolean isOperator(ServerCommandSource source) {
        return source.hasPermissionLevel(4);
    }

    @Override
    public void sendFeedback(CommandContext<ServerCommandSource> context, Text text, boolean broadcast) {
        context.getSource().sendFeedback(() -> text, broadcast);
    }

    @Override
    public void sendError(CommandContext<ServerCommandSource> context, Text text) {
        context.getSource().sendError(text);
    }
}
