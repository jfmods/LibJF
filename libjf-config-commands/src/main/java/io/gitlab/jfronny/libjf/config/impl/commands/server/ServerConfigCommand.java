package io.gitlab.jfronny.libjf.config.impl.commands.server;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.config.impl.commands.JfConfigCommand;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.command.v2.CommandRegistrationCallback;

public class ServerConfigCommand implements ModInitializer {
    @Override
    public void onInitialize() {
        CommandRegistrationCallback.EVENT.register((dispatcher, registryAccess, environment) -> JfConfigCommand.register(new ServerCommandEnv(dispatcher), LibJf.MOD_ID));
    }
}
