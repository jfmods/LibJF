import io.gitlab.jfronny.scripts.*
import javax.lang.model.element.Modifier.*

plugins {
    id("jf.java")
    id("jf.maven-publish")
}

repositories {
    mavenCentral()
    maven("https://maven.frohnmeyer-wds.de/artifacts")
    maven("https://maven.fabricmc.net/")
}

dependencies {
    implementation(libs.commons.serialize.generator.core)
    implementation(devProject(":libjf-config-core-v2"))
    implementation(libs.annotations)
    implementation(libs.commons)
    implementation(libs.commons.serialize.databind)
    implementation(libs.commons.serialize.json)
    implementation(libs.javapoet)
    testAnnotationProcessor(sourceSets.main.get().output)
    configurations.testAnnotationProcessor.get().extendsFrom(configurations.implementation.get())
}

tasks.publish.get().dependsOn(tasks.build.get())
rootProject.tasks.deployDebug.dependsOn(tasks.publish.get())

sourceSets {
    main {
        generate(project) {
            `class`("io.gitlab.jfronny.libjf.config.plugin", "BuildMetadata") {
                modifiers(PUBLIC)
                field("IS_RELEASE", project.hasProperty("release"), PUBLIC, STATIC, FINAL)
            }
        }
    }
}

tasks.named<JavaCompile>("compileTestJava") {
    options.compilerArgs.add("-AmodId=example-mod")
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            from(components["java"])
        }
    }
}