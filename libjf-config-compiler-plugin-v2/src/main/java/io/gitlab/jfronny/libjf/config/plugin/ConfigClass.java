package io.gitlab.jfronny.libjf.config.plugin;

import com.palantir.javapoet.ClassName;
import com.palantir.javapoet.TypeName;

import javax.lang.model.element.TypeElement;

public record ConfigClass(
        TypeElement classElement,
        ClassName className,
        TypeName typeName,
        ClassName generatedClassName,
        String[] referencedConfigs,
        TypeName tweakerName
) {
    public static ConfigClass of(TypeElement element, String[] referencedConfigs, TypeName tweakerName, boolean hasManifold) {
        ClassName className = ClassName.get(element);
        String pkg = hasManifold ? "gsoncompile.extensions." + className.packageName() + "." + className.simpleNames().getFirst() : className.packageName();
        ClassName generatedClassName = ClassName.get(pkg, "JFC_" + className.simpleNames().getFirst(), className.simpleNames().subList(1, className.simpleNames().size()).toArray(String[]::new));
        return new ConfigClass(element, ClassName.get(element), TypeName.get(element.asType()), generatedClassName, referencedConfigs, tweakerName);
    }

    public boolean hasTweaker() {
        return ConfigProcessor.isUsable(tweakerName);
    }
}
