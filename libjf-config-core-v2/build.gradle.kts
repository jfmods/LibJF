import io.gitlab.jfronny.scripts.*

plugins {
    id("jfmod.module")
}

base {
    archivesName = "libjf-config-core-v2"
}

dependencies {
    api(devProject(":libjf-base"))
    modCompileOnly(libs.modmenu)
}
