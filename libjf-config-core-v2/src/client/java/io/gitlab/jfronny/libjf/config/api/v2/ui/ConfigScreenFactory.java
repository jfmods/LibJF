package io.gitlab.jfronny.libjf.config.api.v2.ui;

import io.gitlab.jfronny.libjf.config.api.v2.ConfigHolder;
import io.gitlab.jfronny.libjf.config.api.v2.ConfigInstance;
import io.gitlab.jfronny.libjf.config.api.v2.Naming;
import io.gitlab.jfronny.libjf.config.api.v2.dsl.DSL;
import io.gitlab.jfronny.libjf.config.impl.ui.ConfigScreenFactoryDiscovery;
import io.gitlab.jfronny.libjf.config.impl.ui.OverviewNaming;
import net.minecraft.client.gui.screen.Screen;

import java.util.Objects;

public interface ConfigScreenFactory<S extends Screen, B extends ConfigScreenFactory.Built<S>> {
    static ConfigScreenFactory<?, ?> getInstance() {
        return ConfigScreenFactoryDiscovery.getConfigured2();
    }

    B create(ConfigInstance config, Naming naming, Screen parent);

    int getPriority();

    default Screen createOverview(Screen parent) {
        return createOverview(ConfigHolder.getInstance(), parent);
    }

    default Screen createOverview(ConfigHolder holder, Screen parent) {
        return create(DSL.create("overview").config(builder -> {
            Objects.requireNonNull(holder).getRegistered().forEach((n, ci) -> {
                builder.referenceConfig(ci);
            });
            return builder;
        }), new OverviewNaming(), parent).get();
    }

    interface Built<S extends Screen> {
        S get();

        void onSave(Runnable action);
    }
}
