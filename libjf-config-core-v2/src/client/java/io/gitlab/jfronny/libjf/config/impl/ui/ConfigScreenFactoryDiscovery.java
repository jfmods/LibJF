package io.gitlab.jfronny.libjf.config.impl.ui;

import io.gitlab.jfronny.libjf.config.api.v2.ui.ConfigScreenFactory;
import net.fabricmc.loader.api.FabricLoader;

import java.util.Comparator;
import java.util.List;

public class ConfigScreenFactoryDiscovery {
    private static ConfigScreenFactory<?, ?> discovered2 = null;

    public static io.gitlab.jfronny.libjf.config.api.v2.ui.ConfigScreenFactory<?, ?> getConfigured2() {
        if (discovered2 == null) {
            discovered2 = getEntrypoints()
                    .stream()
                    .filter(it -> it instanceof io.gitlab.jfronny.libjf.config.api.v2.ui.ConfigScreenFactory<?,?>)
                    .<ConfigScreenFactory<?, ?>>map(it -> (ConfigScreenFactory<?, ?>) it)
                    .max(Comparator.comparing(io.gitlab.jfronny.libjf.config.api.v2.ui.ConfigScreenFactory::getPriority))
                    .orElseGet(PlaceholderScreenFactory::new);
        }
        return discovered2;
    }

    private static List<Object> getEntrypoints() {
        return FabricLoader.getInstance().getEntrypoints("libjf:config_screen", Object.class);
    }
}
