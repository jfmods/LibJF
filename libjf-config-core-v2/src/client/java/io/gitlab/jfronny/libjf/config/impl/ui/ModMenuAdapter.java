package io.gitlab.jfronny.libjf.config.impl.ui;

import com.terraformersmc.modmenu.api.ConfigScreenFactory;
import com.terraformersmc.modmenu.api.ModMenuApi;
import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.config.api.v2.ConfigHolder;
import io.gitlab.jfronny.libjf.config.api.v2.ConfigInstance;
import io.gitlab.jfronny.libjf.config.api.v2.Naming;
import io.gitlab.jfronny.libjf.config.impl.ConfigCore;

import java.util.HashMap;
import java.util.Map;

public class ModMenuAdapter implements ModMenuApi {
    @Override
    public ConfigScreenFactory<?> getModConfigScreenFactory() {
        return buildFactory(ConfigCore.CONFIG_INSTANCE);
    }

    @Override
    public Map<String, ConfigScreenFactory<?>> getProvidedConfigScreenFactories() {
        Map<String, ConfigScreenFactory<?>> factories = new HashMap<>();
        for (Map.Entry<String, ConfigInstance> entry : ConfigHolder.getInstance().getRegistered().entrySet()) {
            if (!LibJf.MOD_ID.equals(entry.getKey()))
                factories.put(entry.getKey(), buildFactory(entry.getValue()));
        }
        return factories;
    }

    private static ConfigScreenFactory<?> buildFactory(ConfigInstance config) {
        return s -> io.gitlab.jfronny.libjf.config.api.v2.ui.ConfigScreenFactory.getInstance().create(config, Naming.get(config.getId()), s).get();
    }
}
