package io.gitlab.jfronny.libjf.config.impl.ui;

import io.gitlab.jfronny.libjf.config.api.v2.Naming;
import io.gitlab.jfronny.libjf.config.impl.ConfigCore;
import net.minecraft.text.Text;
import org.jetbrains.annotations.Nullable;

public class OverviewNaming implements Naming {
    @Override
    public Text name() {
        return Text.translatable(ConfigCore.MOD_ID + ".overview");
    }

    @Override
    public @Nullable Text description() {
        return null;
    }

    @Override
    public Text preset(String name) {
        return null;
    }

    @Override
    public Naming category(String name) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Entry entry(String name) {
        throw new UnsupportedOperationException();
    }
}
