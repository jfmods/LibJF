package io.gitlab.jfronny.libjf.config.impl.ui;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.text.Text;

import java.util.Objects;

public class PlaceholderScreen extends Screen {
    private final Screen parent;

    public PlaceholderScreen(Screen parent) {
        this(
                parent,
                Text.translatable("libjf-config-core-v2.no-screen"),
                Text.translatable("libjf-config-core-v2.no-screen.description")
        );
    }

    public PlaceholderScreen(Screen parent, Text title, Text description) {
        super(title);
        this.parent = parent;
        addDrawable((context, mouseX, mouseY, delta) -> {
            context.drawCenteredTextWithShadow(textRenderer, description, width / 2, (height - textRenderer.fontHeight) / 2, 0xFFFFFF);
        });
    }

    @Override
    public void close() {
        Objects.requireNonNull(client).setScreen(parent);
    }
}
