package io.gitlab.jfronny.libjf.config.impl.ui;

import io.gitlab.jfronny.libjf.config.api.v2.ConfigInstance;
import io.gitlab.jfronny.libjf.config.api.v2.ui.ConfigScreenFactory;
import io.gitlab.jfronny.libjf.config.api.v2.Naming;
import net.minecraft.client.gui.screen.Screen;

public class PlaceholderScreenFactory implements ConfigScreenFactory<PlaceholderScreen, PlaceholderScreenFactory.Built> {
    @Override
    public Built create(ConfigInstance config, Naming naming, Screen parent) {
        return new Built(new PlaceholderScreen(parent));
    }

    @Override
    public int getPriority() {
        return -100;
    }

    public record Built(PlaceholderScreen screen) implements ConfigScreenFactory.Built<PlaceholderScreen> {
        @Override
        public PlaceholderScreen get() {
            return screen;
        }

        @Override
        public void onSave(Runnable action) {
            throw new UnsupportedOperationException();
        }
    }
}
