package io.gitlab.jfronny.libjf.config.api.v2;

import java.lang.annotation.*;

/**
 * Annotation for config subclasses, which are to be shown as categories
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Category {
    /**
     * @return Other configs that should be shown to users by their ID
     */
    String[] referencedConfigs() default {};
    /**
     * @return A class for modifying the config with the ConfigBuilder API. Must implement the static method {@code CategoryBuilder tweak(CategoryBuilder builder)}
     */
    Class<?> tweaker() default void.class;
}
