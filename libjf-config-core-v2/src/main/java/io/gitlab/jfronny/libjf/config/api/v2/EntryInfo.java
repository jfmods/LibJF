package io.gitlab.jfronny.libjf.config.api.v2;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeToken;
import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.config.api.v2.type.Type;
import io.gitlab.jfronny.libjf.config.impl.dsl.DslEntryInfo;
import io.gitlab.jfronny.libjf.config.impl.dsl.NothingSerializedException;
import io.gitlab.jfronny.libjf.config.impl.entrypoint.JfConfigSafe;
import org.jetbrains.annotations.ApiStatus;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

public interface EntryInfo<T> {
    static EntryInfo<?> ofField(Field field) {
        return DslEntryInfo.ofField(field);
    }

    @ApiStatus.Internal
    static EntryInfo<?> ofField(Class<?> klazz, String name) {
        try {
            return ofField(klazz.getField(name));
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Get the name of this entry
     * @return This entry's name
     */
    String getName();

    /**
     * @return Get the default value of this entry
     */
    T getDefault();

    /**
     * Gets the current value
     * @return The current value
     */
    T getValue() throws IllegalAccessException;

    /**
     * Set the current value to the parameter
     * @param value The value to use
     */
    void setValue(T value) throws IllegalAccessException;

    /**
     * Get the value type of this entry. Will use the class definition, not the current value
     * @return The type of this entry
     */
    Type getValueType();

    /**
     * Whether the entry can be represented. If this is false, all methods except getName, fix and reset must throw UnsupportedOperationExceptions
     * @return Whether this entry can be represented
     */
    default boolean supportsRepresentation() {
        return true;
    }

    /**
     * Ensure the current value is within expected bounds.
     */
    void fix();

    /**
     * Set this entry's value to that of the element
     * @param reader The reader to read from
     */
    default <TEx extends Exception, Reader extends SerializeReader<TEx, Reader>> void loadFromJson(Reader reader) throws TEx, IllegalAccessException {
        try {
            setValue(deserializeOneFrom(reader));
        } catch (MalformedDataException e) {
            LibJf.LOGGER.error("Could not read " + getName(), e);
        } catch (NothingSerializedException ignored) {
        }
    }

    /**
     * Write the currently cached value to the writer
     * @param writer The writer to write to
     */
    default <TEx extends Exception, Writer extends SerializeWriter<TEx, Writer>> void writeTo(Writer writer, @Nullable String comment) throws TEx, IllegalAccessException {
        try {
            if (comment != null) writer.comment(comment);
            if (getValueType().isEnum()) {
                writer.comment("Valid: [" + Arrays.stream(getValueType().asEnum().options()).map(Objects::toString).collect(Collectors.joining(", ")) + "]");
            }
            writer.name(getName());
            serializeOneTo(getValue(), writer);
        } catch (MalformedDataException e) {
            LibJf.LOGGER.error("Could not write " + getName(), e);
        }
    }

    default <TEx extends Exception, Reader extends SerializeReader<TEx, Reader>> T deserializeOneFrom(Reader reader) throws TEx, MalformedDataException {
        return LibJf.MAPPER.getAdapter(getTypeToken()).deserialize(reader);
    }

    default <TEx extends Exception, Writer extends SerializeWriter<TEx, Writer>> void serializeOneTo(T value, Writer writer) throws TEx, MalformedDataException {
        LibJf.MAPPER.getAdapter(getTypeToken()).serialize(value, writer);
    }

    default TypeToken<T> getTypeToken() {
        return (TypeToken<T>) Objects.requireNonNullElse(getValueType().asToken(), TypeToken.get(String.class));
    }

    /**
     * @return Get the width for this entry
     */
    int getWidth();

    /**
     * @return Get the minimum value of this entry
     */
    double getMinValue();

    /**
     * @return Get the maximum value for this entry
     */
    double getMaxValue();

    default void reset() throws IllegalAccessException {
        setValue(getDefault());
    }
}
