package io.gitlab.jfronny.libjf.config.api.v2;

import java.lang.annotation.*;

/**
 * An annotation for config classes
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface JfConfig {
    /**
     * @return Other configs that should be shown to users by their ID
     */
    String[] referencedConfigs() default {};

    /**
     * @return A class for modifying the config with the ConfigBuilder API. Must implement the static method {@code ConfigBuilder tweak(ConfigBuilder builder)}
     */
    Class<?> tweaker() default void.class;
}
