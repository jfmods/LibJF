package io.gitlab.jfronny.libjf.config.api.v2;

import io.gitlab.jfronny.libjf.config.api.v2.dsl.DSL;

/**
 * The interface for entrypoints performing custom config registrations
 */
public interface JfCustomConfig {
    void register(DSL.Defaulted dsl);
}
