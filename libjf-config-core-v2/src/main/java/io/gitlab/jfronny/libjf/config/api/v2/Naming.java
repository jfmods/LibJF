package io.gitlab.jfronny.libjf.config.api.v2;

import io.gitlab.jfronny.libjf.config.api.v2.type.Type;
import io.gitlab.jfronny.libjf.config.impl.io.DefaultNaming;
import io.gitlab.jfronny.libjf.config.impl.io.Namings;
import net.minecraft.text.Text;
import org.jetbrains.annotations.Nullable;

public interface Naming {
    static Naming defaultWithPrefix(String prefix, @Nullable String id) {
        return new DefaultNaming(prefix, id);
    }

    static Naming defaultWithId(String id) {
        return defaultWithPrefix(id + ".jfconfig.", id);
    }

    static Naming get(String id) {
        return Namings.get(id);
    }

    Text name();

    @Nullable Text description();
    Text preset(String name);

    Naming category(String name);
    Entry entry(String name);

    default Naming referenced(ConfigInstance reference) {
        return get(reference.getId());
    }

    interface Entry {
        Text name();
        @Nullable Text tooltip();

        Text boolValue(boolean value);
        Text enumValue(Type type, Object value);
        Text nullValue();
    }

    interface Custom extends Naming {
        String getId();
    }

    abstract class Delegate implements Naming {
        protected final Naming delegate;

        public Delegate(Naming delegate) {
            this.delegate = delegate;
        }

        /**
         * This constructor allows you to more easily create a custom Naming for your mod.
         * ONLY use this in your top-level Naming class!
         * Make sure to implement Naming.Custom as well!
         *
         * @param id the id of this mod
         */
        public Delegate(String id) {
            this.delegate = Naming.defaultWithId(id);
        }

        @Override
        public Text name() {
            return delegate.name();
        }

        @Override
        public @Nullable Text description() {
            return delegate.description();
        }

        @Override
        public Text preset(String name) {
            return delegate.preset(name);
        }

        @Override
        public Naming category(String name) {
            return delegate.category(name);
        }

        @Override
        public Entry entry(String name) {
            return delegate.entry(name);
        }

        @Override
        public Naming referenced(ConfigInstance reference) {
            return delegate.referenced(reference);
        }
    }
}
