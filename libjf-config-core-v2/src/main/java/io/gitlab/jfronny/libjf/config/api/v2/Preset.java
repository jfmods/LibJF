package io.gitlab.jfronny.libjf.config.api.v2;

import java.lang.annotation.*;

/**
 * An annotation for static methods in config classes which may be called when the user selects them in-game
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Preset {
}
