package io.gitlab.jfronny.libjf.config.api.v2.dsl;

import io.gitlab.jfronny.commons.SamWithReceiver;
import io.gitlab.jfronny.libjf.config.api.v2.*;
import io.gitlab.jfronny.libjf.config.api.v2.type.Type;
import io.gitlab.jfronny.libjf.config.impl.ConfigCore;
import org.jetbrains.annotations.ApiStatus;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * An interface obtained through DSL or ConfigBuilder.category(), used for building config categories
 * @param <Builder> The class implementing this builder
 */
public interface CategoryBuilder<Builder extends CategoryBuilder<Builder>> {
    String CONFIG_PRESET_DEFAULT = ConfigCore.MOD_ID + ".default";

    Builder addPreset(String id, Consumer<ConfigCategory> action);
    Builder addPreset(String id, Runnable preset);
    Builder removePreset(String id);
    Builder addVerifier(Consumer<ConfigCategory> verifier);
    Builder addVerifier(Runnable verifier);
    Builder referenceConfig(String id);
    Builder referenceConfig(ConfigInstance config);
    Builder referenceConfig(Supplier<List<ConfigInstance>> gen);
    Builder category(String id, CategoryBuilderFunction builder);
    Builder value(String id, int def, double min, double max, Supplier<Integer> get, Consumer<Integer> set);
    Builder value(String id, long def, double min, double max, Supplier<Long> get, Consumer<Long> set);
    Builder value(String id, float def, double min, double max, Supplier<Float> get, Consumer<Float> set);
    Builder value(String id, double def, double min, double max, Supplier<Double> get, Consumer<Double> set);
    Builder value(String id, String def, Supplier<String> get, Consumer<String> set);
    Builder value(String id, boolean def, Supplier<Boolean> get, Consumer<Boolean> set);
    Builder value(String id, String def, String[] options, Supplier<String> get, Consumer<String> set);
    <T extends Enum<T>> Builder value(String id, T def, Class<T> klazz, Supplier<T> get, Consumer<T> set);
    <T> Builder value(String id, T def, double min, double max, Type type, int width, Supplier<T> get, Consumer<T> set);
    <T> Builder value(EntryInfo<T> entry);

    /**
     * Adds a migration to the category.
     * Migrations are used to update the config when the format changes.
     * They MUST consume the element and may use it to update the internal state.
     *
     * @param element The element to migrate
     * @param migration The migration to apply
     * @return this
     */
    @ApiStatus.Experimental Builder addMigration(String element, Migration migration);

    String getId();

    ConfigCategory build(Supplier<ConfigInstance> root);

    @FunctionalInterface
    @SamWithReceiver
    interface CategoryBuilderFunction {
        CategoryBuilder<?> apply(CategoryBuilder<?> builder);
    }
}
