package io.gitlab.jfronny.libjf.config.api.v2.dsl;

import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.throwable.ThrowingConsumer;

import java.util.function.Consumer;

@FunctionalInterface
public interface Migration {
    static Migration of(ThrowingConsumer<SerializeReader<?, ?>, Exception> migration) {
        Consumer<SerializeReader<?, ?>> safe = ((ThrowingConsumer<SerializeReader<?, ?>, RuntimeException>) (ThrowingConsumer) migration)::accept;
        return safe::accept;
    }

    <TEx extends Exception, Reader extends SerializeReader<TEx, Reader>> void apply(Reader reader) throws TEx;
}
