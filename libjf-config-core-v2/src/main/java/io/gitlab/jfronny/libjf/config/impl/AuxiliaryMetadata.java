package io.gitlab.jfronny.libjf.config.impl;

import io.gitlab.jfronny.commons.serialize.emulated.DataElement;
import io.gitlab.jfronny.libjf.config.api.v2.Category;
import io.gitlab.jfronny.libjf.config.api.v2.JfConfig;
import io.gitlab.jfronny.libjf.config.api.v2.dsl.CategoryBuilder;
import io.gitlab.jfronny.libjf.serialize.FabricLoaderDataMapper;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.metadata.CustomValue;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static io.gitlab.jfronny.libjf.config.impl.ConfigCore.MODULE_ID;

public class AuxiliaryMetadata {
    public static AuxiliaryMetadata of(Category category) {
        AuxiliaryMetadata meta = new AuxiliaryMetadata();
        if (category != null) {
            meta.referencedConfigs = List.of(category.referencedConfigs());
        }
        return meta.sanitize();
    }

    public static AuxiliaryMetadata of(JfConfig config) {
        AuxiliaryMetadata meta = new AuxiliaryMetadata();
        if (config != null) {
            meta.referencedConfigs = List.of(config.referencedConfigs());
        }
        return meta.sanitize();
    }

    public static @Nullable AuxiliaryMetadata forMod(String modId) {
        var metaRef = new Object() {
            AuxiliaryMetadata meta = null;
        };
        FabricLoader.getInstance().getModContainer(modId).ifPresent(container -> {
            CustomValue cv = container.getMetadata().getCustomValue(MODULE_ID);
            if (cv == null) {
                cv = container.getMetadata().getCustomValue("libjf");
                if (cv != null) {
                    cv = cv.getAsObject().get("config");
                }
            }
            if (cv != null) metaRef.meta = read(FabricLoaderDataMapper.toGson(cv));
        });
        return metaRef.meta;
    }

    public static AuxiliaryMetadata read(DataElement data) {
        if (!(data instanceof DataElement.Object obj)) throw new IllegalArgumentException("AuxiliaryMetadata must be an object");
        AuxiliaryMetadata meta = new AuxiliaryMetadata();
        for (Map.Entry<String, DataElement> entry : obj.members().entrySet()) {
            switch (entry.getKey()) {
                case "referencedConfigs" -> {
                    if (!(entry.getValue() instanceof DataElement.Array arr)) throw new IllegalArgumentException("referencedConfigs must be an array");
                    meta.referencedConfigs = new LinkedList<>();
                    arr.elements().forEach(element -> {
                        if (!(element instanceof DataElement.Primitive.String str)) throw new IllegalArgumentException("referencedConfigs must be an array of strings");
                        meta.referencedConfigs.add(str.value());
                    });
                }
                default -> throw new IllegalArgumentException("Unknown key in AuxiliaryMetadata: " + entry.getKey());
            }
        }
        return meta;
    }

    public List<String> referencedConfigs;

    public void applyTo(CategoryBuilder<?> builder) {
        if (referencedConfigs != null) referencedConfigs.forEach(builder::referenceConfig);
    }

    public AuxiliaryMetadata sanitize() {
        if (referencedConfigs == null) referencedConfigs = List.of();
        else referencedConfigs = List.copyOf(referencedConfigs);
        return this;
    }

    public AuxiliaryMetadata merge(AuxiliaryMetadata other) {
        if (other == null) return this;
        AuxiliaryMetadata meta = new AuxiliaryMetadata();
        meta.referencedConfigs = new LinkedList<>();
        meta.referencedConfigs.addAll(this.referencedConfigs);
        meta.referencedConfigs.addAll(other.referencedConfigs);
        return meta.sanitize();
    }
}
