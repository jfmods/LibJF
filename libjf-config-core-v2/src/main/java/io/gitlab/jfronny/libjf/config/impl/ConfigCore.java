package io.gitlab.jfronny.libjf.config.impl;

import io.gitlab.jfronny.libjf.config.api.v2.ConfigHolder;
import io.gitlab.jfronny.libjf.config.api.v2.ConfigInstance;
import io.gitlab.jfronny.libjf.config.api.v2.dsl.DSL;

public class ConfigCore {
    public static final String MOD_ID = "libjf-config-core-v2";
    public static final String MODULE_ID = "libjf:config";
    public static final String NAMING_ID = "libjf:config_naming";
    public static boolean watchForChanges = true;
    public static final ConfigInstance CONFIG_INSTANCE;

    static {
        ConfigHolder.getInstance().migrateFiles(MOD_ID);
        CONFIG_INSTANCE = DSL.create(MOD_ID).register(builder -> builder
                .value("watchForChanges", watchForChanges, () -> watchForChanges, b -> watchForChanges = b)
        );
    }
}
