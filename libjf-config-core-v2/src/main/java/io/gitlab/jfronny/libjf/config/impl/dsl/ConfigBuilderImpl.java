package io.gitlab.jfronny.libjf.config.impl.dsl;

import io.gitlab.jfronny.libjf.config.api.v2.ConfigInstance;
import io.gitlab.jfronny.libjf.config.api.v2.dsl.ConfigBuilder;
import io.gitlab.jfronny.libjf.config.impl.io.DefaultConfigIO;
import net.fabricmc.loader.api.FabricLoader;

import java.nio.file.Path;
import java.util.function.Consumer;

public class ConfigBuilderImpl extends CategoryBuilderImpl<ConfigBuilderImpl> implements ConfigBuilder<ConfigBuilderImpl> {
    public DslConfigInstance built;
    public Consumer<ConfigInstance> load;
    public Consumer<ConfigInstance> write;
    public Path path;

    public ConfigBuilderImpl(String id) {
        super(id, "");
        load = DefaultConfigIO.loader(id);
        write = DefaultConfigIO.writer(id);
        path = FabricLoader.getInstance().getConfigDir().resolve(id + ".json5");
    }

    @Override
    public ConfigBuilderImpl setLoadMethod(Consumer<ConfigInstance> load) {
        checkBuilt();
        this.load = load;
        return this;
    }

    public ConfigBuilderImpl setWriteMethod(Consumer<ConfigInstance> write) {
        checkBuilt();
        this.write = write;
        return this;
    }

    @Override
    public ConfigBuilderImpl executeAfterWrite(Consumer<ConfigInstance> method) {
        checkBuilt();
        this.write = this.write.andThen(method);
        return null;
    }

    public ConfigBuilderImpl setPath(Path path) {
        checkBuilt();
        this.path = path;
        return this;
    }

    @Override
    public DslConfigInstance build() {
        markBuilt();
        built = new DslConfigInstance(
                id,
                entries,
                presets,
                referencedConfigs,
                categories,
                () -> built,
                verifiers,
                migrations,
                load,
                write,
                path
        );
        built.load();
        return built;
    }
}
