package io.gitlab.jfronny.libjf.config.impl.dsl;

import io.gitlab.jfronny.libjf.config.api.v2.ConfigHolder;
import io.gitlab.jfronny.libjf.config.api.v2.ConfigInstance;
import io.gitlab.jfronny.libjf.config.api.v2.dsl.ConfigBuilder;
import io.gitlab.jfronny.libjf.config.api.v2.dsl.DSL;

import java.util.Objects;

public class DSLImpl implements DSL {
    @Override
    public ConfigInstance config(String configId, ConfigBuilder.ConfigBuilderFunction builder) {
        return builder.apply(new ConfigBuilderImpl(configId)).build();
    }

    @Override
    public ConfigInstance register(String configId, ConfigBuilder.ConfigBuilderFunction builder) {
        return register(ConfigHolder.getInstance(), configId, builder);
    }

    @Override
    public ConfigInstance register(ConfigHolder config, String configId, ConfigBuilder.ConfigBuilderFunction builder) {
        ConfigInstance instance = config(configId, builder);
        config.register(configId, instance);
        return instance;
    }

    public static class Defaulted extends DSLImpl implements DSL.Defaulted {
        public final String defaultId;

        public Defaulted(String defaultId) {
            this.defaultId = defaultId;
        }

        @Override
        public ConfigInstance config(ConfigBuilder.ConfigBuilderFunction builder) {
            return config(Objects.requireNonNull(defaultId), builder);
        }

        @Override
        public ConfigInstance register(ConfigBuilder.ConfigBuilderFunction builder) {
            return register(Objects.requireNonNull(defaultId), builder);
        }

        @Override
        public ConfigInstance register(ConfigHolder config, ConfigBuilder.ConfigBuilderFunction builder) {
            return register(config, Objects.requireNonNull(defaultId), builder);
        }
    }
}
