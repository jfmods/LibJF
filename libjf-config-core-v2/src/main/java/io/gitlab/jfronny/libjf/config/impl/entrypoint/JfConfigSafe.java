package io.gitlab.jfronny.libjf.config.impl.entrypoint;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.config.api.v2.JfCustomConfig;
import io.gitlab.jfronny.libjf.config.api.v2.dsl.DSL;
import io.gitlab.jfronny.libjf.config.impl.ConfigCore;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.entrypoint.EntrypointContainer;
import net.fabricmc.loader.api.entrypoint.PreLaunchEntrypoint;
import net.minecraft.util.Language;

import java.util.HashSet;
import java.util.Set;
import java.util.function.UnaryOperator;

public class JfConfigSafe implements PreLaunchEntrypoint {
    public static UnaryOperator<String> TRANSLATION_SUPPLIER = s -> null;
    public static final Set<String> REGISTERED_MODS = new HashSet<>();
    @Override
    public void onPreLaunch() {
        LibJf.setup();
        for (EntrypointContainer<Object> custom : FabricLoader.getInstance().getEntrypointContainers(ConfigCore.MODULE_ID, Object.class)) {
            String id = custom.getProvider().getMetadata().getId();
            if (custom.getEntrypoint() instanceof JfCustomConfig cfg && REGISTERED_MODS.add(id)) {
                cfg.register(DSL.create(id));
            }
        }
        TRANSLATION_SUPPLIER = s -> {
            String translated;
            try {
                translated = Language.getInstance().get(s);
            } catch (Throwable t) {
                LibJf.LOGGER.debug("Failed to translate key " + s, t);
                return null;
            }
            return translated.equals(s) ? null : translated;
        };
    }
}
