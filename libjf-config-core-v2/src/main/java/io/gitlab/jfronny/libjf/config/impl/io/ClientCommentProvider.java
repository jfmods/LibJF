package io.gitlab.jfronny.libjf.config.impl.io;

import io.gitlab.jfronny.libjf.config.impl.entrypoint.JfConfigSafe;
import org.jetbrains.annotations.Nullable;

public record ClientCommentProvider(String prefix) implements CommentProvider {
    public static final Loader LOADER = s -> new ClientCommentProvider(s + ".jfconfig.");

    @Override
    public @Nullable String description() {
        return JfConfigSafe.TRANSLATION_SUPPLIER.apply(prefix + "tooltip");
    }

    @Override
    public CommentProvider category(String id) {
        return new ClientCommentProvider(prefix + id + ".");
    }

    @Override
    public Entry entry(String id) {
        return new Entry(prefix + id);
    }

    public record Entry(String path) implements CommentProvider.Entry {
        @Override
        public @Nullable String tooltip() {
            return JfConfigSafe.TRANSLATION_SUPPLIER.apply(path + ".tooltip");
        }
    }
}
