package io.gitlab.jfronny.libjf.config.impl.io;

import org.jetbrains.annotations.Nullable;

public interface CommentProvider {
    @Nullable String description();

    CommentProvider category(String id);
    Entry entry(String id);

    interface Entry {
        @Nullable String tooltip();
    }

    interface Loader {
        Loader INSTANCE = ServerCommentProvider.LOADER;

        CommentProvider load(String id);
    }
}
