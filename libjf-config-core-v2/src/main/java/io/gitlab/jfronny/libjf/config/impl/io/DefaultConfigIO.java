package io.gitlab.jfronny.libjf.config.impl.io;

import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.Token;
import io.gitlab.jfronny.commons.serialize.json.JsonReader;
import io.gitlab.jfronny.commons.serialize.json.JsonWriter;
import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.config.api.v2.*;
import io.gitlab.jfronny.libjf.config.impl.dsl.DslConfigCategory;
import io.gitlab.jfronny.libjf.config.impl.watch.JfConfigWatchService;

import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.function.Consumer;

public class DefaultConfigIO {
    public static Consumer<ConfigInstance> loader(String id) {
        return c -> c.getFilePath().ifPresent(path -> {
            // Actions cannot be cached since entries can change
            if (Files.exists(path)) {
                try (BufferedReader br = Files.newBufferedReader(path);
                     JsonReader jr = LibJf.LENIENT_TRANSPORT.createReader(br)) {
                    runActions(id, createActions(c), jr);
                } catch (Exception e) {
                    LibJf.LOGGER.error("Could not read config for " + id, e);
                }
            }
            c.write();
        });
    }

    record Action(Consumer<SerializeReader> task, boolean required) {
        public Action(Consumer<SerializeReader> task) {
            this(task, true);
        }
    }

    private static <TEx extends Exception, Reader extends SerializeReader<TEx, Reader>> void runActions(String id, Map<String, Action> actions, Reader reader) {
        try {
            if (reader.peek() != Token.BEGIN_OBJECT) {
                LibJf.LOGGER.error("Invalid config: Not a JSON object for " + id);
                reader.skipValue();
                return;
            }
            Set<String> appeared = new HashSet<>();
            reader.beginObject();
            while (reader.peek() != Token.END_OBJECT) {
                String name = reader.nextName();
                try (var view = reader.createView()) {
                    DefaultConfigIO.Action action = actions.get(name);
                    if (action == null) {
                        LibJf.LOGGER.warn("Unrecognized key in config for " + id + ": " + name);
                        continue;
                    }
                    if (!appeared.add(name)) {
                        LibJf.LOGGER.warn("Duplicate key in config for " + id + ": " + name);
                        continue;
                    }
                    action.task.accept(view);
                }
            }
            reader.endObject();
            actions.forEach((name, action) -> {
                if (action.required && !appeared.contains(name)) {
                    LibJf.LOGGER.error("Missing entry in config for " + id + ": " + name);
                }
            });
        } catch (Exception e) {
            throw new IllegalStateException("Could not read config", e);
        }
    }

    private static Map<String, Action> createActions(ConfigCategory category) {
        Map<String, Action> actions = new HashMap<>();
        category.getEntries().forEach(entry -> actions.putIfAbsent(entry.getName(), new Action(reader -> {
            try {
                entry.loadFromJson(reader);
            } catch (Exception e) {
                LibJf.LOGGER.error("Could not set config entry value of " + entry.getName(), e);
            }
        })));
        category.getCategories().forEach((id, cat) -> {
            String innerId = category.getId() + "." + id;
            var innerActions = createActions(cat);
            actions.putIfAbsent(id, new Action(reader -> runActions(innerId, innerActions, reader)));
        });
        if (category instanceof DslConfigCategory cat) {
            cat.migrations.forEach((id, migration) -> {
                actions.putIfAbsent(id, new Action(migration, false));
            });
        }
        return Map.copyOf(actions);
    }

    public static Consumer<ConfigInstance> writer(String id) {
        return c -> c.getFilePath().ifPresent(path -> JfConfigWatchService.lock(path, () -> {
            try (BufferedWriter bw = Files.newBufferedWriter(path);
                 JsonWriter jw = LibJf.LENIENT_TRANSPORT.createWriter(bw)) {
                writeTo(jw, c, CommentProvider.Loader.INSTANCE.load(c.getId()));
            } catch (Exception e) {
                LibJf.LOGGER.error("Could not write config for " + id, e);
            }
        }));
    }

    private static void writeTo(JsonWriter writer, ConfigCategory category, CommentProvider comments) throws IOException {
        category.fix();
        String commentText;
        if ((commentText = comments.description()) != null) {
            writer.comment(commentText);
        }
        writer.beginObject();
        String val;
        for (EntryInfo<?> entry : category.getEntries()) {
            try {
                entry.writeTo(writer, comments.entry(entry.getName()).tooltip());
            } catch (IllegalAccessException e) {
                LibJf.LOGGER.error("Could not write entry", e);
            }
        }
        for (Map.Entry<String, ConfigCategory> entry : category.getCategories().entrySet()) {
            writer.name(entry.getKey());
            writeTo(writer, entry.getValue(), comments.category(entry.getKey()));
        }
        writer.endObject();
    }
}
