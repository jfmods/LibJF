package io.gitlab.jfronny.libjf.config.impl.io;

import io.gitlab.jfronny.libjf.config.api.v2.type.Type;
import io.gitlab.jfronny.libjf.config.api.v2.Naming;
import io.gitlab.jfronny.libjf.config.impl.ConfigCore;
import net.minecraft.text.Text;
import net.minecraft.util.Language;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public record DefaultNaming(String prefix, @Nullable String id) implements Naming {
    @Override
    public Text name() {
        return Text.translatableWithFallback(prefix + "title", id);
    }

    @Override
    public @Nullable Text description() {
        return Language.getInstance().hasTranslation(prefix + "tooltip")
                ? Text.translatable(prefix + "tooltip")
                : null;
    }

    @Override
    public Text preset(String name) {
        return Text.translatableWithFallback(prefix + name, name);
    }

    @Override
    public DefaultNaming category(String name) {
        return new DefaultNaming(prefix + name + ".", name);
    }

    @Override
    public Entry entry(String name) {
        return new Entry(prefix, name);
    }

    public record Entry(String prefix, String name_) implements Naming.Entry {
        @Override
        public Text name() {
            return Text.translatableWithFallback(prefix + name_, name_);
        }

        @Override
        public @Nullable Text tooltip() {
            return Language.getInstance().hasTranslation(prefix + name_ + ".tooltip")
                    ? Text.translatable(prefix + name_ + ".tooltip")
                    : null;
        }

        @Override
        public Text boolValue(boolean value) {
            return Language.getInstance().hasTranslation(prefix + name_ + "." + value)
                    ? Text.translatableWithFallback(prefix + name_ + "." + value, Boolean.toString(value))
                    : Text.translatableWithFallback(ConfigCore.MOD_ID + "." + value, Boolean.toString(value));
        }

        @Override
        public Text enumValue(Type type, Object value) {
            return type.asClass() == null
                    ? Text.translatableWithFallback(prefix + name_ + "." + value, Objects.toString(value))
                    : Text.translatableWithFallback(prefix + "enum." + type.getName() + "." + value, Objects.toString(value));
        }

        @Override
        public Text nullValue() {
            return Text.translatableWithFallback(ConfigCore.MOD_ID + ".null", "null");
        }
    }
}
