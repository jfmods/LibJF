package io.gitlab.jfronny.libjf.config.impl.io;

import io.gitlab.jfronny.libjf.config.api.v2.Naming;
import io.gitlab.jfronny.libjf.config.impl.ConfigCore;
import net.fabricmc.loader.api.FabricLoader;

import java.util.Map;
import java.util.stream.Collectors;

import static java.util.function.Function.identity;

public class Namings {
    private static final Map<String, Naming> NAMINGS = FabricLoader.getInstance().getEntrypoints(ConfigCore.NAMING_ID, Naming.Custom.class)
            .stream()
            .collect(Collectors.toMap(Naming.Custom::getId, identity()));

    public static Naming get(String id) {
        Naming result = NAMINGS.get(id);
        if (result != null) return result;
        return Naming.defaultWithId(id);
    }
}
