package io.gitlab.jfronny.libjf.config.impl.io;

import io.gitlab.jfronny.commons.data.String2ObjectMap;
import io.gitlab.jfronny.commons.serialize.Transport;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeToken;
import io.gitlab.jfronny.commons.serialize.json.JsonReader;
import io.gitlab.jfronny.commons.switchsupport.Result;
import io.gitlab.jfronny.commons.throwable.Coerce;
import io.gitlab.jfronny.libjf.LibJf;
import net.fabricmc.loader.api.FabricLoader;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

/**
 * Provides significantly worse support for config comments on the server side.
 */
public record ServerCommentProvider(Map<String, String> map, String prefix) implements CommentProvider {
    private static final TypeAdapter<String2ObjectMap<String>> ADAPTER = LibJf.MAPPER.getAdapter(new TypeToken<>() {});
    public static final CommentProvider.Loader LOADER = id -> FabricLoader.getInstance().getModContainer(id)
            .<CommentProvider>flatMap(s -> s.findPath("assets/" + s.getMetadata().getId() + "/lang/en_us.json")
                    .map(Coerce.<Path, String, IOException>function(Files::readString).toResult(IOException.class))
                    .flatMap(Result::toOptional)
                    .map(Coerce.<String, String2ObjectMap<String>, IOException>function(m -> LibJf.JSON_TRANSPORT.read(m, (Transport.Returnable<JsonReader, String2ObjectMap<String>, IOException>) ADAPTER::deserialize)).toResult(IOException.class))
                    .flatMap(Result::toOptional)
                    .map(m -> new ServerCommentProvider(m, id + ".jfconfig.")))
            .orElseGet(() -> new Fallback(id));

    @Override
    public @Nullable String description() {
        return map.get(prefix + "tooltip");
    }

    @Override
    public CommentProvider category(String id) {
        return new ServerCommentProvider(map, prefix + id + ".");
    }

    @Override
    public Entry entry(String id) {
        return new Entry(map, prefix + id);
    }

    public record Entry(Map<String, String> map, String path) implements CommentProvider.Entry {
        @Override
        public @Nullable String tooltip() {
            return map.get(path + ".tooltip");
        }
    }

    record Fallback(String name) implements CommentProvider {
        @Override
        public @Nullable String description() {
            return null;
        }

        @Override
        public CommentProvider category(String id) {
            return new Fallback(id);
        }

        @Override
        public Entry entry(String id) {
            return new Entry(name);
        }

        record Entry(String name) implements CommentProvider.Entry {
            @Override
            public @Nullable String tooltip() {
                return null;
            }
        }
    }
}
