package io.gitlab.jfronny.libjf.config.impl.watch;

import io.gitlab.jfronny.commons.throwable.ThrowingRunnable;

import java.io.Closeable;
import java.nio.file.Path;

public interface JfConfigWatchService extends Closeable {
    static <TEx extends Throwable> void lock(Path p, ThrowingRunnable<TEx> task) throws TEx {
        JfConfigWatchServiceImpl.lock(p, task);
    }

    void executeIteration();
}
