package io.gitlab.jfronny.libjf.config.impl.watch;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.config.impl.ConfigCore;
import io.gitlab.jfronny.libjf.coprocess.ThreadCoProcess;

import java.io.Closeable;
import java.io.IOException;

public class ToggleableConfigWatchService extends ThreadCoProcess implements Closeable {
    private JfConfigWatchService delegate = null;

    @Override
    public void executeIteration() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ignored) {
            return;
        }
        if (ConfigCore.watchForChanges) {
            if (delegate == null) {
                delegate = new JfConfigWatchServiceImpl();
                LibJf.LOGGER.info("Created config watch service");
            }
            delegate.executeIteration();
        } else {
            if (delegate != null) {
                try {
                    delegate.close();
                } catch (IOException ignored) {
                }
                delegate = null;
                LibJf.LOGGER.info("Discarded config watch service implementation");
            }
        }
    }

    @Override
    public void close() throws IOException {
        if (delegate != null) {
            delegate.close();
            delegate = null;
            LibJf.LOGGER.info("Discarded config watch service");
        }
    }
}
