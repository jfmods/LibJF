import io.gitlab.jfronny.scripts.*

plugins {
    id("jfmod.module")
}

base {
    archivesName = "libjf-config-network-v0"
}

dependencies {
    api(devProject(":libjf-base"))
    api(devProject(":libjf-config-core-v2"))
    modCompileOnly(libs.modmenu)
    include(modImplementation("net.fabricmc.fabric-api:fabric-networking-api-v1")!!)
    include(modImplementation("net.fabricmc.fabric-api:fabric-command-api-v2")!!)
}
