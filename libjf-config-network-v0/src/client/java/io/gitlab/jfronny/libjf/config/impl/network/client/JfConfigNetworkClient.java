package io.gitlab.jfronny.libjf.config.impl.network.client;

import io.gitlab.jfronny.libjf.config.impl.network.packet.ConfigurationCompletePacket;
import io.gitlab.jfronny.libjf.config.impl.network.packet.ConfigurationPacket;
import io.gitlab.jfronny.libjf.config.impl.network.RequestRouter;
import io.gitlab.jfronny.libjf.config.impl.network.packet.RequestPacket;
import io.gitlab.jfronny.libjf.config.impl.network.packet.ResponsePacket;
import net.fabricmc.fabric.api.client.networking.v1.*;

public class JfConfigNetworkClient {
    public static boolean isAvailable = false;
    public static void initialize() {
        RequestRouter.initialize();
        JfConfigNetworkCommands.initialize();
        ClientPlayNetworking.registerGlobalReceiver(ResponsePacket.ID, (payload, context) -> {
            RequestRouter.acceptResponse(payload, context.responseSender());
        });
        ClientPlayNetworking.registerGlobalReceiver(RequestPacket.ID, (payload, context) -> {
            RequestRouter.acceptRequest(payload, context.responseSender());
        });
        ClientConfigurationNetworking.registerGlobalReceiver(ConfigurationPacket.ID, (payload, context) -> {
            isAvailable = payload.version() == RequestRouter.PROTOCOL_VERSION; // Handshake possible?
            context.responseSender().sendPacket(new ConfigurationCompletePacket());
        });
        ClientConfigurationConnectionEvents.INIT.register((handler, client) -> {
            isAvailable = false; // Reset for new server connection
        });
    }
}
