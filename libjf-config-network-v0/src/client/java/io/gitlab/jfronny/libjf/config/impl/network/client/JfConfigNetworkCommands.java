package io.gitlab.jfronny.libjf.config.impl.network.client;

import com.mojang.brigadier.Command;
import io.gitlab.jfronny.libjf.config.api.v2.ConfigHolder;
import io.gitlab.jfronny.libjf.config.api.v2.ui.ConfigScreenFactory;
import io.gitlab.jfronny.libjf.config.impl.network.RequestRouter;
import io.gitlab.jfronny.libjf.config.impl.network.rci.MirrorConfigHolder;
import net.fabricmc.fabric.api.client.command.v2.ClientCommandRegistrationCallback;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.text.Text;

import static net.fabricmc.fabric.api.client.command.v2.ClientCommandManager.literal;

public class JfConfigNetworkCommands {
    public static void initialize() {
        ClientCommandRegistrationCallback.EVENT.register((dispatcher, registryAccess) -> {
            dispatcher.register(literal(RequestRouter.MOD_ID).executes(ctx -> {
                if (JfConfigNetworkClient.isAvailable) {
                    ConfigHolder ch = new MirrorConfigHolder(ClientPlayNetworking.getSender());
                    Screen screen = ConfigScreenFactory.getInstance().createOverview(ch, null);
                    // Delay since the chat needs to close first
                    MinecraftClient.getInstance().setScreen(new ScheduledScreen(screen));
                    return Command.SINGLE_SUCCESS;
                } else {
                    ctx.getSource().sendError(Text.literal(RequestRouter.MOD_ID + " server is unavailable"));
                    return -1;
                }
            }));
        });
    }
}
