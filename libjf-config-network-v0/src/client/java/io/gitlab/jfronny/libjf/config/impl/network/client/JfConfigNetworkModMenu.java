package io.gitlab.jfronny.libjf.config.impl.network.client;

import com.terraformersmc.modmenu.api.ModMenuApi;
import io.gitlab.jfronny.libjf.config.api.v2.ConfigHolder;
import io.gitlab.jfronny.libjf.config.api.v2.ui.ConfigScreenFactory;
import io.gitlab.jfronny.libjf.config.impl.network.PermissionDeniedException;
import io.gitlab.jfronny.libjf.config.impl.network.RequestRouter;
import io.gitlab.jfronny.libjf.config.impl.network.rci.MirrorConfigHolder;
import io.gitlab.jfronny.libjf.config.impl.ui.PlaceholderScreen;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.minecraft.text.Text;

public class JfConfigNetworkModMenu implements ModMenuApi {
    @Override
    public com.terraformersmc.modmenu.api.ConfigScreenFactory<?> getModConfigScreenFactory() {
        return parent -> {
            if (!JfConfigNetworkClient.isAvailable) return new PlaceholderScreen(
                    parent,
                    Text.translatable(RequestRouter.MOD_ID + ".modmenu.unavailable.title"),
                    Text.translatable(RequestRouter.MOD_ID + ".modmenu.unavailable.description")
            );
            try {
                ConfigHolder ch = new MirrorConfigHolder(ClientPlayNetworking.getSender());
                ch.getRegistered();
                return ConfigScreenFactory.getInstance().createOverview(ch, parent);
            } catch (PermissionDeniedException pe) {
                return new PlaceholderScreen(
                        parent,
                        Text.translatable(RequestRouter.MOD_ID + ".modmenu.denied.title"),
                        Text.translatable(RequestRouter.MOD_ID + ".modmenu.denied.description")
                );
            }
        };
    }
}
