package io.gitlab.jfronny.libjf.config.impl.network.client;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.text.Text;

public class ScheduledScreen extends Screen {
    private Screen scheduled;

    protected ScheduledScreen(Screen scheduled) {
        super(Text.literal("Close this screen"));

        this.scheduled = scheduled;
    }

    @Override
    public void removed() {
        if (scheduled != null) {
            Screen s = scheduled;
            client.send(() -> client.setScreen(s));
        }
        scheduled = null;
    }
}
