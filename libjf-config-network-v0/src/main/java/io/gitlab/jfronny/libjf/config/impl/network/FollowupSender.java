package io.gitlab.jfronny.libjf.config.impl.network;

import net.fabricmc.fabric.api.networking.v1.PacketSender;
import net.minecraft.network.PacketByteBuf;

import java.util.Map;

public interface FollowupSender {
    void sendFollowupRequest(PacketSender responseSender, String name, PacketByteBuf body, ResponseHandler responseHandler, Map<String, RequestHandler> temporaryHandlers);
}
