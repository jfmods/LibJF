package io.gitlab.jfronny.libjf.config.impl.network;

import net.minecraft.network.PacketByteBuf;

import java.util.Objects;
import java.util.function.Consumer;

public record PMResponseHandler(Consumer<Response> handler) implements ResponseHandler {
    public PMResponseHandler {
        Objects.requireNonNull(handler);
    }

    @Override
    public void onSuccess(PacketByteBuf buf) {
        handler.accept(new Response.Success(buf));
    }

    @Override
    public void onDeny() {
        handler.accept(Response.SimpleFailure.DENY);
    }

    @Override
    public void onNotFound() {
        handler.accept(Response.SimpleFailure.NOT_FOUND);
    }

    @Override
    public void onFailure(String message) {
        handler.accept(new Response.Failure(message));
    }

    public sealed interface Response {
        PacketByteBuf get();

        record Success(PacketByteBuf buf) implements Response {
            @Override
            public PacketByteBuf get() {
                return buf;
            }
        }

        enum SimpleFailure implements Response {
            DENY, NOT_FOUND;

            @Override
            public PacketByteBuf get() {
                if (this == DENY) throw new PermissionDeniedException("Access not allowed");
                else throw new UnsupportedOperationException("Method not found");
            }
        }

        record Failure(String message) implements Response {
            public Failure {
                Objects.requireNonNull(message);
            }

            @Override
            public PacketByteBuf get() {
                throw new RuntimeException(message);
            }
        }
    }
}
