package io.gitlab.jfronny.libjf.config.impl.network;

import net.minecraft.network.PacketByteBuf;

public interface RequestHandler {
    PacketByteBuf handle(PacketByteBuf buf, FollowupSender followupSender) throws Throwable;
}
