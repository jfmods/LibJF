package io.gitlab.jfronny.libjf.config.impl.network;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.config.impl.network.packet.ConfigurationCompletePacket;
import io.gitlab.jfronny.libjf.config.impl.network.packet.ConfigurationPacket;
import io.gitlab.jfronny.libjf.config.impl.network.packet.RequestPacket;
import io.gitlab.jfronny.libjf.config.impl.network.packet.ResponsePacket;
import io.netty.buffer.ByteBuf;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.PacketSender;
import net.fabricmc.fabric.api.networking.v1.PayloadTypeRegistry;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.Identifier;

import java.util.*;

public class RequestRouter {
    public static final String MOD_ID = "libjf-config-network-v0";
    public static int PROTOCOL_VERSION = 1;

    private static final Map<String, RequestHandler> persistendHandlers = new HashMap<>();
    private static final Map<Long, Request> currentRequests = new HashMap<>(); //TODO implement timeout and prune old requests
    private static final Random random = new Random();

    public static void initialize() {
        PayloadTypeRegistry.playC2S().register(RequestPacket.ID, RequestPacket.CODEC);
        PayloadTypeRegistry.playS2C().register(RequestPacket.ID, RequestPacket.CODEC);
        PayloadTypeRegistry.playC2S().register(ResponsePacket.ID, ResponsePacket.CODEC);
        PayloadTypeRegistry.playS2C().register(ResponsePacket.ID, ResponsePacket.CODEC);
        PayloadTypeRegistry.configurationS2C().register(ConfigurationPacket.ID, ConfigurationPacket.CODEC);
        PayloadTypeRegistry.configurationC2S().register(ConfigurationCompletePacket.ID, ConfigurationCompletePacket.CODEC);
    }

    public static Identifier id(String path) {
        return Identifier.of(MOD_ID, path);
    }

    public static void acceptResponse(ResponsePacket response, PacketSender responseSender) {
        Request request = currentRequests.remove(response.request());
        if (request != null) {
            switch (response.status()) {
                case 0 -> request.responseHandler.onSuccess(response.aux());
                case 1 -> request.responseHandler.onNotFound();
                case 2 -> request.responseHandler.onDeny();
                case 3 -> request.responseHandler.onFailure(response.aux().readString());
                default -> request.responseHandler.onFailure("Unrecognized error received");
            }
        }
    }

    public static void acceptRequest(RequestPacket request, PacketSender responseSender) {
        long id = request.request();
        int mode = 3;
        PacketByteBuf aux = PacketByteBufs.create();
        try {
            if (request.parent() == null) {
                // persistent
                mode = handleRequest(aux, id, request.aux(), persistendHandlers.get(request.name()));
            } else {
                // followup
                Request parent = currentRequests.get(request.parent());
                if (parent == null) {
                    mode = 1;
                } else {
                    String key = request.name();
                    RequestHandler handler = parent.temporaryHandlers.get(key);
                    if (handler == null) handler = persistendHandlers.get(key);
                    mode = handleRequest(aux, id, request.aux(), handler);
                }
            }
        } catch (Throwable t) {
            LibJf.LOGGER.error("Cannot complete request", t);
            aux.clear();
            aux.writeString(t.getMessage() == null ? "null" : t.getMessage());
        } finally {
            responseSender.sendPacket(new ResponsePacket(id, mode, aux));
        }
    }

    private static int handleRequest(ByteBuf aux, long id, PacketByteBuf buf, RequestHandler handler) throws Throwable {
        if (handler == null) {
            return 1;
        } else {
            PacketByteBuf response = handler.handle(
                    buf,
                    (responseSender, name, body, responseHandler, temporaryHandlers) ->
                            RequestRouter.sendRequest(responseSender, id, name, body, responseHandler, temporaryHandlers)
            );
            if (response != null) aux.writeBytes(response.copy());
            return 0;
        }
    }

    public static void deny(long id, PacketSender responseSender) {
        responseSender.sendPacket(new ResponsePacket(id, 2, PacketByteBufs.empty()));
    }

    public static void registerHandler(String name, RequestHandler handler) {
        persistendHandlers.put(name, handler);
    }

    public static void sendRequest(PacketSender responseSender, String name, PacketByteBuf body, ResponseHandler responseHandler, Map<String, RequestHandler> temporaryHandlers) {
        sendRequest(responseSender, null, name, body, responseHandler, temporaryHandlers);
    }

    private static void sendRequest(PacketSender responseSender, Long parent, String name, PacketByteBuf body, ResponseHandler responseHandler, Map<String, RequestHandler> temporaryHandlers) {
        long id;
        synchronized (currentRequests) {
            Set<Long> keys = currentRequests.keySet();
            do {
                id = random.nextLong();
            } while (keys.contains(id));
            currentRequests.put(id, new Request(temporaryHandlers, responseHandler));
        }
        responseSender.sendPacket(new RequestPacket(id, parent, name, body == null ? PacketByteBufs.empty() : PacketByteBufs.copy(body)));
    }

    private record Request(Map<String, RequestHandler> temporaryHandlers, ResponseHandler responseHandler) {}
}
