package io.gitlab.jfronny.libjf.config.impl.network;

import net.minecraft.network.PacketByteBuf;

public interface ResponseHandler {
    void onSuccess(PacketByteBuf buf);
    void onDeny();
    void onNotFound();
    void onFailure(String message);
}
