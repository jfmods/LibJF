package io.gitlab.jfronny.libjf.config.impl.network.packet;

import io.gitlab.jfronny.libjf.config.impl.network.RequestRouter;
import io.netty.buffer.ByteBuf;
import net.minecraft.network.codec.PacketCodec;
import net.minecraft.network.packet.CustomPayload;

public record ConfigurationCompletePacket() implements CustomPayload {
    public static final CustomPayload.Id<ConfigurationCompletePacket> ID = new CustomPayload.Id<>(RequestRouter.id("handshake_complete"));
    public static final PacketCodec<ByteBuf, ConfigurationCompletePacket> CODEC = PacketCodec.unit(new ConfigurationCompletePacket());

    @Override
    public Id<? extends CustomPayload> getId() {
        return ID;
    }
}
