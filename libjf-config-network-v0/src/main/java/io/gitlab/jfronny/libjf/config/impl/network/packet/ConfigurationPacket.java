package io.gitlab.jfronny.libjf.config.impl.network.packet;

import io.gitlab.jfronny.libjf.config.impl.network.RequestRouter;
import io.netty.buffer.ByteBuf;
import net.minecraft.network.codec.PacketCodec;
import net.minecraft.network.codec.PacketCodecs;
import net.minecraft.network.packet.CustomPayload;

public record ConfigurationPacket(int version) implements CustomPayload {
    public static final CustomPayload.Id<ConfigurationPacket> ID = new CustomPayload.Id<>(RequestRouter.id("handshake"));
    public static final PacketCodec<ByteBuf, ConfigurationPacket> CODEC = PacketCodecs.INTEGER.xmap(ConfigurationPacket::new, ConfigurationPacket::version);

    @Override
    public Id<? extends CustomPayload> getId() {
        return ID;
    }
}
