package io.gitlab.jfronny.libjf.config.impl.network.packet;

import io.gitlab.jfronny.libjf.config.impl.network.RequestRouter;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.codec.PacketCodec;
import net.minecraft.network.codec.PacketCodecs;
import net.minecraft.network.packet.CustomPayload;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public record RequestPacket(long request, @Nullable Long parent, String name, PacketByteBuf aux) implements CustomPayload {
    public static final Id<RequestPacket> ID = new CustomPayload.Id<>(RequestRouter.id("request"));
    public static final PacketCodec<PacketByteBuf, RequestPacket> CODEC = PacketCodec.tuple(
            PacketCodecs.VAR_LONG, RequestPacket::request,
            PacketCodecs.optional(PacketCodecs.VAR_LONG), RequestPacket::optionalParent,
            PacketCodecs.STRING, RequestPacket::name,
            PacketCodec.of((value, buf) -> buf.writeBytes(value), PacketByteBufs::copy), RequestPacket::aux,
            RequestPacket::new);

    private RequestPacket(long request, Optional<Long> parent, String name, PacketByteBuf aux) {
        this(request, parent.orElse(null), name, aux);
    }

    private Optional<Long> optionalParent() {
        return Optional.ofNullable(parent);
    }

    @Override
    public Id<? extends CustomPayload> getId() {
        return ID;
    }
}
