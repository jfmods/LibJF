package io.gitlab.jfronny.libjf.config.impl.network.packet;

import io.gitlab.jfronny.libjf.config.impl.network.RequestRouter;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.codec.PacketCodec;
import net.minecraft.network.codec.PacketCodecs;
import net.minecraft.network.packet.CustomPayload;

public record ResponsePacket(long request, int status, PacketByteBuf aux) implements CustomPayload {
    public static final CustomPayload.Id<ResponsePacket> ID = new CustomPayload.Id<>(RequestRouter.id("response"));
    public static final PacketCodec<PacketByteBuf, ResponsePacket> CODEC = PacketCodec.tuple(
            PacketCodecs.VAR_LONG, ResponsePacket::request,
            PacketCodecs.INTEGER, ResponsePacket::status,
            PacketCodec.of((value, buf) -> buf.writeBytes(value), PacketByteBufs::copy), ResponsePacket::aux,
            ResponsePacket::new
    );

    @Override
    public Id<? extends CustomPayload> getId() {
        return ID;
    }
}
