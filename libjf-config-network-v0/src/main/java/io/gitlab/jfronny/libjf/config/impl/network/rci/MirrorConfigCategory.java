package io.gitlab.jfronny.libjf.config.impl.network.rci;

import io.gitlab.jfronny.libjf.config.api.v2.*;
import io.gitlab.jfronny.libjf.config.api.v2.type.Type;
import io.gitlab.jfronny.libjf.config.impl.network.rci.entry.*;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.PacketSender;
import net.minecraft.network.PacketByteBuf;

import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MirrorConfigCategory extends MirrorObject implements ConfigCategory {
    protected final String id;
    protected final String categoryPath;
    protected final Supplier<MirrorConfigInstance> root;

    public MirrorConfigCategory(PacketSender packetSender, String id, String categoryPath, Supplier<MirrorConfigInstance> root) {
        super(packetSender);
        this.id = id;
        this.categoryPath = categoryPath;
        this.root = root;
    }

    @Override
    public String getId() {
        return id;
    }

    public void writeCategoryPath(PacketByteBuf buf) {
        root.get().writeConfigInstance(buf);
        buf.writeCollection(getCategoryPathList(), PacketByteBuf::writeString);
    }

    protected List<String> getCategoryPathList() {
        if (categoryPath.isEmpty()) return List.of();
        String[] sc = categoryPath.split("\\.");
        return Arrays.stream(sc).toList();
    }

    @Override
    public List<EntryInfo<?>> getEntries() {
        PacketByteBuf buf = PacketByteBufs.create();
        writeCategoryPath(buf);
        buf = sendRequest("getEntries", buf);
        return List.copyOf(buf.readList(b -> {
            String name = b.readString();
            int type = b.readInt();
            if (type == -1) return new MirrorEntryInfoUnsupported<>(packetSender, this, name);
            Type t = switch (Datatype.values()[type]) {
                case INT -> Type.TInt.INSTANCE;
                case LONG -> Type.TLong.INSTANCE;
                case FLOAT -> Type.TFloat.INSTANCE;
                case DOUBLE -> Type.TDouble.INSTANCE;
                case STRING -> Type.TString.INSTANCE;
                case BOOL -> Type.TBool.INSTANCE;
                case ENUM -> Type.TEnum.create(name, b.readList(PacketByteBuf::readString).toArray(String[]::new));
            };
            return new MirrorEntryInfo<>(
                    packetSender,
                    this,
                    name,
                    MirrorEntryInfo.read(b, t),
                    t,
                    b.readInt(),
                    b.readDouble(),
                    b.readDouble());
        }));
    }

    @Override
    public Map<String, Runnable> getPresets() {
        PacketByteBuf buf = PacketByteBufs.create();
        writeCategoryPath(buf);
        buf = sendRequest("getPresets", buf);
        return buf.readList(PacketByteBuf::readString).stream().collect(Collectors.toUnmodifiableMap(
                Function.identity(),
                s -> () -> runPreset(s)
        ));
    }

    private void runPreset(String id) {
        PacketByteBuf buf = PacketByteBufs.create();
        writeCategoryPath(buf);
        buf.writeString(id);
        sendRequest("runPreset", buf);
    }

    @Override
    public List<ConfigInstance> getReferencedConfigs() {
        PacketByteBuf buf = PacketByteBufs.create();
        writeCategoryPath(buf);
        buf = sendRequest("getReferencedConfigs", buf);
        return buf.readList(b -> b.readBoolean()
                ? MirrorConfigInstance.create(packetSender, b.readString())
                : MirrorConfigInstance.create(
                        packetSender,
                        b.readString(),
                        Stream.concat(
                                root.get().streamParentPaths(),
                                Stream.of(
                                        Stream.concat(
                                                Stream.of(root.get().getId()),
                                                getCategoryPathList().stream()
                                        ).toList()
                                )
                        ).toList()
                )
        );
    }

    @Override
    public Map<String, ConfigCategory> getCategories() {
        PacketByteBuf buf = PacketByteBufs.create();
        writeCategoryPath(buf);
        buf = sendRequest("getCategories", buf);
        return buf.readList(PacketByteBuf::readString)
                .stream()
                .collect(Collectors.toUnmodifiableMap(
                        Function.identity(),
                        s -> new MirrorConfigCategory(packetSender, s, categoryPath + s + ".", root)
                ));
    }

    @Override
    public ConfigInstance getRoot() {
        return root.get();
    }
}
