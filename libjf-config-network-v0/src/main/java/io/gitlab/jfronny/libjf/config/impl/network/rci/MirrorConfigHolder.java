package io.gitlab.jfronny.libjf.config.impl.network.rci;

import io.gitlab.jfronny.libjf.config.api.v2.ConfigHolder;
import io.gitlab.jfronny.libjf.config.api.v2.ConfigInstance;
import net.fabricmc.fabric.api.networking.v1.*;
import net.minecraft.network.PacketByteBuf;

import java.nio.file.Path;
import java.util.*;
import java.util.function.Function;
import java.util.stream.*;

public class MirrorConfigHolder extends MirrorObject implements ConfigHolder {
    public MirrorConfigHolder(PacketSender packetSender) {
        super(packetSender);
    }

    @Override
    public void register(String modId, ConfigInstance config) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Map<String, ConfigInstance> getRegistered() {
        PacketByteBuf buf = sendRequest("getRegistered", null);
        return buf.readList(PacketByteBuf::readString)
                .stream()
                .collect(Collectors.toUnmodifiableMap(Function.identity(), s -> MirrorConfigInstance.create(packetSender, s)));
    }

    @Override
    public ConfigInstance get(String modId) {
        return isRegistered(modId) ? MirrorConfigInstance.create(packetSender, modId) : null;
    }

    @Override
    public ConfigInstance get(Path configPath) {
        return null;
    }

    @Override
    public boolean isRegistered(String modId) {
        PacketByteBuf buf = PacketByteBufs.create();
        buf.writeString(modId);
        buf = sendRequest("isRegistered", buf);
        return buf.readBoolean();
    }

    @Override
    public boolean isRegistered(Path configPath) {
        return false;
    }

    @Override
    public void migrateFiles(String modId) {
        PacketByteBuf buf = PacketByteBufs.create();
        buf.writeString(modId);
        sendRequest("migrateFiles", buf);
    }
}
