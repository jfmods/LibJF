package io.gitlab.jfronny.libjf.config.impl.network.rci;

import io.gitlab.jfronny.libjf.config.api.v2.*;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.PacketSender;
import net.minecraft.network.PacketByteBuf;

import java.nio.file.Path;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class MirrorConfigInstance extends MirrorConfigCategory implements ConfigInstance {
    protected final List<List<String>> parentPaths;
    protected MirrorConfigInstance(PacketSender packetSender, String id, Supplier<MirrorConfigInstance> root, List<List<String>> parentPaths) {
        super(packetSender, id, "", root);
        this.parentPaths = parentPaths;
    }

    public static MirrorConfigInstance create(PacketSender packetSender, String id) {
        return create(packetSender, id, List.of());
    }

    public static MirrorConfigInstance create(PacketSender packetSender, String id, List<List<String>> parentPaths) {
        MirrorConfigInstance[] cis = {null};
        cis[0] = new MirrorConfigInstance(packetSender, id, () -> cis[0], parentPaths);
        return cis[0];
    }

    public Stream<List<String>> streamParentPaths() {
        return parentPaths.stream();
    }

    public void writeConfigInstance(PacketByteBuf buf) {
        buf.writeCollection(parentPaths, (b, l) -> b.writeCollection(l, PacketByteBuf::writeString));
        buf.writeString(id);
    }

    @Override
    public void load() {
        PacketByteBuf buf = PacketByteBufs.create();
        writeConfigInstance(buf);
        sendRequest("load", buf);
    }

    @Override
    public void write() {
        PacketByteBuf buf = PacketByteBufs.create();
        writeConfigInstance(buf);
        sendRequest("write", buf);
    }

    @Override
    public Optional<Path> getFilePath() {
        return Optional.empty();
    }
}
