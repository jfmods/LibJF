package io.gitlab.jfronny.libjf.config.impl.network.rci;

import io.gitlab.jfronny.libjf.config.impl.network.*;
import net.fabricmc.fabric.api.networking.v1.PacketSender;
import net.minecraft.network.PacketByteBuf;

import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

public class MirrorObject {
    protected final PacketSender packetSender;

    public MirrorObject(PacketSender packetSender) {
        this.packetSender = packetSender;
    }

    protected <T> T synchronize(Consumer<Hold<T>> action) {
        Object[] result = {null};
        ReentrantLock lock = new ReentrantLock();
        Condition condition = lock.newCondition();
        action.accept(with -> {
            result[0] = with;
            lock.lock();
            try {
                condition.signal();
            } finally {
                lock.unlock();
            }
        });
        lock.lock();
        try {
            condition.await();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } finally {
            lock.unlock();
        }
        return (T) result[0];
    }

    protected PacketByteBuf sendRequest(String name, PacketByteBuf body) {
        return sendRequest(name, body, Map.of());
    }

    protected PacketByteBuf sendRequest(String name, PacketByteBuf body, Map<String, RequestHandler> temporaryHandlers) {
        return this.<PMResponseHandler.Response>synchronize(hold -> {
            RequestRouter.sendRequest(packetSender, name, body, new PMResponseHandler(hold::resume), temporaryHandlers);
        }).get();
    }

    protected interface Hold<T> {
        void resume(T with);
    }
}
