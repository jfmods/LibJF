package io.gitlab.jfronny.libjf.config.impl.network.rci.entry;

public enum Datatype {
    INT, LONG, FLOAT, DOUBLE, STRING, BOOL, ENUM
}
