package io.gitlab.jfronny.libjf.config.impl.network.rci.entry;

import io.gitlab.jfronny.libjf.config.api.v2.type.Type;
import io.gitlab.jfronny.libjf.config.impl.network.rci.MirrorConfigCategory;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.PacketSender;
import net.minecraft.network.PacketByteBuf;

public class MirrorEntryInfo<T> extends MirrorEntryInfoBase<T> {
    private final T defaultValue;
    private final Type valueType;
    private final int width;
    private final double minValue;
    private final double maxValue;

    public MirrorEntryInfo(
            PacketSender packetSender,
            MirrorConfigCategory category,
            String entryName,
            T defaultValue,
            Type valueType,
            int width,
            double minValue,
            double maxValue
    ) {
        super(packetSender, category, entryName);
        this.defaultValue = defaultValue;
        this.valueType = valueType;
        this.width = width;
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    @Override
    public T getDefault() {
        return defaultValue;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public double getMinValue() {
        return minValue;
    }

    @Override
    public double getMaxValue() {
        return maxValue;
    }

    @Override
    public Type getValueType() {
        return valueType;
    }

    public static Object read(PacketByteBuf buf, Type type) {
        if (!buf.readBoolean()) return null;
        if (type.isInt()) return buf.readInt();
        if (type.isLong()) return buf.readLong();
        if (type.isFloat()) return buf.readFloat();
        if (type.isDouble()) return buf.readDouble();
        if (type.isString()) return buf.readString();
        if (type.isBool()) return buf.readBoolean();
        if (type.isEnum()) return type.asEnum().optionForString(buf.readString());
        throw new UnsupportedOperationException();
    }

    public static void write(PacketByteBuf buf, Type type, Object data) {
        if (data == null) buf.writeBoolean(false);
        else {
            buf.writeBoolean(true);
            if (type.isInt()) buf.writeInt((int) data);
            else if (type.isLong()) buf.writeLong((long) data);
            else if (type.isFloat()) buf.writeFloat((float) data);
            else if (type.isDouble()) buf.writeDouble((double) data);
            else if (type.isString()) buf.writeString((String) data);
            else if (type.isBool()) buf.writeBoolean((boolean) data);
            else if (type.isEnum()) buf.writeString(data.toString());
            else throw new UnsupportedOperationException();
        }
    }

    @Override
    public T getValue() {
        PacketByteBuf buf = PacketByteBufs.create();
        writePath(buf);
        buf = sendRequest("getEntryValue", buf);
        return (T) read(buf, valueType);
    }

    @Override
    public void setValue(T value) {
        PacketByteBuf buf = PacketByteBufs.create();
        writePath(buf);
        write(buf, valueType, value);
        sendRequest("setEntryValue", buf);
    }
}
