package io.gitlab.jfronny.libjf.config.impl.network.rci.entry;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.libjf.config.api.v2.EntryInfo;
import io.gitlab.jfronny.libjf.config.impl.network.rci.MirrorConfigCategory;
import io.gitlab.jfronny.libjf.config.impl.network.rci.MirrorObject;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.PacketSender;
import net.minecraft.network.PacketByteBuf;

public abstract class MirrorEntryInfoBase<T> extends MirrorObject implements EntryInfo<T> {
    protected final MirrorConfigCategory category;
    protected final String entryName;

    public MirrorEntryInfoBase(PacketSender packetSender, MirrorConfigCategory category, String entryName) {
        super(packetSender);
        this.category = category;
        this.entryName = entryName;
    }

    protected void writePath(PacketByteBuf buf) {
        category.writeCategoryPath(buf);
        buf.writeString(entryName);
    }

    @Override
    public String getName() {
        return entryName;
    }

    @Override
    public <TEx extends Exception, Reader extends SerializeReader<TEx, Reader>> void loadFromJson(Reader reader) {
        throw new UnsupportedOperationException();
    }

    @Override
    public <TEx extends Exception, Writer extends SerializeWriter<TEx, Writer>> void writeTo(Writer writer, String translationPrefix) {
        throw new UnsupportedOperationException();
    }

    @Override
    public <TEx extends Exception, Reader extends SerializeReader<TEx, Reader>> T deserializeOneFrom(Reader reader) throws TEx, MalformedDataException {
        throw new UnsupportedOperationException();
    }

    @Override
    public <TEx extends Exception, Writer extends SerializeWriter<TEx, Writer>> void serializeOneTo(T value, Writer writer) throws TEx, MalformedDataException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void fix() {
        PacketByteBuf buf = PacketByteBufs.create();
        writePath(buf);
        sendRequest("fixEntry", buf);
    }

    @Override
    public void reset() {
        PacketByteBuf buf = PacketByteBufs.create();
        writePath(buf);
        sendRequest("resetEntry", buf);
    }
}
