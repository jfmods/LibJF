package io.gitlab.jfronny.libjf.config.impl.network.rci.entry;

import io.gitlab.jfronny.libjf.config.api.v2.type.Type;
import io.gitlab.jfronny.libjf.config.impl.network.rci.MirrorConfigCategory;
import net.fabricmc.fabric.api.networking.v1.PacketSender;

public class MirrorEntryInfoUnsupported<T> extends MirrorEntryInfoBase<T> {
    public MirrorEntryInfoUnsupported(PacketSender packetSender, MirrorConfigCategory category, String entryName) {
        super(packetSender, category, entryName);
    }

    @Override
    public T getDefault() {
        throw new UnsupportedOperationException();
    }

    @Override
    public T getValue() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setValue(T value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Type getValueType() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getWidth() {
        throw new UnsupportedOperationException();
    }

    @Override
    public double getMinValue() {
        throw new UnsupportedOperationException();
    }

    @Override
    public double getMaxValue() {
        throw new UnsupportedOperationException();
    }
}
