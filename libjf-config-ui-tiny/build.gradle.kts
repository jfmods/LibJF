import io.gitlab.jfronny.scripts.*

plugins {
    id("jfmod.module")
}

base {
    archivesName = "libjf-config-ui-tiny"
}

dependencies {
    api(devProject(":libjf-base"))
    api(devProject(":libjf-config-core-v2"))
    include("net.fabricmc.fabric-api:fabric-resource-loader-v0")
}
