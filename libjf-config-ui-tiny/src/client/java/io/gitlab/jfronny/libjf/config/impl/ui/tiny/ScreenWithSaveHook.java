package io.gitlab.jfronny.libjf.config.impl.ui.tiny;

import io.gitlab.jfronny.commons.ref.R;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.text.Text;

public abstract class ScreenWithSaveHook extends Screen {
    public Runnable saveHook = R::nop;

    protected ScreenWithSaveHook(Text title) {
        super(title);
    }
}
