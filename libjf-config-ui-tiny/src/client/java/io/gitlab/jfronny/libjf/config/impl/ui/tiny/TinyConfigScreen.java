package io.gitlab.jfronny.libjf.config.impl.ui.tiny;

import io.gitlab.jfronny.commons.throwable.Try;
import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.config.api.v2.ConfigCategory;
import io.gitlab.jfronny.libjf.config.api.v2.Naming;
import io.gitlab.jfronny.libjf.config.impl.ConfigCore;
import io.gitlab.jfronny.libjf.config.impl.ui.tiny.entry.EntryListWidget;
import io.gitlab.jfronny.libjf.config.impl.ui.tiny.entry.WidgetState;
import io.gitlab.jfronny.libjf.config.impl.ui.tiny.presets.PresetsScreen;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.ScreenRect;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.tab.TabManager;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.gui.widget.TabNavigationWidget;
import net.minecraft.screen.ScreenTexts;
import net.minecraft.text.Text;
import net.minecraft.util.math.MathHelper;

import java.util.*;

@Environment(EnvType.CLIENT)
public class TinyConfigScreen extends ScreenWithSaveHook {
    private final Screen parent;
    private final ConfigCategory config;
    private final Naming naming;
    public final List<WidgetState<?>> widgets; // Filled in from TinyConfigTab
    private final Placeholder<EntryListWidget> placeholder;
    private final TabManager tabManager = new TabManager(a -> selectTab(((TinyConfigTabWrapper)a).getTab()), a -> {});
    private List<TinyConfigTab> tabs;
    private final boolean considerTabs;
    public ButtonWidget done;
    private boolean reload = false;

    public TinyConfigScreen(ConfigCategory config, Naming naming, Screen parent) {
        this(config, naming, parent, shouldConsiderTabs(config, naming));
    }

    public TinyConfigScreen(ConfigCategory config, Naming naming, Screen parent, boolean considerTabs) {
        super(naming.name());
        this.parent = parent;
        this.config = config;
        this.naming = naming;
        this.widgets = new LinkedList<>();
        this.placeholder = new Placeholder<>(null);
        this.considerTabs = considerTabs;
    }

    private static boolean shouldConsiderTabs(ConfigCategory config, Naming naming) {
        return config.getEntries().isEmpty()
                && config.getReferencedConfigs().isEmpty()
                && config.getCategories().size() > 1
                && naming.description() == null;
    }

    @Override
    protected void init() {
        super.init();

        if (!reload) {
            this.done = ButtonWidget.builder(ScreenTexts.DONE, button -> {
                        for (WidgetState<?> state : widgets) {
                            Try.orElse(state::writeToEntry, e -> LibJf.LOGGER.error("Could not write config data to class", e));
                        }
                        config.getRoot().write();
                        Objects.requireNonNull(client).setScreen(parent);
                        saveHook.run();
                    })
                    .dimensions(this.width / 2 + 4, this.height - 28, 150, 20)
                    .build();
        } else done.setPosition(width / 2 + 4, height - 28);

        updateTabs();

        TabNavigationWidget tabNavigation = TabNavigationWidget.builder(tabManager, this.width)
                .tabs(tabs.toArray(TinyConfigTab[]::new))
                .build();

        if (tabs.size() > 1) this.addDrawableChild(tabNavigation);
        tabNavigation.selectTab(0, false);
        tabNavigation.init();

        this.addDrawableChild(ButtonWidget.builder(ScreenTexts.CANCEL,
                        button -> Objects.requireNonNull(client).setScreen(parent))
                .dimensions(this.width / 2 - 154, this.height - 28, 150, 20)
                .build());

        this.addDrawableChild(done);

        if (!config.getPresets().isEmpty()) {
            this.addDrawableChild(ButtonWidget.builder(Text.translatable(ConfigCore.MOD_ID + ".presets"),
                            button -> Objects.requireNonNull(client).setScreen(new PresetsScreen(this, config, naming, this::afterSelectPreset)))
                    .dimensions(8, tabs.size() == 1 ? 6 : this.height - 28, 80, 20)
                    .build());
        }

        this.addDrawableChild(this.placeholder);

        // Sizing is also done in TinyConfigTab. Keep these in sync!
        tabManager.setTabArea(new ScreenRect(0, 32, width, height - 68));

        reload = true;
    }

    private boolean wasTabs = false;
    private void updateTabs() {
        boolean useTabs = considerTabs && !tabsWouldOverflow(config.getCategories().values());
        if (reload) {
            if (!considerTabs) return;
            if (wasTabs == useTabs) return;
        }
        wasTabs = useTabs;

        tabs = useTabs
                ? config.getCategories()
                    .values()
                    .stream()
                    .map(c -> new TinyConfigTab(this, c, naming.category(c.getId()), textRenderer, false))
                    .toList()
                : List.of(new TinyConfigTab(this, config, naming, textRenderer, true));
    }

    private boolean tabsWouldOverflow(Collection<ConfigCategory> categories) {
        // Mirrors TabNavigationWidget#init
        int tabNavWidth = this.width;
        int headerWidth = Math.min(400, tabNavWidth) - 28;
        int singleHeaderWidth = MathHelper.roundUpToMultiple(headerWidth / categories.size(), 2);
        int singleTextWidth = singleHeaderWidth - 2;
        for (ConfigCategory category : categories) {
            if (textRenderer.getWidth(naming.category(category.getId()).name()) > singleTextWidth) return true;
        }
        return false;
    }

    public void afterSelectPreset() {
        for (WidgetState<?> widget : widgets) {
            widget.updateCache();
        }
    }

    private void selectTab(TinyConfigTab tab) {
        placeholder.setChild(tab.getList());
    }

    @Override
    public void render(DrawContext context, int mouseX, int mouseY, float delta) {
        super.render(context, mouseX, mouseY, delta);

        if (tabs.size() == 1) context.drawCenteredTextWithShadow(textRenderer, title, width / 2, 16 - textRenderer.fontHeight, 0xFFFFFF);

        Optional<Text> hovered = placeholder.getChild().getHoveredEntryTitle(mouseY);
        if (hovered.isPresent()) {
            for (WidgetState<?> info : widgets) {
                Text text = hovered.get();
                Text name = info.naming.name();
                boolean showTooltip = text.equals(name);
                if (showTooltip && info.error != null) {
                    showTooltip = false;
                    context.drawTooltip(textRenderer, info.error, mouseX, mouseY);
                }
                Text tooltip = info.naming.tooltip();
                if (showTooltip && tooltip != null) {
                    showTooltip = false;
                    context.drawTooltip(textRenderer, tooltip, mouseX, mouseY);
                }
            }
        }
    }

    @Override
    public void close() {
        Objects.requireNonNull(client).setScreen(parent);
    }

    public MinecraftClient getClient() {
        return Objects.requireNonNull(client);
    }
}
