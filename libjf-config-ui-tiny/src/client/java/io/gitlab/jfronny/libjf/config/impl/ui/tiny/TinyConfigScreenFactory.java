package io.gitlab.jfronny.libjf.config.impl.ui.tiny;

import io.gitlab.jfronny.commons.serialize.Transport;
import io.gitlab.jfronny.commons.serialize.json.JsonReader;
import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.config.api.v2.ConfigInstance;
import io.gitlab.jfronny.libjf.config.api.v2.EntryInfo;
import io.gitlab.jfronny.libjf.config.api.v2.Naming;
import io.gitlab.jfronny.libjf.config.api.v2.dsl.CategoryBuilder;
import io.gitlab.jfronny.libjf.config.api.v2.type.Type;
import io.gitlab.jfronny.libjf.config.api.v2.ui.ConfigScreenFactory;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.toast.SystemToast;
import net.minecraft.text.Text;

import java.io.IOException;

// IDEA doesn't like this, but it does work in practice
public class TinyConfigScreenFactory implements ConfigScreenFactory<Screen, TinyConfigScreenFactory.Built> {
    @Override
    public Built create(ConfigInstance config, Naming naming, Screen parent) {
        if (config.getEntries().size() == 1
                && config.getPresets().keySet().stream().allMatch(s -> s.equals(CategoryBuilder.CONFIG_PRESET_DEFAULT))
                && config.getReferencedConfigs().isEmpty()
                && config.getCategories().isEmpty()) {
            EntryInfo<?> entry = config.getEntries().getFirst();
            Type type = entry.supportsRepresentation() ? entry.getValueType() : null;
            if (type != null && !type.isInt() && !type.isLong() && !type.isFloat() && !type.isDouble() && !type.isString() && !type.isBool() && !type.isEnum()) {
                return createJson(config, parent, entry, naming.entry(entry.getName()));
            }
        }
        return new Built(new TinyConfigScreen(config, naming, parent));
    }

    private <T> Built createJson(ConfigInstance config, Screen parent, EntryInfo<T> entry, Naming.Entry naming) {
        final String jsonified;
        try {
            var value = entry.getValue();
            jsonified = LibJf.LENIENT_TRANSPORT.write(writer -> entry.serializeOneTo(value, writer));
        } catch (IllegalAccessException | IOException e) {
            throw new RuntimeException(e);
        }
        return new Built(new EditorScreen(
                naming.name(),
               naming.tooltip(),
                parent,
                jsonified,
                json -> {
                    try {
                        entry.setValue(LibJf.LENIENT_TRANSPORT.read(
                                json,
                                (Transport.Returnable<JsonReader, ? extends T, IOException>) entry::deserializeOneFrom));
                        config.write();
                    } catch (Throwable e) {
                        LibJf.LOGGER.error("Could not write element", e);
                        SystemToast.add(
                                MinecraftClient.getInstance().getToastManager(),
                                SystemToast.Type.PACK_LOAD_FAILURE,
                                Text.translatable("libjf-config-ui-tiny.entry.json.write.fail.title"),
                                Text.translatable("libjf-config-ui-tiny.entry.json.write.fail.description")
                        );
                    }
                }
        ));
    }

    @Override
    public int getPriority() {
        return 0;
    }

    public record Built(ScreenWithSaveHook screen) implements ConfigScreenFactory.Built<Screen> {
        @Override
        public Screen get() {
            return screen;
        }

        @Override
        public void onSave(Runnable action) {
            Runnable currentHook = screen.saveHook;
            screen.saveHook = () -> {
                currentHook.run();
                action.run();
            };
        }
    }
}
