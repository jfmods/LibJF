package io.gitlab.jfronny.libjf.config.impl.ui.tiny;

import io.gitlab.jfronny.libjf.config.api.v2.ConfigCategory;
import io.gitlab.jfronny.libjf.config.api.v2.ConfigInstance;
import io.gitlab.jfronny.libjf.config.api.v2.Naming;
import io.gitlab.jfronny.libjf.config.api.v2.dsl.CategoryBuilder;
import io.gitlab.jfronny.libjf.config.impl.ConfigCore;
import io.gitlab.jfronny.libjf.config.impl.ui.tiny.entry.WidgetFactory;
import io.gitlab.jfronny.libjf.config.impl.ui.tiny.entry.*;
import io.gitlab.jfronny.libjf.config.impl.ui.tiny.presets.PresetsScreen;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.ScreenRect;
import net.minecraft.client.gui.tab.Tab;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.gui.widget.ClickableWidget;
import net.minecraft.text.Text;

import java.util.*;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;

public class TinyConfigTab implements Tab {
    private final ConfigCategory config;
    private final Naming naming;
    private final EntryListWidget list;

    public TinyConfigTab(TinyConfigScreen screen, ConfigCategory config, Naming naming, TextRenderer textRenderer, boolean isRoot) {
        this.config = config;
        this.naming = naming;
        List<String> erroredEntries = new LinkedList<>();
        List<WidgetState<?>> widgets = EntryInfoWidgetBuilder.buildWidgets(config, naming, screen.widgets, erroredEntries);

        config.fix();
        for (WidgetState<?> widget : widgets) {
            widget.updateCache();
        }

        // Sizing is also done in TinyConfigScreen. Keep these in sync!
        this.list = new EntryListWidget(screen.getClient(), textRenderer, screen.width, screen.height - 68, 32);

        Text tooltip = naming.description();
        if (tooltip != null) {
            this.list.addText(tooltip);
        }

        for (String erroredEntry : erroredEntries) {
            this.list.addText(Text.translatable(ConfigCore.MOD_ID + ".errored_entry", erroredEntry));
        }

        if (!isRoot && !config.getPresets().keySet().stream().allMatch(s -> s.equals(CategoryBuilder.CONFIG_PRESET_DEFAULT))) {
            this.list.addReference(Text.translatable(ConfigCore.MOD_ID + ".presets"),
                    () -> new PresetsScreen(screen, config, naming, screen::afterSelectPreset));

        }
        for (Map.Entry<String, ConfigCategory> entry : config.getCategories().entrySet()) {
            Naming nmg = naming.category(entry.getKey());
            this.list.addReference(nmg.name(),
                    () -> new TinyConfigScreen(entry.getValue(), nmg, screen, false));
        }
        for (WidgetState<?> info : widgets) {
            WidgetFactory.Widget control = info.factory == null ? null : info.factory.build(screen, textRenderer);
            ButtonWidget resetButton = ButtonWidget.builder(Text.translatable(ConfigCore.MOD_ID + ".reset"), (button -> info.reset()))
                    .dimensions(screen.width - 155, 0, 40, 20)
                    .build();
            BooleanSupplier resetVisible = () -> {
                boolean visible = !Objects.equals(info.entry.getDefault(), info.cachedValue);
                resetButton.active = visible;
                return visible;
            };
            Reflowable reflow = (width, height) -> {
                resetButton.setX(width - 155);
                if (control != null) control.reflow(width, height);
            };
            if (control == null) this.list.addUnknown(resetButton, resetVisible, info.naming.name(), reflow);
            else this.list.addButton(control.control(), resetButton, resetVisible, info.naming.name(), reflow);
        }
        for (ConfigInstance ci : config.getReferencedConfigs()) {
            if (ci != null) {
                Naming nmg = naming.referenced(ci);
                this.list.addReference(Text.translatable(ConfigCore.MOD_ID + ".see-also", nmg.name()),
                        () -> new TinyConfigScreen(ci, nmg, screen));
            }
        }
    }

    @Override
    public Text getTitle() {
        return naming.name();
    }

    @Override
    public void forEachChild(Consumer<ClickableWidget> consumer) {
        consumer.accept(new TinyConfigTabWrapper(this));
    }

    @Override
    public void refreshGrid(ScreenRect tabArea) {
        list.refreshGrid(tabArea);
    }

    public EntryListWidget getList() {
        return list;
    }
}
