package io.gitlab.jfronny.libjf.config.impl.ui.tiny.entry;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.*;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.*;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.text.*;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.function.BooleanSupplier;
import java.util.function.Supplier;

@Environment(EnvType.CLIENT)
public class EntryListWidget extends ElementListWidget<EntryListWidget.ConfigEntry> {
    private final TextRenderer textRenderer;

    public EntryListWidget(MinecraftClient client, TextRenderer tr, int width, int height, int top) {
        super(client, width, height, top, 25);
        this.centerListVertically = false;
        textRenderer = tr;
    }

    @Override
    public int getScrollbarX() {
        return this.width -7;
    }

    public void addUnknown(ClickableWidget resetButton, BooleanSupplier resetVisible, Text text, Reflowable reflow) {
        this.addEntry(new ConfigUnknownEntry(text, resetVisible, resetButton, reflow, textRenderer));
    }

    public void addButton(@Nullable ClickableWidget button, ClickableWidget resetButton, BooleanSupplier resetVisible, Text text, Reflowable reflow) {
        this.addEntry(new ConfigScreenEntry(button, text, resetVisible, resetButton, reflow, textRenderer));
    }

    public void addReference(Text text, Supplier<Screen> targetScreen) {
        this.addEntry(new ConfigReferenceEntry(width, text, targetScreen));
    }

    public void addText(Text text) {
        for (ConfigEntry entry : ConfigTextEntry.create(textRenderer, this, text, width)) this.addEntry(entry);
    }

    @Override
    public int getRowWidth() {
        return 10000;
    }

    public Optional<Text> getHoveredEntryTitle(double mouseY) {
        for (ConfigEntry abstractEntry : this.children()) {
            if (abstractEntry instanceof ConfigScreenEntry entry
                    && entry.button.visible
                    && mouseY >= entry.button.getY() && mouseY < entry.button.getY() + itemHeight) {
                return Optional.of(entry.getText());
            } else if (abstractEntry instanceof ConfigUnknownEntry entry
                    && mouseY >= entry.resetButton.getY() && mouseY < entry.resetButton.getY() + itemHeight) {
                return Optional.of(entry.getText());
            }
        }
        return Optional.empty();
    }

    public void refreshGrid(ScreenRect tabArea) {
        setWidth(tabArea.width());
        setHeight(tabArea.height());
        setX(tabArea.getLeft());
        setY(tabArea.getTop());
        for (int len = getEntryCount() - 1, i = len; i >= 0; i--) {
            getEntry(i).reflow(width, height);
        }
    }

    @Override
    protected void renderDecorations(DrawContext context, int mouseX, int mouseY) {
        super.renderDecorations(context, mouseX, mouseY);

        // Gradient at the top of the screen
        context.fillGradient(RenderLayer.getGuiOverlay(), getX(), getY(), getRight(), getY() + 4, 0xff000000, 0x00000000, 0);
        // Gradient at the bottom of the screen
        if (getScrollY() < getMaxScrollY()) context.fillGradient(RenderLayer.getGuiOverlay(), getX(), getBottom() - 4, getRight(), getBottom(), 0x00000000, 0xff000000, 0);
    }

    @Environment(EnvType.CLIENT)
    public abstract static class ConfigEntry extends Entry<ConfigEntry> implements Reflowable {
        @Override
        public void render(DrawContext context, int index, int y, int x, int entryWidth, int entryHeight, int mouseX, int mouseY, boolean hovered, float tickDelta) {
            if (hovered) {
                context.fill(x, y, x + entryWidth, y + entryHeight, 0x24FFFFFF);
            }
        }
    }

    @Environment(EnvType.CLIENT)
    public class ConfigTextEntry extends ConfigEntry {
        private int width;
        private final OrderedText text;
        private final TextRenderer renderer;
        public StringVisitable originalText = null;

        public ConfigTextEntry(int width, OrderedText text, TextRenderer renderer) {
            this.width = width;
            this.text = text;
            this.renderer = renderer;
        }

        public static List<ConfigTextEntry> create(TextRenderer renderer, EntryListWidget parent, StringVisitable text, int width) {
            List<ConfigTextEntry> entries = new ArrayList<>();
            List<OrderedText> wrappedLines = new ArrayList<>(renderer.wrapLines(text, width));
            Collections.reverse(wrappedLines);
            for (OrderedText line : wrappedLines) {
                entries.add(parent.new ConfigTextEntry(width, line, renderer));
            }
            if (!entries.isEmpty()) entries.getLast().originalText = text;
            Collections.reverse(entries);
            return entries;
        }

        @Override
        public List<? extends Selectable> selectableChildren() {
            return List.of();
        }

        @Override
        public List<? extends Element> children() {
            return List.of();
        }

        @Override
        public void render(DrawContext context, int index, int y, int x, int entryWidth, int entryHeight, int mouseX, int mouseY, boolean hovered, float tickDelta) {
            super.render(context, index, y, x, entryWidth, entryHeight, mouseX, mouseY, hovered, tickDelta);
            int textX = (width - renderer.getWidth(text)) / 2;
            int textY = y + (20 - renderer.fontHeight) / 2;
            context.drawTextWithShadow(renderer, text, textX, textY, 0xFFFFFF);
        }

        @Override
        public void reflow(int width, int height) {
            this.width = width;
            if (originalText != null) {
                List<ConfigEntry> children = EntryListWidget.this.children();
                int i = children.indexOf(this);
                EntryListWidget.this.removeEntry(this);
                for (ConfigTextEntry entry : create(renderer, EntryListWidget.this, originalText, width)) {
                    children.add(i++, entry);
                }
            } else {
                EntryListWidget.this.removeEntry(this);
            }
        }
    }

    @Environment(EnvType.CLIENT)
    public static class ConfigReferenceEntry extends ConfigEntry {
        private final ClickableWidget button;

        public ConfigReferenceEntry(int width, Text text, Supplier<Screen> targetScreen) {
            this.button = ButtonWidget.builder(text, btn -> MinecraftClient.getInstance().setScreen(targetScreen.get()))
                    .dimensions(width / 2 - 154, 0, 308, 20)
                    .build();
        }

        @Override
        public List<? extends Selectable> selectableChildren() {
            return List.of(button);
        }

        @Override
        public List<? extends Element> children() {
            return List.of(button);
        }

        @Override
        public void render(DrawContext context, int index, int y, int x, int entryWidth, int entryHeight, int mouseX, int mouseY, boolean hovered, float tickDelta) {
            super.render(context, index, y, x, entryWidth, entryHeight, mouseX, mouseY, hovered, tickDelta);
            button.setY(y);
            button.render(context, mouseX, mouseY, tickDelta);
        }

        @Override
        public void reflow(int width, int height) {
            button.setX(width / 2 - 154);
        }
    }

    @Environment(EnvType.CLIENT)
    public static class ConfigScreenEntry extends ConfigEntry {
        public final ClickableWidget button;
        private final BooleanSupplier resetVisible;
        private final ClickableWidget resetButton;
        private final Text text;
        private final Reflowable reflow;
        private final TextRenderer renderer;

        public ConfigScreenEntry(ClickableWidget button, Text text, BooleanSupplier resetVisible, ClickableWidget resetButton, Reflowable reflow, TextRenderer renderer) {
            this.button = button;
            this.resetVisible = resetVisible;
            this.resetButton = resetButton;
            this.text = text;
            this.reflow = reflow;
            this.renderer = renderer;
        }

        @Override
        public void render(DrawContext context, int index, int y, int x, int entryWidth, int entryHeight, int mouseX, int mouseY, boolean hovered, float tickDelta) {
            super.render(context, index, y, x, entryWidth, entryHeight, mouseX, mouseY, hovered, tickDelta);
            button.setY(y);
            button.render(context, mouseX, mouseY, tickDelta);
            context.drawTextWithShadow(renderer, text, 12, y + 5, 0xFFFFFF);
            if (resetVisible.getAsBoolean()) {
                resetButton.setY(y);
                resetButton.render(context, mouseX, mouseY, tickDelta);
            }
        }

        @Override
        public void reflow(int width, int height) {
            reflow.reflow(width, height);
        }

        @Override
        public List<? extends Element> children() {
            return List.of(button, resetButton);
        }

        @Override
        public List<? extends Selectable> selectableChildren() {
            return List.of(button);
        }

        public Text getText() {
            return text;
        }
    }

    @Environment(EnvType.CLIENT)
    public static class ConfigUnknownEntry extends ConfigEntry {
        private final BooleanSupplier resetVisible;
        private final ClickableWidget resetButton;
        private final Text text;
        private final Reflowable reflow;
        private final TextRenderer renderer;

        public ConfigUnknownEntry(Text text, BooleanSupplier resetVisible, ClickableWidget resetButton, Reflowable reflow, TextRenderer renderer) {
            this.resetVisible = resetVisible;
            this.resetButton = resetButton;
            this.text = text;
            this.reflow = reflow;
            this.renderer = renderer;
        }

        @Override
        public void render(DrawContext context, int index, int y, int x, int entryWidth, int entryHeight, int mouseX, int mouseY, boolean hovered, float tickDelta) {
            super.render(context, index, y, x, entryWidth, entryHeight, mouseX, mouseY, hovered, tickDelta);
            context.drawTextWithShadow(renderer, text, 12, y + 5, 0xFFFFFF);
            if (resetVisible.getAsBoolean()) {
                resetButton.setY(y);
                resetButton.render(context, mouseX, mouseY, tickDelta);
            }
        }

        @Override
        public void reflow(int width, int height) {
            reflow.reflow(width, height);
        }

        @Override
        public List<? extends Element> children() {
            return List.of(resetButton);
        }

        @Override
        public List<? extends Selectable> selectableChildren() {
            return List.of();
        }

        public Text getText() {
            return text;
        }
    }
}
