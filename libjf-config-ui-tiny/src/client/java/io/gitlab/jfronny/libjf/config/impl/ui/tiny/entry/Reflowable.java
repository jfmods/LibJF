package io.gitlab.jfronny.libjf.config.impl.ui.tiny.entry;

public interface Reflowable {
    void reflow(int width, int height);
}
