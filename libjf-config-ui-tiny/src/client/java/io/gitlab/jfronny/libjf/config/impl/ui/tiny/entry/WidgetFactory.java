package io.gitlab.jfronny.libjf.config.impl.ui.tiny.entry;

import io.gitlab.jfronny.libjf.config.impl.ui.tiny.TinyConfigScreen;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.widget.ClickableWidget;

import java.util.function.Consumer;

public interface WidgetFactory<T> {
    Widget<T> build(TinyConfigScreen screen, TextRenderer textRenderer);

    record Widget<T>(WidgetState<T> state, Consumer<T> updateControls, ClickableWidget control, Reflowable reflow) implements Reflowable {
        public Widget {
            state.onUpdateCache(() -> updateControls.accept(state.cachedValue));
        }

        @Override
        public void reflow(int width, int height) {
            reflow.reflow(width, height);
        }
    }
}
