package io.gitlab.jfronny.libjf.config.impl.ui.tiny.entry;

import io.gitlab.jfronny.commons.ref.R;
import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.config.api.v2.EntryInfo;
import io.gitlab.jfronny.libjf.config.api.v2.Naming;
import net.minecraft.text.Text;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class WidgetState<T> {
    public EntryInfo<T> entry;
    public List<WidgetState<?>> knownStates;
    public Naming.Entry naming;

    public Text error;
    public boolean inLimits = true;
    public String tempValue;
    public boolean managedTemp = true;
    public T cachedValue;
    @Nullable public WidgetFactory factory;

    private Runnable onUpdateCache = R::nop;

    public void initialize(EntryInfo<T> entry, List<WidgetState<?>> knownStates, @Nullable WidgetFactory factory, Naming.Entry naming) {
        this.entry = entry;
        this.knownStates = knownStates;
        this.factory = factory;
        this.naming = naming;
        updateCache();
    }

    public void updateCache() {
        try {
            updateCache(entry.getValue());
        } catch (IllegalAccessException e) {
            LibJf.LOGGER.error("Could not set widget state cache to current value", e);
        }
    }

    public void updateCache(T newValue) {
        cachedValue = newValue;
        if (managedTemp) tempValue = newValue == null ? null : newValue.toString();
        onUpdateCache.run();
    }

    public void onUpdateCache(Runnable action) {
        Runnable previous = onUpdateCache;
        onUpdateCache = () -> {
            previous.run();
            action.run();
        };
    }

    public void writeToEntry() throws IllegalAccessException {
        entry.setValue(cachedValue);
    }

    public void reset() {
        cachedValue = entry.getDefault();
        onUpdateCache.run();
    }
}
