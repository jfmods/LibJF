package io.gitlab.jfronny.libjf.config.impl.ui.tiny.presets;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.*;
import net.minecraft.client.gui.widget.ClickableWidget;
import net.minecraft.client.gui.widget.ElementListWidget;
import net.minecraft.client.util.math.MatrixStack;

import java.util.List;

public class PresetListWidget extends ElementListWidget<PresetListWidget.PresetEntry> {
    public PresetListWidget(MinecraftClient client, int width, int height, int top, int itemHeight) {
        super(client, width, height, top, itemHeight);
    }

    public void addButton(ClickableWidget button) {
        addEntry(new PresetEntry(button));
    }

    @Override
    protected int getScrollbarX() {
        return this.width - 7;
    }

    @Override
    public int getRowWidth() {
        return 10000;
    }

    public static class PresetEntry extends Entry<PresetEntry> {
        private final ClickableWidget button;
        public PresetEntry(ClickableWidget button) {
            this.button = button;
        }

        @Override
        public List<? extends Selectable> selectableChildren() {
            return List.of(button);
        }

        @Override
        public List<? extends Element> children() {
            return List.of(button);
        }

        @Override
        public void render(DrawContext context, int index, int y, int x, int entryWidth, int entryHeight, int mouseX, int mouseY, boolean hovered, float tickDelta) {
            button.setY(y);
            button.render(context, mouseX, mouseY, tickDelta);
        }
    }
}
