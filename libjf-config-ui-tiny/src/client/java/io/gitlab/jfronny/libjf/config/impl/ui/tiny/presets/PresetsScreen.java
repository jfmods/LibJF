package io.gitlab.jfronny.libjf.config.impl.ui.tiny.presets;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.config.api.v2.ConfigCategory;
import io.gitlab.jfronny.libjf.config.api.v2.Naming;
import io.gitlab.jfronny.libjf.config.api.v2.dsl.CategoryBuilder;
import io.gitlab.jfronny.libjf.config.impl.ConfigCore;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.text.Text;

import java.util.Map;
import java.util.Objects;

@Environment(EnvType.CLIENT)
public class PresetsScreen extends Screen {
    private final Screen parent;
    private final ConfigCategory config;
    private final Naming naming;
    private final Runnable afterSelect;

    public PresetsScreen(Screen parent, ConfigCategory config, Naming naming, Runnable afterSelect) {
        super(Text.translatable(ConfigCore.MOD_ID + ".presets"));
        this.parent = parent;
        this.config = config;
        this.naming = naming;
        this.afterSelect = afterSelect;
    }

    @Override
    protected void init() {
        super.init();
        PresetListWidget list = new PresetListWidget(this.client, this.width, this.height, 32, 25);
        for (Map.Entry<String, Runnable> entry : config.getPresets().entrySet()) {
            list.addButton(ButtonWidget.builder(CategoryBuilder.CONFIG_PRESET_DEFAULT.equals(entry.getKey()) ? Text.translatable(entry.getKey()) : naming.preset(entry.getKey()),
                    button -> {
                        LibJf.LOGGER.info("Preset selected: " + entry.getKey());
                        entry.getValue().run();
                        config.fix();
                        afterSelect.run();
                        Objects.requireNonNull(client).setScreen(parent);
                    })
                    .dimensions(width / 2 - 100, 0, 200, 20)
                    .build());
        }
        this.addDrawableChild(list);
    }

    @Override
    public void close() {
        Objects.requireNonNull(client).setScreen(parent);
    }

    @Override
    public void render(DrawContext context, int mouseX, int mouseY, float delta) {
        super.render(context, mouseX, mouseY, delta);
        context.drawCenteredTextWithShadow(textRenderer, title, width / 2, 16 - textRenderer.fontHeight / 2, 0xFFFFFF);
    }
}
