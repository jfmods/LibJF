package io.gitlab.jfronny.libjf.config.test.tiny;

import io.gitlab.jfronny.commons.data.String2ObjectMap;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeToken;
import io.gitlab.jfronny.libjf.config.api.v2.JfCustomConfig;
import io.gitlab.jfronny.libjf.config.api.v2.dsl.DSL;
import io.gitlab.jfronny.libjf.config.api.v2.type.Type;

public class TestConfig implements JfCustomConfig {
    private int value1 = 0;
    private double doubleValue = 0.3;
    private double doubleValue2 = 0.3;
    private String value2 = "";
    private boolean value3 = false;
    private int value4 = 0;
    private String value5 = "";
    private boolean value6 = false;
    private String2ObjectMap<String> map = new String2ObjectMap<>();

    @Override
    public void register(DSL.Defaulted dsl) {
        dsl.register("libjf-config-ui-tiny-testmod", builder -> builder
                .category("ca1", builder1 -> builder1
                        .value("value1", value1, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, () -> value1, v -> value1 = v)
                        .value("doubleValue", doubleValue, -0.74, 1.6, () -> doubleValue, v -> doubleValue = v)
                        .category("nestedCa", builder2 -> builder2.value("doubleValue", doubleValue2, 12, 47, () -> doubleValue2, v -> doubleValue2 = v))
                ).category("ca2", builder1 -> builder1
                        .value("value2", value2, () -> value2, v -> value2 = v)
                        .value("value3", value3, () -> value3, v -> value3 = v)
                ).category("ca3", builder1 -> builder1
                        .value("value3", value4, -5, 12, () -> value4, v -> value4 = v)
                ).category("ca4", builder1 -> builder1
                        .value("value4", value5, () -> value5, v -> value5 = v)
                ).category("ca5", builder1 -> builder1
                        .value("value5", value6, () -> value6, v -> value6 = v)
                ).category("ca6", builder1 -> builder1
                        .value("mappy", map, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, Type.ofToken(new TypeToken<String2ObjectMap<String>>() {}), 100, () -> map, v -> map = v)
                )
        );
    }
}
