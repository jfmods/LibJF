import io.gitlab.jfronny.scripts.*

plugins {
    id("jfmod.module")
}

base {
    archivesName = "libjf-data-manipulation-v0"
}

dependencies {
    api(devProject(":libjf-base"))
    api(devProject(":libjf-unsafe-v0"))
    modApi("net.fabricmc.fabric-api:fabric-api-base")
}
