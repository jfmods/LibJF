package io.gitlab.jfronny.libjf.data.manipulation.api;

import net.minecraft.util.Identifier;

import java.util.HashSet;
import java.util.Set;

@SuppressWarnings("unused")
public class RecipeUtil {
    private static final Set<Identifier> REMOVAL_BY_ID = new HashSet<>();

    public static void removeRecipe(Identifier identifier) {
    	REMOVAL_BY_ID.add(identifier);
    }

    public static boolean isIdBlocked(Identifier identifier) {
    	return REMOVAL_BY_ID.contains(identifier);
    }
}
