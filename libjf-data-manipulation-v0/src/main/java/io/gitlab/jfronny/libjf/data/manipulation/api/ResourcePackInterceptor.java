package io.gitlab.jfronny.libjf.data.manipulation.api;

import io.gitlab.jfronny.commons.concurrent.ScopedValue;
import io.gitlab.jfronny.commons.throwable.ExceptionWrapper;
import io.gitlab.jfronny.commons.throwable.ThrowingRunnable;
import io.gitlab.jfronny.commons.throwable.ThrowingSupplier;
import io.gitlab.jfronny.libjf.data.manipulation.impl.ResourcePackHook;
import net.minecraft.resource.InputSupplier;
import net.minecraft.resource.ResourcePack;
import net.minecraft.resource.ResourceType;
import net.minecraft.resource.metadata.ResourceMetadataSerializer;
import net.minecraft.util.Identifier;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Supplier;

/**
 * An interceptor for resource pack operations.
 * Register as a libjf:resource_pack_interceptor entrypoint.
 */
public interface ResourcePackInterceptor {
    static <TVal, TEx extends Throwable> TVal disable(ThrowingSupplier<TVal, TEx> then) throws TEx {
        try {
            return io.gitlab.jfronny.commons.concurrent.ScopedValue.getWhere(ResourcePackHook.DISABLED, true, then.orThrow());
        } catch (ExceptionWrapper ew) {
            throw (TEx) ExceptionWrapper.unwrap(ew);
        }
    }

    static <TEx extends Throwable> void disable(ThrowingRunnable<TEx> then) throws TEx {
        try {
            ScopedValue.runWhere(ResourcePackHook.DISABLED, true, then.orThrow());
        } catch (ExceptionWrapper ew) {
            throw (TEx) ExceptionWrapper.unwrap(ew);
        }
    }

    default InputSupplier<InputStream> openRoot(String[] fileName, InputSupplier<InputStream> previous, ResourcePack pack) {
        return previous;
    }

    default InputSupplier<InputStream> open(ResourceType type, Identifier id, InputSupplier<InputStream> previous, ResourcePack pack) {
        return previous;
    }

    default ResourcePack.ResultConsumer findResources(ResourceType type, String namespace, String prefix, ResourcePack.ResultConsumer previous, ResourcePack pack) {
        return previous;
    }

    default <T> T parseMetadata(ResourceMetadataSerializer<T> reader, Supplier<T> previous, ResourcePack pack) throws IOException {
        return previous.get();
    }
}
