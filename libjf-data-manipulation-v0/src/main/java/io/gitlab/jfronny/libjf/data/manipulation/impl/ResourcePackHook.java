package io.gitlab.jfronny.libjf.data.manipulation.impl;

import io.gitlab.jfronny.commons.LazySupplier;
import io.gitlab.jfronny.commons.concurrent.ScopedValue;
import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.data.manipulation.api.ResourcePackInterceptor;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.resource.*;
import net.minecraft.resource.metadata.ResourceMetadataSerializer;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.ApiStatus;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@SuppressWarnings("unused")
@ApiStatus.Internal
public class ResourcePackHook {
    @ApiStatus.Internal
    public static final ScopedValue<Boolean> DISABLED = new ScopedValue<>();
    private static final List<ResourcePackInterceptor> INTERCEPTORS = FabricLoader.getInstance()
            .getEntrypoints(LibJf.MOD_ID + ":resource_pack_interceptor", ResourcePackInterceptor.class)
            .stream()
            .toList();

    private static boolean isDisabled() {
        Boolean b = DISABLED.orElse(false);
        return b != null && b; // this null check should not be needed, but it's here just in case
    }

    public static InputSupplier<InputStream> hookOpenRoot(InputSupplier<InputStream> value, ResourcePack pack, String[] fileName) {
        if (isDisabled()) return value;
        for (ResourcePackInterceptor interceptor : INTERCEPTORS) {
            value = interceptor.openRoot(fileName, value, pack);
        }
        return value;
    }

    public static InputSupplier<InputStream> hookOpen(InputSupplier<InputStream> value, ResourcePack pack, ResourceType type, Identifier id) {
        if (isDisabled()) return value;
        for (ResourcePackInterceptor interceptor : INTERCEPTORS) {
            value = interceptor.open(type, id, value, pack);
        }
        return value;
    }

    public static ResourcePack.ResultConsumer hookFindResources(ResourcePack pack, ResourceType type, String namespace, String prefix, ResourcePack.ResultConsumer target) {
        if (isDisabled()) return target;
        for (ResourcePackInterceptor interceptor : INTERCEPTORS.reversed()) {
            target = interceptor.findResources(type, namespace, prefix, target, pack);
        }
        return target;
    }

    public static <T> T hookParseMetadata(T value, ResourcePack pack, ResourceMetadataSerializer<T> reader) throws IOException {
        if (isDisabled()) return value;
        LazySupplier<T> lazy = new LazySupplier<>(value);
        for (ResourcePackInterceptor interceptor : INTERCEPTORS) {
            lazy = lazy.andThen(supplier -> {
                try {
                    return interceptor.parseMetadata(reader, supplier, pack);
                } catch (IOException e) {
                    LibJf.LOGGER.error("Could not call ResourcePack.OPEN_ROOT listener", e);
                    return null;
                }
            });
        }
        return lazy.get();
    }

    public static <T> T hookParseMetadataEx(IOException ex, ResourcePack pack, ResourceMetadataSerializer<T> reader) throws IOException {
        if (isDisabled()) throw ex;
        try {
            return hookParseMetadata(null, pack, reader);
        } catch (Throwable t) {
            ex.addSuppressed(t);
            throw ex;
        }
    }
}
