package io.gitlab.jfronny.libjf.data.manipulation.impl.mixin;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.data.manipulation.api.RecipeUtil;
import net.minecraft.recipe.Recipe;
import net.minecraft.recipe.ServerRecipeManager;
import net.minecraft.util.Identifier;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import java.util.*;
import java.util.function.BiConsumer;

@Mixin(ServerRecipeManager.class)
public class RecipeManagerMixin {
    @Redirect(method = "prepare(Lnet/minecraft/resource/ResourceManager;Lnet/minecraft/util/profiler/Profiler;)Lnet/minecraft/recipe/PreparedRecipes;", at = @At(value = "INVOKE", target = "Ljava/util/SortedMap;forEach(Ljava/util/function/BiConsumer;)V", remap = false))
    private void filterIterator(SortedMap<Identifier, Recipe<?>> instance, BiConsumer<Identifier, Recipe<?>> biConsumer) {
        instance.forEach((id, recipe) -> {
            if (RecipeUtil.isIdBlocked(id)) {
                LibJf.LOGGER.info("Blocking recipe by identifier: " + id);
            } else {
                biConsumer.accept(id, recipe);
            }
        });
    }
}
