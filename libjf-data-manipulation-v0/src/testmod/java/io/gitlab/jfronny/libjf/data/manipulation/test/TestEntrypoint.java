package io.gitlab.jfronny.libjf.data.manipulation.test;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.data.manipulation.api.ResourcePackInterceptor;
import net.minecraft.resource.*;
import net.minecraft.util.Identifier;

import java.io.InputStream;

public class TestEntrypoint implements ResourcePackInterceptor {
    @Override
    public InputSupplier<InputStream> open(ResourceType type, Identifier id, InputSupplier<InputStream> previous, ResourcePack pack) {
        if (pack instanceof DirectoryResourcePack) {
            LibJf.LOGGER.info(pack.getInfo().title() + " opened " + type.name() + "/" + id.toString());
            return null;
        }
        return previous;
    }
}
