import io.gitlab.jfronny.scripts.*

plugins {
    id("jfmod.module")
}

base {
    archivesName = "libjf-data-v0"
}

dependencies {
    api(devProject(":libjf-base"))
    include("net.fabricmc.fabric-api:fabric-resource-loader-v0")
}
