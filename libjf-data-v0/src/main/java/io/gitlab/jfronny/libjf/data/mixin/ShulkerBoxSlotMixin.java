package io.gitlab.jfronny.libjf.data.mixin;

import io.gitlab.jfronny.libjf.data.Tags;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.slot.ShulkerBoxSlot;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(ShulkerBoxSlot.class)
public class ShulkerBoxSlotMixin {
    /**
     * @reason Required for custom blocked items to work properly.
     * None of the original content of the method is useful and may actually break this
     * Any other mod modifying this will have the same intention and collisions would occur
     * If that ever happens a library should be created but for now this works
     * @author JFronny
     */
    @Overwrite
    public boolean canInsert(ItemStack stack) {
        return !stack.isIn(Tags.SHULKER_ILLEGAL);
    }
}
