import io.gitlab.jfronny.scripts.*

plugins {
    id("jfmod.module")
}

jfModule {
    devOnly = true
}

base {
    archivesName = "libjf-devutil"
}

dependencies {
    api(devProject(":libjf-base"))
}
