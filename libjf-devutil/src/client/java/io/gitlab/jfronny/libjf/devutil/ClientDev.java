package io.gitlab.jfronny.libjf.devutil;

import net.minecraft.client.MinecraftClient;

public class ClientDev {
    public static String onDebug() {
        MinecraftClient mc = MinecraftClient.getInstance();
        if (mc == null) return "MinecraftClient is null";
        mc.mouse.unlockCursor();
        return "Mouse unlocked";
    }
}
