package io.gitlab.jfronny.libjf.devutil.mixin;

import com.mojang.authlib.minecraft.UserApiService;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.RunArgs;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(MinecraftClient.class)
public class MinecraftClientMixin {
    /**
     * @author JFronny
     * @reason The social interactions service will not work in dev envs and only produces log spam
     */
    @Overwrite
    private UserApiService createUserApiService(YggdrasilAuthenticationService yggdrasilAuthenticationService, RunArgs runArgs) {
        return UserApiService.OFFLINE;
    }
}
