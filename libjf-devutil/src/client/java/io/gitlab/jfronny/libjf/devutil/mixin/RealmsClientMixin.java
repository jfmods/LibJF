package io.gitlab.jfronny.libjf.devutil.mixin;

import net.minecraft.client.realms.*;
import net.minecraft.client.realms.exception.RealmsServiceException;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(RealmsClient.class)
public class RealmsClientMixin {
    /**
     * @author JFronny
     * @reason Prevent crash log
     */
    @Overwrite
    public RealmsClient.CompatibleVersionResponse clientCompatible() throws RealmsServiceException {
        return RealmsClient.CompatibleVersionResponse.OTHER;
    }
}
