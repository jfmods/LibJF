package io.gitlab.jfronny.libjf.devutil.mixin;

import net.minecraft.server.MinecraftServer;
import net.minecraft.server.WorldGenerationProgressListener;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(MinecraftServer.class)
public class MinecraftServerMixin {
    /**
     * Taken from mod-fungible
     * @author magistermaks
     * @reason Skip generating spawn chunks
     */
    @Overwrite
    private void prepareStartRegion(WorldGenerationProgressListener worldGenerationProgressListener) {
        // do nothing
    }
}
