package io.gitlab.jfronny.libjf.mainhttp.api.v0;

import org.jetbrains.annotations.Nullable;

public interface MainHttpHandler {
    default boolean isActive() {
        return true;
    }

    byte @Nullable [] handle(byte[] request);
}
