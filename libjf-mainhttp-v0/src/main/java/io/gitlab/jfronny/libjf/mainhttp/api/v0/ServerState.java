package io.gitlab.jfronny.libjf.mainhttp.api.v0;

import io.gitlab.jfronny.libjf.mainhttp.impl.MainHttp;

public interface ServerState {
    static void onActivate(Runnable listener) {
        MainHttp.ON_ACTIVATE.add(listener);
    }

    static boolean isActive() {
        return !MainHttp.GAME_PORT.isEmpty();
    }

    static int getPort() {
        return MainHttp.GAME_PORT.getTopmost();
    }
}
