package io.gitlab.jfronny.libjf.mainhttp.impl;

import io.gitlab.jfronny.libjf.mainhttp.api.v0.MainHttpHandler;
import io.gitlab.jfronny.libjf.mainhttp.impl.util.ClaimPool;
import net.fabricmc.loader.api.FabricLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class MainHttp {
    public static final List<Runnable> ON_ACTIVATE = new ArrayList<>();
    public static final ClaimPool<Integer> GAME_PORT = new ClaimPool<>();
    public static final String MOD_ID = "libjf-mainhttp";
    public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);
    private static final List<MainHttpHandler> activeHandlers = FabricLoader.getInstance()
            .getEntrypoints(MOD_ID + ":v0", MainHttpHandler.class)
            .stream()
            .filter(MainHttpHandler::isActive)
            .toList();
    public static final byte[] NOT_FOUND = """
            HTTP/1.1 404 Not Found
            Connection: keep-alive
            Content-Length: 0
            """.getBytes();

    public static boolean isEnabled() {
        return !activeHandlers.isEmpty();
    }

    public static byte[] handle(byte[] request) {
        for (MainHttpHandler handler : activeHandlers) {
            byte[] option = handler.handle(request);
            if (option != null) return option;
        }
        return NOT_FOUND;
    }
}
