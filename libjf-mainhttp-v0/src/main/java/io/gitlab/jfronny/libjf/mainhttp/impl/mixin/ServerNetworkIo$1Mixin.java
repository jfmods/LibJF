package io.gitlab.jfronny.libjf.mainhttp.impl.mixin;

import io.gitlab.jfronny.libjf.mainhttp.impl.HttpDecoder;
import io.netty.channel.Channel;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(targets = "net.minecraft.server.ServerNetworkIo$1")
public class ServerNetworkIo$1Mixin {
    @Inject(method = "initChannel(Lio/netty/channel/Channel;)V", at = @At(value = "INVOKE", target = "Lnet/minecraft/network/ClientConnection;addHandlers(Lio/netty/channel/ChannelPipeline;Lnet/minecraft/network/NetworkSide;ZLnet/minecraft/network/handler/PacketSizeLogger;)V"))
    private void inject(Channel channel, CallbackInfo ci) {
        // directly after legacy_query
        channel.pipeline().addLast("libjf_http", new HttpDecoder());
    }
}
