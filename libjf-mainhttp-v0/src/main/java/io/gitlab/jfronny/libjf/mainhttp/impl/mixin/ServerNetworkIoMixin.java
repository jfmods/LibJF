package io.gitlab.jfronny.libjf.mainhttp.impl.mixin;

import io.gitlab.jfronny.libjf.mainhttp.impl.MainHttp;
import io.gitlab.jfronny.libjf.mainhttp.impl.util.ClaimPool;
import net.minecraft.server.ServerNetworkIo;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.net.InetAddress;
import java.util.HashSet;
import java.util.Set;

@Mixin(ServerNetworkIo.class)
public class ServerNetworkIoMixin {
    @Unique
    private final Set<ClaimPool<Integer>.Claim> libjf$portClaim = new HashSet<>();

    @Inject(method = "bind(Ljava/net/InetAddress;I)V", at = @At("HEAD"))
    void onBind(InetAddress address, int port, CallbackInfo ci) {
        libjf$portClaim.add(MainHttp.GAME_PORT.claim(port));
        for (Runnable runnable : MainHttp.ON_ACTIVATE) runnable.run();
    }

    @Inject(method = "stop()V", at = @At("HEAD"))
    void onStop(CallbackInfo ci) {
        for (ClaimPool<Integer>.Claim claim : libjf$portClaim) claim.release();
    }
}
