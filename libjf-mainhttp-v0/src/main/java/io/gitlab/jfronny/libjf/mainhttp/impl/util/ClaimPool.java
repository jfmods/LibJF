package io.gitlab.jfronny.libjf.mainhttp.impl.util;

import java.util.Objects;

public class ClaimPool<T> {
    private Claim top = null;

    public Claim claim(T value) {
        synchronized (this) {
            return top = new Claim(value, top);
        }
    }

    public T getTopmost() {
        Claim tp = top;
        return tp == null ? null : tp.value;
    }

    public boolean isEmpty() {
        return top == null;
    }

    public class Claim {
        private Claim prev, next;
        private final T value;
        private boolean active = true;

        private Claim(T value, Claim prev) {
            // This is synchronized on the pool, so we can safely read top
            this.value = value;
            if (prev != null) {
                this.prev = prev;
                prev.next = this;
            }
        }

        public void release() {
            // By synchronizing from left to right, we ensure that only claims to our left can be blocked by this claim,
            // meaning that the rightmost claim can always be released, preventing deadlocks

            // If there is no previous object, prev cannot change, so we can safely read it without synchronization
            // (which is effectively what is caused by synchronizing on this twice)
            synchronized (Objects.requireNonNullElse(prev, this)) {
                synchronized (this) {
                    // If there is no next object, ClaimPool might add a new one, so we need to synchronize on it
                    synchronized (Objects.requireNonNullElse(next, ClaimPool.this)) {
                        if (!active) throw new UnsupportedOperationException("Cannot release claim that is already released");
                        active = false;
                        if (prev != null) prev.next = next;
                        if (next != null) next.prev = prev;
                        if (top == this) top = prev;
                    }
                }
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("ClaimPool{");
        Claim current = top;
        while (current != null) {
            sb.append(current.value).append(" -> ");
            current = current.prev;
        }
        return sb.append('}').toString();
    }
}