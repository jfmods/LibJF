package io.gitlab.jfronny.libjf.mainhttp.impl.util;

import it.unimi.dsi.fastutil.bytes.Byte2ObjectArrayMap;
import it.unimi.dsi.fastutil.bytes.Byte2ObjectMap;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Trie<T> {
    public final Byte2ObjectMap<Trie<T>> next;
    public T content;

    public Trie() {
        this.next = new Byte2ObjectArrayMap<>();
    }

    public void add(Map<String, T> next) {
        next.forEach(this::add);
    }

    public void add(String key, T value) {
        if (key.isEmpty()) this.content = value;
        char c = key.charAt(0);
        if (c > 255) throw new IllegalArgumentException("Invalid character: " + c);
        else this.next.computeIfAbsent((byte) c, k -> new Trie<>())
                .add(key.substring(1), value);
    }

    public static <T> Trie<T> of(Map<String, T> source) {
        Trie<T> root = new Trie<>();
        root.add(source);
        return root;
    }

    public static Trie<String> of(List<String> source) {
        return of(source.stream().collect(Collectors.toMap(Function.identity(), Function.identity())));
    }
}
