plugins {
    id("jfmod.module")
}

base {
    archivesName = "libjf-resource-pack-entry-widgets-v0"
}

dependencies {
    modImplementation("net.fabricmc.fabric-api:fabric-api-base")
}
