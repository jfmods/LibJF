package io.gitlab.jfronny.libjf.entrywidgets.api.v0;

import io.gitlab.jfronny.libjf.entrywidgets.impl.EntrypointComparator;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.entrypoint.EntrypointContainer;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.pack.ResourcePackOrganizer;

import java.util.List;

/**
 * Represents an additional widget inserted on the right hand side of a resource pack entry (in the resource pack or data pack screen)
 */
public interface ResourcePackEntryWidget {
    /**
     * Lists all known widgets from right to left. Immutable.
     */
    List<ResourcePackEntryWidget> WIDGETS = FabricLoader.getInstance()
            .getEntrypointContainers("libjf:resource_pack_entry_widget", ResourcePackEntryWidget.class)
            .stream()
            .sorted(new EntrypointComparator())
            .map(EntrypointContainer::getEntrypoint)
            .toList();

    /**
     * Checks whether the widget should be rendered for a given pack.
     *
     * @param pack the pack to render the widget for
     * @param selectable whether the pack is selectable
     * @return whether the widget is visible
     */
    default boolean isVisible(ResourcePackOrganizer.Pack pack, boolean selectable) {
        return true;
    }

    /**
     * Gets the width of this widget.
     *
     * @param pack the pack to render the widget for
     * @return the width of the widget
     */
    int getWidth(ResourcePackOrganizer.Pack pack);

    /**
     * Gets the height of this widget.
     *
     * @param pack the pack to render the widget for
     * @param rowHeight the height of the row containing the widget
     * @return the height of the widget
     */
    int getHeight(ResourcePackOrganizer.Pack pack, int rowHeight);

    /**
     * Gets the Y position of this widget relative to the top of the row.
     *
     * @param pack the pack to render the widget for
     * @param rowHeight the height of the row containing the widget
     * @return the relative y position of the widget
     */
    default int getY(ResourcePackOrganizer.Pack pack, int rowHeight) {
        return (rowHeight - getHeight(pack, rowHeight)) / 2;
    }

    /**
     * Gets the X margin of the widget.
     * Two widgets will be separated by the maximum of their X margins.
     * Also, the rightmost widget will be separated from the edge of the entry by its margin.
     *
     * @param pack the pack to render the widget for
     * @return the X margin of the widget
     */
    default int getXMargin(ResourcePackOrganizer.Pack pack) {
        return 7;
    }

    /**
     * Renders the widget.
     *
     * @param pack      the pack to render the widget for
     * @param context   the context to draw to
     * @param x         the absolute x coordinate at which to draw
     * @param y         the absolute y coordinate at which to draw
     * @param hovered   whether the widget is being hovered by the cursor
     * @param tickDelta the time that has passed since the last call
     */
    void render(ResourcePackOrganizer.Pack pack, DrawContext context, int x, int y, boolean hovered, float tickDelta);

    /**
     * Executed when a widget is clicked.
     *
     * @param pack the pack for which the widget was clicked
     */
    void onClick(ResourcePackOrganizer.Pack pack);
}
