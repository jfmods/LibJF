package io.gitlab.jfronny.libjf.entrywidgets.impl;

import io.gitlab.jfronny.libjf.entrywidgets.api.v0.ResourcePackEntryWidget;
import net.fabricmc.loader.api.entrypoint.EntrypointContainer;

import java.util.Comparator;
import java.util.function.Function;

/**
 * Shuffles the order of widgets.
 * Although the order is (intentionally) not guaranteed, it is consistent.
 */
public class EntrypointComparator implements Comparator<EntrypointContainer<ResourcePackEntryWidget>> {
    @Override
    public int compare(EntrypointContainer<ResourcePackEntryWidget> o1, EntrypointContainer<ResourcePackEntryWidget> o2) {
        return compareByValues(o1, o2,
                s -> s.getProvider().getMetadata().getId().hashCode(),
                s -> s.getProvider().getMetadata().getId()
        );
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @SafeVarargs
    private static <T> int compareByValues(T left, T right, Function<T, Comparable>... extractors) {
        for (Function<T, Comparable> extractor : extractors) {
            int result = extractor.apply(left).compareTo(extractor.apply(right));
            if (result != 0) {
                return result;
            }
        }
        return 0;
    }
}
