package io.gitlab.jfronny.libjf.entrywidgets.impl;

import io.gitlab.jfronny.libjf.entrywidgets.impl.mixin.RecomputeWrapping;
import io.gitlab.jfronny.libjf.entrywidgets.impl.mixin.RenderWidgets;

/**
 * Shares access to values between the various mixins.
 */
public interface IResourcePackEntry {
    /**
     * Contains the inset caused by the widgets.
     * Set in {@link RecomputeWrapping#libjf$recomputeWrapping()}.
     */
    ScopedValue<Integer> widgetInset = new ScopedValue<>();

    /**
     * Recomputes the wrapping of the entry widgets.
     * Implemented in {@link RecomputeWrapping}.
     */
    void libjf$recomputeWrapping();

    /**
     * Gets the inset caused by the widgets.
     * Implemented in {@link RenderWidgets}.
     */
    int libjf$getWidgetInset();
}
