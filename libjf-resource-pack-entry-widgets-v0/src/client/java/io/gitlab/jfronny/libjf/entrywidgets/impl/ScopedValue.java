package io.gitlab.jfronny.libjf.entrywidgets.impl;

import java.util.*;

/**
 * Emulates a limited subset of the JVM ScopedValue API while that is still in preview.
 * WILL be removed once it is stable.
 */
public class ScopedValue<T> {
    public static <T> void runWhere(ScopedValue<T> key, T value, Runnable op) {
        key.value.get().setValue(value);
        try {
            op.run();
        } finally {
            key.value.get().clear();
        }
    }

    private final InheritableThreadLocal<Carrier<T>> value = new InheritableThreadLocal<>() {
        @Override
        protected Carrier<T> childValue(Carrier<T> parentValue) {
            return new Carrier<>(parentValue);
        }

        @Override
        protected Carrier<T> initialValue() {
            return new Carrier<>();
        }
    };

    public T orElse(T other) {
        return value.get().orElse(other);
    }

    private static class Carrier<T> {
        private final Deque<T> value;
        private final Carrier<T> parent;

        public Carrier() {
            this(null);
        }

        public Carrier(Carrier<T> parent) {
            this.value = new LinkedList<>();
            this.parent = parent;
        }

        public void setValue(T value) {
            synchronized (this.value) {
                this.value.push(value);
            }
        }

        public void clear() {
            synchronized (this.value) {
                value.pop();
            }
        }

        public T orElse(T other) {
            synchronized (value) {
                if (!value.isEmpty()) return value.peek();
                if (parent == null) return other;
                return parent.orElse(other);
            }
        }
    }
}
