package io.gitlab.jfronny.libjf.entrywidgets.impl.mixin;

import io.gitlab.jfronny.libjf.entrywidgets.impl.IResourcePackEntry;
import net.minecraft.client.font.MultilineText;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.screen.pack.PackListWidget;
import net.minecraft.text.StringVisitable;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

/**
 * Encapsulates the logic for adjusting the wrapping based on the computed inset.
 */
@Mixin(PackListWidget.ResourcePackEntry.class)
public abstract class AdjustWrapping implements IResourcePackEntry {
    @Redirect(method = "trimTextToWidth(Lnet/minecraft/client/MinecraftClient;Lnet/minecraft/text/Text;)Lnet/minecraft/text/OrderedText;", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/font/TextRenderer;getWidth(Lnet/minecraft/text/StringVisitable;)I"))
    private static int adjustComputedWidth(TextRenderer instance, StringVisitable text) {
        return instance.getWidth(text) + getInset();
    }

    @Redirect(method = "createMultilineText(Lnet/minecraft/client/MinecraftClient;Lnet/minecraft/text/Text;)Lnet/minecraft/client/font/MultilineText;", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/font/MultilineText;create(Lnet/minecraft/client/font/TextRenderer;II[Lnet/minecraft/text/Text;)Lnet/minecraft/client/font/MultilineText;"))
    private static MultilineText adjustTrimWidth(TextRenderer renderer, int maxWidth, int maxLines, Text[] texts) {
        return MultilineText.create(renderer, maxWidth - getInset(), maxLines, texts);
    }

    @Unique private static int getInset() {
        return widgetInset.orElse(10) - 10;
    }
}
