package io.gitlab.jfronny.libjf.entrywidgets.impl.mixin;

import io.gitlab.jfronny.libjf.entrywidgets.impl.IResourcePackEntry;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.MultilineText;
import net.minecraft.client.gui.screen.pack.PackListWidget;
import net.minecraft.client.gui.screen.pack.ResourcePackOrganizer;
import net.minecraft.text.OrderedText;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.*;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.*;

import static io.gitlab.jfronny.libjf.entrywidgets.impl.ScopedValue.runWhere;

/**
 * Encapsulates the logic for caching the original text sources and recompute the wrapped texts based on the computed inset.
 */
@Mixin(PackListWidget.ResourcePackEntry.class)
public abstract class RecomputeWrapping implements IResourcePackEntry {
    @Shadow @Final @Mutable private OrderedText displayName;
    @Shadow @Final @Mutable private MultilineText description;
    @Shadow @Final @Mutable private OrderedText incompatibleText;
    @Shadow @Final @Mutable private MultilineText compatibilityNotificationText;

    @Shadow private static OrderedText trimTextToWidth(MinecraftClient client, Text text) {
        throw new IllegalStateException("Mixin failed to apply");
    }

    @Shadow private static MultilineText createMultilineText(MinecraftClient client, Text text) {
        throw new IllegalStateException("Mixin failed to apply");
    }

    @Unique private static final Map<OrderedText, Text> trimmedTextCache = new HashMap<>();
    @Inject(method = "trimTextToWidth(Lnet/minecraft/client/MinecraftClient;Lnet/minecraft/text/Text;)Lnet/minecraft/text/OrderedText;", at = @At("TAIL"))
    private static void grabTrimmedText(MinecraftClient client, Text text, CallbackInfoReturnable<OrderedText> cir) {
        trimmedTextCache.put(cir.getReturnValue(), text);
    }

    @Unique private static final Map<MultilineText, Text> multilineTextCache = new HashMap<>();
    @Inject(method = "createMultilineText(Lnet/minecraft/client/MinecraftClient;Lnet/minecraft/text/Text;)Lnet/minecraft/client/font/MultilineText;", at = @At("TAIL"))
    private static void grabMultilineText(MinecraftClient client, Text text, CallbackInfoReturnable<MultilineText> cir) {
        multilineTextCache.put(cir.getReturnValue(), text);
    }

    @Unique private Text originalDisplayName;
    @Unique private Text originalDescription;
    @Unique private Text originalIncompatibleText;
    @Unique private Text originalCompatibilityNotificationText;
    @Inject(method = "<init>(Lnet/minecraft/client/MinecraftClient;Lnet/minecraft/client/gui/screen/pack/PackListWidget;Lnet/minecraft/client/gui/screen/pack/ResourcePackOrganizer$Pack;)V", at = @At("TAIL"))
    private void cacheTextSources(MinecraftClient client, PackListWidget widget, ResourcePackOrganizer.Pack pack, CallbackInfo ci) {
        originalDisplayName = Optional.ofNullable(trimmedTextCache.get(displayName)).orElseGet(pack::getDisplayName);
        originalDescription = Optional.ofNullable(multilineTextCache.get(description)).orElseGet(pack::getDecoratedDescription);
        originalIncompatibleText = Optional.ofNullable(trimmedTextCache.get(incompatibleText)).orElseGet(() -> Text.translatable("pack.incompatible"));
        originalCompatibilityNotificationText = Optional.ofNullable(multilineTextCache.get(compatibilityNotificationText)).orElseGet(() -> pack.getCompatibility().getNotification());
        trimmedTextCache.clear();
        multilineTextCache.clear();
    }

    @Shadow @Final protected MinecraftClient client;
    @Unique private int libjf$widthCache = -1;
    @Override
    public void libjf$recomputeWrapping() {
        int width = libjf$getWidgetInset();
        if (width == libjf$widthCache) return;
        libjf$widthCache = width;

        runWhere(widgetInset, width, () -> {
            displayName = trimTextToWidth(client, originalDisplayName);
            description = createMultilineText(client, originalDescription);
            incompatibleText = trimTextToWidth(client, originalIncompatibleText);
            compatibilityNotificationText = createMultilineText(client, originalCompatibilityNotificationText);
        });
    }
}
