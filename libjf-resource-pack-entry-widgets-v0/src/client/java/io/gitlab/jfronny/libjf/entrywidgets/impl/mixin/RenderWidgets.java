package io.gitlab.jfronny.libjf.entrywidgets.impl.mixin;

import io.gitlab.jfronny.libjf.entrywidgets.api.v0.ResourcePackEntryWidget;
import io.gitlab.jfronny.libjf.entrywidgets.impl.IResourcePackEntry;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.pack.PackListWidget;
import net.minecraft.client.gui.screen.pack.ResourcePackOrganizer;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.util.Identifier;
import org.spongepowered.asm.mixin.*;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

/**
 * Encapsulates the logic for rendering the entry widgets, handling interaction and computing insets.
 */
@Mixin(PackListWidget.ResourcePackEntry.class)
public abstract class RenderWidgets implements IResourcePackEntry {
    @Shadow protected abstract boolean isSelectable();
    @Shadow @Final private ResourcePackOrganizer.Pack pack;
    @Shadow @Final private PackListWidget widget;
    @Unique private int libjf$selected = -1;
    @Unique private Boolean fold;
    @Unique private int libjf$foldTicks = 0;
    @Unique private static final int maxFoldTicks = 10;
    @Unique private int lastWidth = -1;

    @Inject(at = @At("TAIL"), method = "render(Lnet/minecraft/client/gui/DrawContext;IIIIIIIZF)V")
    private void render(DrawContext context, int index, int y, int x, int entryWidth, int entryHeight, int mouseX, int mouseY, boolean hovered, float tickDelta, CallbackInfo info) {
        int prevMargin = 0;
        int deltaX = 3 + (widget.getMaxScrollY() > 0 ? 7 : 0);
        boolean selectable = isSelectable();
        libjf$selected = -1;
        if (libjf$foldTicks == maxFoldTicks) {
            context.drawGuiTexture(RenderLayer::getGuiTextured, Identifier.ofVanilla("transferable_list/unselect"), x + entryWidth - 16 - deltaX, y + entryWidth / 20 - 16, 16, 32);
        } else {
            for (int i = 0; i < ResourcePackEntryWidget.WIDGETS.size(); i++) {
                ResourcePackEntryWidget widget = ResourcePackEntryWidget.WIDGETS.get(i);
                if (!widget.isVisible(pack, selectable)) continue;
                deltaX += Math.max(prevMargin, widget.getXMargin(pack));
                int width = widget.getWidth(pack);
                int height = widget.getHeight(pack, entryHeight);
                int entryX = x + entryWidth - (deltaX + width) * (maxFoldTicks - libjf$foldTicks) / maxFoldTicks;
                int entryY = y + widget.getY(pack, entryHeight);
                deltaX += width;
                boolean widgetHovered = mouseX <= entryX + width && mouseX >= entryX && mouseY <= entryY + height && mouseY >= entryY;
                widget.render(pack, context, entryX, entryY, widgetHovered, tickDelta);
                if (widgetHovered) libjf$selected = i;
                prevMargin = widget.getXMargin(pack);
            }
        }
        if (fold == null) fold = deltaX > 48; // set only once to prevent flickering
        lastWidth = Math.max(10, deltaX);
        libjf$recomputeWrapping();
        if (!fold) return;
        int mouseRange = lastWidth + 40;
        if (mouseX >= x + entryWidth - mouseRange && mouseX <= x + entryWidth && mouseY <= y + entryHeight && mouseY >= y) libjf$foldTicks = Math.max(libjf$foldTicks - 1, 0);
        else libjf$foldTicks = Math.min(libjf$foldTicks + 1, maxFoldTicks);
    }

    @Override
    public int libjf$getWidgetInset() {
        return lastWidth;
    }

    @Inject(at = @At(value = "INVOKE", target = "Lnet/minecraft/client/gui/widget/AlwaysSelectedEntryListWidget$Entry;mouseClicked(DDI)Z"), method = "mouseClicked(DDI)Z", cancellable = true)
    private void mouseClicked(double mouseX, double mouseY, int button, CallbackInfoReturnable<Boolean> info) {
        // Inject before super call
        if (libjf$selected != -1) {
            info.setReturnValue(true);
            ResourcePackEntryWidget.WIDGETS.get(libjf$selected).onClick(pack);
        }
    }
}
