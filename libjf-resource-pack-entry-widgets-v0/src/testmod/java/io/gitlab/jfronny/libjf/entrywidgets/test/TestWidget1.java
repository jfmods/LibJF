package io.gitlab.jfronny.libjf.entrywidgets.test;

import io.gitlab.jfronny.libjf.entrywidgets.api.v0.ResourcePackEntryWidget;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.pack.ResourcePackOrganizer;

public class TestWidget1 implements ResourcePackEntryWidget {
    @Override
    public boolean isVisible(ResourcePackOrganizer.Pack pack, boolean selectable) {
        return pack.isEnabled();
    }

    @Override
    public int getWidth(ResourcePackOrganizer.Pack pack) {
        return (int) ((System.currentTimeMillis() % 5_000) / 100 + 10);
    }

    @Override
    public int getHeight(ResourcePackOrganizer.Pack pack, int rowHeight) {
        return 16;
    }

    @Override
    public int getY(ResourcePackOrganizer.Pack pack, int rowHeight) {
        return 14;
    }

    @Override
    public int getXMargin(ResourcePackOrganizer.Pack pack) {
        return (int) (Math.abs((System.currentTimeMillis() % 1_200) / 50 - 12) + 5);
    }

    @Override
    public void render(ResourcePackOrganizer.Pack pack, DrawContext context, int x, int y, boolean hovered, float tickDelta) {
        context.fill(x, y, x + getWidth(pack), y + getHeight(pack, 20), hovered ? 0x8000FF80 : 0x8000FF00);
    }

    @Override
    public void onClick(ResourcePackOrganizer.Pack pack) {
        System.out.println("Clicked on Widget1");
    }
}
