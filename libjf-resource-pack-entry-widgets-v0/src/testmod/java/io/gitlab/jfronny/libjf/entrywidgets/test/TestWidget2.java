package io.gitlab.jfronny.libjf.entrywidgets.test;

import io.gitlab.jfronny.libjf.entrywidgets.api.v0.ResourcePackEntryWidget;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.pack.ResourcePackOrganizer;

public class TestWidget2 implements ResourcePackEntryWidget {
    @Override
    public boolean isVisible(ResourcePackOrganizer.Pack pack, boolean selectable) {
        return System.currentTimeMillis() % 1_000 < 500;
    }

    @Override
    public int getWidth(ResourcePackOrganizer.Pack pack) {
        return 20;
    }

    @Override
    public int getHeight(ResourcePackOrganizer.Pack pack, int rowHeight) {
        return 20;
    }

    @Override
    public void render(ResourcePackOrganizer.Pack pack, DrawContext context, int x, int y, boolean hovered, float tickDelta) {
        context.fill(x, y, x + getWidth(pack), y + getHeight(pack, 20), hovered ? 0x80FF0080 : 0x80FF0000);
    }

    @Override
    public void onClick(ResourcePackOrganizer.Pack pack) {
        System.out.println("Clicked on Widget2");
    }
}
