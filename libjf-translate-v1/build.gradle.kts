import io.gitlab.jfronny.scripts.*

plugins {
    id("jfmod.module")
}

base {
    archivesName = "libjf-translate-v1"
}

dependencies {
    api(devProject(":libjf-base"))
    api(devProject(":libjf-config-core-v2"))

    compileOnly(libs.commons.serialize.generator.annotations)
    annotationProcessor(libs.commons.serialize.generator)
}
