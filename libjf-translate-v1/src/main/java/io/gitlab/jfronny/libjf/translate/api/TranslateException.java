package io.gitlab.jfronny.libjf.translate.api;

public class TranslateException extends Exception {
    public TranslateException() {
    }

    public TranslateException(String message) {
        super(message);
    }

    public TranslateException(String message, Throwable cause) {
        super(message, cause);
    }

    public TranslateException(Throwable cause) {
        super(cause);
    }
}
