package io.gitlab.jfronny.libjf.translate.impl;

import io.gitlab.jfronny.libjf.translate.api.*;

import java.util.regex.Pattern;

public abstract class AbstractTranslateService<T extends Language> implements TranslateService<T> {
    private static final Pattern WHITE_SPACE = Pattern.compile("\\s*");

    @Override
    public String translate(String textToTranslate, T translateFrom, T translateTo) throws TranslateException {
        if (textToTranslate == null) throw new TranslateException("textToTranslate must not be null");
        if (translateFrom == null) translateFrom = getAutoDetectLang();
        if (translateTo == null) throw new TranslateException("translateTo must not be null");
        if (WHITE_SPACE.matcher(textToTranslate).matches()) return textToTranslate;
        try {
            return performTranslate(textToTranslate, translateFrom, translateTo);
        } catch (TranslateException e) {
            throw e;
        } catch (Throwable e) {
            throw new TranslateException("Could not translate text", e);
        }
    }

    protected abstract T getAutoDetectLang();
    protected abstract String performTranslate(String textToTranslate, T translateFrom, T translateTo) throws Exception;
}
