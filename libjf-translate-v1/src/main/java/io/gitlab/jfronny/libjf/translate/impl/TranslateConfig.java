package io.gitlab.jfronny.libjf.translate.impl;

import io.gitlab.jfronny.libjf.config.api.v2.JfCustomConfig;
import io.gitlab.jfronny.libjf.config.api.v2.dsl.DSL;
import io.gitlab.jfronny.libjf.translate.api.TranslateService;
import io.gitlab.jfronny.libjf.translate.impl.google.GoogleTranslateService;
import io.gitlab.jfronny.libjf.translate.impl.libretranslate.LibreTranslateService;

public class TranslateConfig implements JfCustomConfig {
    public static String translationService = GoogleTranslateService.NAME;
    public static String libreTranslateHost = null;

    public static void ensureValid() {
        if (translationService == null) translationService = GoogleTranslateService.NAME;
        if ("https://translate.argosopentech.com".equals(libreTranslateHost))
            libreTranslateHost = null;
        if (translationService.equals(LibreTranslateService.NAME) && (libreTranslateHost == null || libreTranslateHost.isBlank()))
            translationService = GoogleTranslateService.NAME;
    }

    @Override
    public void register(DSL.Defaulted dsl) {
    }

    static {
        DSL.create("libjf-translate-v1").register(b -> b
                .value("translationService", translationService, TranslateService.INSTALLED_NAMES.toArray(String[]::new), () -> translationService, v -> translationService = v)
                .value("libreTranslateHost", libreTranslateHost, () -> libreTranslateHost, v -> libreTranslateHost = v)
                .addVerifier(TranslateConfig::ensureValid)
        );
    }
}
