package io.gitlab.jfronny.libjf.translate.impl.deeplx;

import io.gitlab.jfronny.libjf.translate.api.Language;

public record DeepLXLanguage(String name, String id) implements Language {
    public static final DeepLXLanguage AUTO_DETECT = new DeepLXLanguage("Detect language", "auto");

    @Override
    public String toString() {
        return name;
    }

    @Override
    public String getDisplayName() {
        return name;
    }

    @Override
    public String getIdentifier() {
        return id;
    }
}
