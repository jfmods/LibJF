package io.gitlab.jfronny.libjf.translate.impl.deeplx;

import io.gitlab.jfronny.commons.http.client.HttpClient;
import io.gitlab.jfronny.commons.serialize.json.*;
import io.gitlab.jfronny.libjf.translate.api.TranslateException;
import io.gitlab.jfronny.libjf.translate.impl.AbstractTranslateService;
import io.gitlab.jfronny.libjf.translate.impl.deeplx.model.*;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.InflaterInputStream;

public class DeepLXService extends AbstractTranslateService<DeepLXLanguage> {
    public static final String NAME = "DeepLX";
    private static DeepLXService INSTANCE;
    private static final Pattern LANGUAGE_KEY = Pattern.compile("selectLang_target_([a-z]+)'] = '([^']+)'");
    private final Map<String, DeepLXLanguage> knownLanguages;

    static {
//        String key = "jdk.httpclient.allowRestrictedHeaders";
//        String res = System.getProperty(key);
//        System.setProperty(key, res == null ? "Connection" : res + ", Connection");
    }

    public static DeepLXService get() throws URISyntaxException, IOException {
        if (INSTANCE == null) {
            INSTANCE = new DeepLXService();
        }
        return INSTANCE;
    }

    private DeepLXService() throws URISyntaxException, IOException {
        Map<String, DeepLXLanguage> knownLanguages = new HashMap<>();
        Matcher matcher = LANGUAGE_KEY.matcher(HttpClient.get("https://www.deepl.com/en/translator").sendString());
        while (matcher.find()) {
            String id = matcher.group(1).toUpperCase(Locale.ROOT);
            String name = matcher.group(2);
            knownLanguages.put(id, new DeepLXLanguage(name, id));
        }
        if (knownLanguages.isEmpty())
            throw new IOException("Could not detect languages, Google likely changed the site. Please inform the maintainer of LibJF");
        this.knownLanguages = Map.copyOf(knownLanguages);
    }

    @Override
    protected DeepLXLanguage getAutoDetectLang() {
        return DeepLXLanguage.AUTO_DETECT;
    }

    private static final JsonTransport TRANSPORT = new JsonTransport() {
        @Override
        public JsonWriter createWriter(Writer target) throws IOException {
            return super.createWriter(target).configureFormatting("", "", ": ", ", ");
        }

        @Override
        public JsonReader createReader(Reader source) {
            return super.createReader(source).setLenient(true);
        }
    };

    @Override
    protected String performTranslate(String textToTranslate, DeepLXLanguage translateFrom, DeepLXLanguage translateTo) throws Exception {
        // Based on https://github.com/OwO-Network/PyDeepLX
        PostData data = new PostData(textToTranslate, translateFrom.id(), translateTo.id(), 3);
        String json = GC_PostData.serializeToString(data, TRANSPORT);
        if ((data.id() + 5) % 29 == 0 || (data.id() + 3) % 13 == 0) {
            json = json.replace("\"method\":\"", "\"method\" : \"");
        } else {
            json = json.replace("\"method\":\"", "\"method\": \"");
        }
        var resp = HttpClient.post("https://www2.deepl.com/jsonrpc")
                .bodyJson(json)
                .header("Accept", "*/*")
                .header("x-app-os-name", "iOS")
                .header("x-app-os-version", "16.3.0")
                .setHeader("Accept-Encoding", "gzip, deflate") //, br")
                .header("x-app-device", "iPhone13,2")
                .userAgent("DeepL-iOS/2.9.1 iOS 16.3.0 (iPhone13,2)")
                .header("x-app-build", "510265")
                .header("x-app-version", "2.9.1")
//                .setHeader("Connection", "keep-alive")
                .ignoreAll()
                .forceHttp11()
                .sendInputStreamResponse();
        try (var is= resp.body();
             var dis = decode(is, resp.headers().allValues("Content-Encoding"));
             var r = new InputStreamReader(dis, StandardCharsets.UTF_8);
             var jr = TRANSPORT.createReader(r)) {
            if (resp.statusCode() == 429) {
                throw new TranslateException("Too many requests, please wait a bit before trying again (received: " + TRANSPORT.write(jr::copyTo) + ")");
            }
            if (resp.statusCode() != 200) {
                throw new TranslateException("Unexpected status code: " + resp.statusCode() + " (received: " + TRANSPORT.write(jr::copyTo) + ")");
            }
            return GC_ResponseData.deserialize(jr).result().texts().getFirst().text();
        }
    }

    private static InputStream decode(InputStream in, List<String> encoding) throws IOException {
        return switch (encoding.isEmpty() ? "identity" : encoding.getFirst().toLowerCase(Locale.ROOT)) {
            case "gzip" -> new GZIPInputStream(in);
            case "deflate" -> new InflaterInputStream(in);
//            case "br" -> new BrotliInputStream(in);
            default -> in;
        };
    }

    @Override
    public DeepLXLanguage detect(String text) throws TranslateException {
        return DeepLXLanguage.AUTO_DETECT;
    }

    @Override
    public DeepLXLanguage parseLang(String lang) {
        return knownLanguages.getOrDefault(lang.toUpperCase(Locale.ROOT), DeepLXLanguage.AUTO_DETECT);
    }

    @Override
    public List<DeepLXLanguage> getAvailableLanguages() {
        List<DeepLXLanguage> langs = new ArrayList<>(knownLanguages.values());
        langs.remove(DeepLXLanguage.AUTO_DETECT);
        return langs;
    }

    @Override
    public String getName() {
        return NAME;
    }
}
