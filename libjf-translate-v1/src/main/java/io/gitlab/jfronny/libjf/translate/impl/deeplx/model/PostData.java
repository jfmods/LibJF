package io.gitlab.jfronny.libjf.translate.impl.deeplx.model;

import io.gitlab.jfronny.commons.serialize.annotations.SerializedName;
import io.gitlab.jfronny.commons.serialize.generator.annotations.GPrefer;
import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;

import java.util.List;
import java.util.Random;

@GSerializable
public record PostData(String jsonrpc, String method, long id, Params params) {
    @GPrefer public PostData {}
    public PostData(String text, String from, String to, int alternatives) {
        this("2.0", "LMT_handle_texts", getId(), new Params(
                new Params.Text(text, alternatives),
                new Params.Lang(from, to)
        ));
    }

    private static long getId() {
        long l = new Random(System.nanoTime()).nextLong(99999) + 8300000;
        return l * 1000;
    }

    @GSerializable
    public record Params(List<Text> texts, String splitting, Lang lang, long timestamp, CommonJobParams commonJobParams) {
        @GPrefer public Params {}
        public Params(Text text, Lang lang) {
            this(List.of(text), "newlines", lang, getTime(text.text), new CommonJobParams());
        }

        private static long getTime(String text) {
            int count = text.chars().filter(ch -> ch == 'i').sum();
            long time = System.nanoTime();
            if (count == 0) return time;
            count += 1;
            return time - (time % count) + count;
        }

        @GSerializable
        public record Text(String text, int requestAlternatives) {
            public Text {
                if (requestAlternatives < 0 || requestAlternatives > 3) {
                    throw new IllegalArgumentException("requestAlternatives must be between 0 and 3");
                }
            }
        }

        @GSerializable
        public record Lang(@SerializedName("source_lang_user_selected") String sourceLangUserSelected, @SerializedName("target_lang") String targetLang) {
        }

        @GSerializable
        public record CommonJobParams(boolean wasSpoken, @SerializedName("transcribe_as") String transcribeAs) {
            @GPrefer public CommonJobParams {}
            public CommonJobParams() {
                this(false, "");
            }
        }
    }
}
