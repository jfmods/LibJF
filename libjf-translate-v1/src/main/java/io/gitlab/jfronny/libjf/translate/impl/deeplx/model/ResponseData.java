package io.gitlab.jfronny.libjf.translate.impl.deeplx.model;

import io.gitlab.jfronny.commons.serialize.annotations.SerializedName;
import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;

import java.util.List;
import java.util.Map;

@GSerializable
public record ResponseData(String jsonrpc, long id, Result result) {
    @GSerializable
    public record Result(List<Text> texts, String lang, @SerializedName("lang_is_confident") boolean langIsConfident, Map<String, Double> detectedLanguages) {
        @GSerializable
        public record Text(List<Alternative> alternatives, String text) {
            @GSerializable
            public record Alternative(String text) {
            }
        }
    }
}
