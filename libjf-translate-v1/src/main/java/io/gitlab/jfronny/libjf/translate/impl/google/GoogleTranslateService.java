package io.gitlab.jfronny.libjf.translate.impl.google;

import io.gitlab.jfronny.commons.http.client.HttpClient;
import io.gitlab.jfronny.libjf.translate.api.TranslateException;
import io.gitlab.jfronny.libjf.translate.impl.AbstractTranslateService;
import org.apache.commons.lang3.StringEscapeUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GoogleTranslateService extends AbstractTranslateService<GoogleTranslateLanguage> {
    public static final String NAME = "Google";
    private static GoogleTranslateService INSTANCE;
    private static final Pattern TRANSLATION_RESULT = Pattern.compile("class=\"result-container\">([^<]*)</div>", Pattern.MULTILINE);
    private static final Pattern LANGUAGE_KEY = Pattern.compile("<div class=\"language-item\"><a href=\"\\./m\\?sl&amp;tl=([a-zA-Z\\-]+)&amp;hl=[a-zA-Z\\-]+\">([^<]+)</a></div>", Pattern.MULTILINE);
    private final Map<String, GoogleTranslateLanguage> knownLanguages;

    public static GoogleTranslateService get() throws URISyntaxException, IOException {
        if (INSTANCE == null) {
            INSTANCE = new GoogleTranslateService();
        }
        return INSTANCE;
    }

    private GoogleTranslateService() throws URISyntaxException, IOException {
        Map<String, GoogleTranslateLanguage> knownLanguages = new HashMap<>();
        Matcher matcher = LANGUAGE_KEY.matcher(get("https://translate.google.com/m?mui=tl"));
        while (matcher.find()) {
            String id = matcher.group(1);
            String name = matcher.group(2);
            knownLanguages.put(id, new GoogleTranslateLanguage(name, id));
        }
        if (knownLanguages.isEmpty())
            throw new IOException("Could not detect languages, Google likely changed the site. Please inform the maintainer of LibJF");
        this.knownLanguages = Map.copyOf(knownLanguages);
    }

    @Override
    protected GoogleTranslateLanguage getAutoDetectLang() {
        return GoogleTranslateLanguage.AUTO_DETECT;
    }

    @Override
    protected String performTranslate(String textToTranslate, GoogleTranslateLanguage translateFrom, GoogleTranslateLanguage translateTo) throws Exception {
        String pageSource;
        try {
            pageSource = getPageSource(textToTranslate, translateFrom.getIdentifier(), translateTo.getIdentifier());
        } catch (Exception e) {
            throw new TranslateException("Could not translate string", e);
        }
        try {
            Matcher matcher = TRANSLATION_RESULT.matcher(pageSource);
            if (matcher.find()) {
                String match = matcher.group(1);
                if (match != null && !match.isEmpty()) {
                    return StringEscapeUtils.unescapeHtml4(match); //TODO use commons-text once that is shipped with Minecraft
                }
            }
            throw new TranslateException("Could not translate \"" + textToTranslate + "\": result page couldn't be parsed");
        } catch (Exception e) {
            try {
                Path p = Files.createTempFile("libjf-translate-pagedump-", ".html").toAbsolutePath();
                Files.writeString(p, pageSource);
                throw new TranslateException("Could not translate string, see dumped page at " + p, e);
            } catch (IOException ioe) {
                throw new TranslateException("Could not translate string and the page could not be dumped", ioe);
            }
        }
    }

    @Override
    public GoogleTranslateLanguage detect(String text) throws TranslateException {
        return GoogleTranslateLanguage.AUTO_DETECT;
    }

    @Override
    public GoogleTranslateLanguage parseLang(String lang) {
        return knownLanguages.getOrDefault(lang, GoogleTranslateLanguage.AUTO_DETECT);
    }

    @Override
    public List<GoogleTranslateLanguage> getAvailableLanguages() {
        List<GoogleTranslateLanguage> langs = new ArrayList<>(knownLanguages.values());
        langs.remove(GoogleTranslateLanguage.AUTO_DETECT);
        return langs;
    }

    @Override
    public String getName() {
        return NAME;
    }

    private static String getPageSource(String textToTranslate, String translateFrom, String translateTo) throws URISyntaxException, IOException {
        if (textToTranslate == null)
            return null;
        String pageUrl = String.format("https://translate.google.com/m?hl=en&sl=%s&tl=%s&ie=UTF-8&prev=_m&q=%s",
                translateFrom, translateTo, URLEncoder.encode(textToTranslate.trim(), StandardCharsets.UTF_8));
        return get(pageUrl);
    }

    private static String get(String url) throws URISyntaxException, IOException {
        return HttpClient.get(url).sendString();
    }

    // Alternative methods to get the page source (kept for reference)
    // The HttpClient here is not the same as the one in libjf-commons, so adjust imports if you want to use them again
//    private static String get(String url) {
//        // An attempt to use the stuff wrapped by HttpClient. Will need to test and (if it works) adjust commons, so it can be used again
//        try {
//            HttpClient client = HttpClient.newBuilder().followRedirects(HttpClient.Redirect.ALWAYS).build();
//            HttpResponse<String> response = client.send(HttpRequest.newBuilder().uri(new URI(url)).header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36").build(), HttpResponse.BodyHandlers.ofString());
//            if (response.statusCode() / 100 != 2) {
//                throw new IOException("Could not get page: " + response.statusCode());
//            }
//            return response.body();
//        } catch (URISyntaxException | IOException | InterruptedException e) {
//            throw new RuntimeException(e);
//        }
//    }

//    private static String get(String url) {
//        // Technically, we should be using HttpClient, but Google Translate doesn't like it for some reason and this mess bypasses that
//        // based on https://github.com/jhy/jsoup/blob/master/src/main/java/org/jsoup/helper/HttpConnection.java
//        try {
//            HttpsURLConnection connection = (HttpsURLConnection) URI.create(url).toURL().openConnection();
//            connection.setRequestMethod("GET");
//            connection.setInstanceFollowRedirects(false);
//            connection.setConnectTimeout(5000);
//            connection.setReadTimeout(2500);
//            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36");
//            connection.connect();
//            if (connection.getResponseCode() / 100 != 2) {
//                String loc = connection.getHeaderField("Location");
//                if (loc == null) loc = "";
//                else loc = " (redirected to " + loc + ")";
//                throw new IOException("Could not get page: " + connection.getResponseCode() + loc);
//            }
//            connection.getRequestMethod();
//            connection.getURL();
//            connection.getResponseCode();
//            connection.getResponseMessage();
//            connection.getContentType();
//            try (BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8))) {
//                StringBuilder sb = new StringBuilder();
//                String line;
//                while ((line = br.readLine()) != null) {
//                    sb.append(line).append('\n');
//                }
//                return sb.toString();
//            }
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
//    }
}
