package io.gitlab.jfronny.libjf.translate.impl.libretranslate;

import io.gitlab.jfronny.commons.http.client.HttpClient;
import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.translate.api.TranslateException;
import io.gitlab.jfronny.libjf.translate.impl.AbstractTranslateService;
import io.gitlab.jfronny.libjf.translate.impl.libretranslate.model.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;

public class LibreTranslateService extends AbstractTranslateService<LibreTranslateLanguage> {
    public static final String NAME = "LibreTranslate";
    private static final Map<String, LibreTranslateService> knownInstances = new HashMap<>();

    public static LibreTranslateService get(String host) throws TranslateException {
        Objects.requireNonNull(host, "host");
        LibreTranslateService lts;
        if (knownInstances.containsKey(host)) {
            lts = knownInstances.get(host);
            if (lts == null) throw new TranslateException("Translate service previously failed to initialize. Not trying again");
            return lts;
        }
        try {
            lts = new LibreTranslateService(host);
        } catch (TranslateException e) {
            knownInstances.put(host, null);
            throw new TranslateException("Could not instantiate translate service", e);
        }
        knownInstances.put(host, lts);
        return lts;
    }

    private final String host;
    private final List<LibreTranslateLanguage> knownLanguages;
    private final Map<String, LibreTranslateLanguage> languageById = new HashMap<>();
    private LibreTranslateService(String host) throws TranslateException {
        if (Objects.requireNonNull(host, "host").endsWith("/")) host = host.substring(0, host.length() - 1);
        this.host = host;
        try (var r = HttpClient.get(host + "/languages").sendReader();
             var jr = LibJf.LENIENT_TRANSPORT.createReader(r)) {
            var langs = jr.arrayByElements(GC_LibreTranslateLanguage::deserialize);
            for (LibreTranslateLanguage lang : langs) languageById.put(lang.code(), lang);
            langs.add(LibreTranslateLanguage.AUTO_DETECT);
            this.knownLanguages = List.copyOf(langs);
        } catch (IOException | URISyntaxException e) {
            throw new TranslateException("Could not get known languages for LibreTranslate backend", e);
        }
    }

    @Override
    protected LibreTranslateLanguage getAutoDetectLang() {
        return LibreTranslateLanguage.AUTO_DETECT;
    }

    @Override
    protected String performTranslate(String textToTranslate, LibreTranslateLanguage translateFrom, LibreTranslateLanguage translateTo) throws Exception {
        try (var r = HttpClient.post(host + "/translate").bodyForm(Map.of(
                    "q", textToTranslate,
                    "source", translateFrom.getIdentifier(),
                    "target", translateTo.getIdentifier()
                 )).sendReader();
             var jr = LibJf.LENIENT_TRANSPORT.createReader(r)) {
            return GC_LibreTranslateResult.deserialize(jr).translatedText();
        }
    }

    @Override
    public LibreTranslateLanguage detect(String text) throws TranslateException {
        List<LibreTranslateDetectResult> result;
        try (var r = HttpClient.post(host + "/detect").bodyForm(Map.of("q", text)).sendReader();
             var jr = LibJf.LENIENT_TRANSPORT.createReader(r)) {
            result = jr.arrayByElements(GC_LibreTranslateDetectResult::deserialize);
        } catch (IOException | URISyntaxException e) {
            throw new TranslateException("Could not detect language", e);
        }
        LibreTranslateDetectResult resCurr = null;
        for (LibreTranslateDetectResult res : result) {
            if (resCurr == null || res.confidence() > resCurr.confidence())
                resCurr = res;
        }
        if (resCurr == null) throw new TranslateException("Could not identify any valid language");
        return parseLang(resCurr.language());
    }

    @Override
    public LibreTranslateLanguage parseLang(String lang) {
        return languageById.get(lang);
    }

    @Override
    public List<LibreTranslateLanguage> getAvailableLanguages() {
        return knownLanguages;
    }

    @Override
    public String getName() {
        return NAME;
    }
}
