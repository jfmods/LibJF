package io.gitlab.jfronny.libjf.translate.impl.libretranslate.model;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;

@GSerializable
public record LibreTranslateDetectResult(float confidence, String language) {
}
