package io.gitlab.jfronny.libjf.translate.impl.libretranslate.model;

import io.gitlab.jfronny.commons.serialize.annotations.Ignore;
import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.libjf.translate.api.Language;

@GSerializable
public record LibreTranslateLanguage(String code, String name) implements Language {
    public static final LibreTranslateLanguage AUTO_DETECT = new LibreTranslateLanguage("auto", "AUTO_DETECT");

    @Override
    public String toString() {
        return name;
    }

    @Override
    @Ignore
    public String getDisplayName() {
        return name;
    }

    @Override
    @Ignore
    public String getIdentifier() {
        return code;
    }
}
