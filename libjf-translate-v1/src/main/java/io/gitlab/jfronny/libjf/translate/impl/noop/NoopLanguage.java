package io.gitlab.jfronny.libjf.translate.impl.noop;

import io.gitlab.jfronny.libjf.translate.api.Language;

public class NoopLanguage implements Language {
    static final NoopLanguage INSTANCE = new NoopLanguage();

    @Override
    public String getDisplayName() {
        return "none";
    }

    @Override
    public String getIdentifier() {
        return "none";
    }
}
