import io.gitlab.jfronny.scripts.*

plugins {
    id("jfmod.module")
}

base {
    archivesName = "libjf-unsafe-v0"
}

dependencies {
    api(devProject(":libjf-base"))
    compileOnly(libs.commons.unsafe) { isTransitive = false }
    shadow(libs.commons.unsafe) { isTransitive = false }
}
