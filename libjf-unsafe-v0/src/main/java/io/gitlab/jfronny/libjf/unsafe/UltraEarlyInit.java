package io.gitlab.jfronny.libjf.unsafe;

public interface UltraEarlyInit {
    void init();
}
