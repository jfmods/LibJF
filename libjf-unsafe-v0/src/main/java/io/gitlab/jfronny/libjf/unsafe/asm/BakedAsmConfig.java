package io.gitlab.jfronny.libjf.unsafe.asm;

import io.gitlab.jfronny.libjf.unsafe.asm.patch.Patch;

import java.util.Set;

public class BakedAsmConfig implements AsmConfig {
    private final Set<String> skipClasses;
    private final Set<Patch> patches;
    private final String source;
    public BakedAsmConfig(AsmConfig config, String source) {
        skipClasses = config.skipClasses();
        patches = config.getPatches();
        this.source = source;
    }

    @Override
    public Set<String> skipClasses() {
        return skipClasses;
    }

    @Override
    public Set<Patch> getPatches() {
        return patches;
    }

    public String getSource() {
        return source;
    }
}
