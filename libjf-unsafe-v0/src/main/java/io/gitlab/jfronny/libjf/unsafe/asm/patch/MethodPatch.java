package io.gitlab.jfronny.libjf.unsafe.asm.patch;

import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;

public interface MethodPatch {
    boolean apply(MethodNode method, ClassNode klazz);
}
