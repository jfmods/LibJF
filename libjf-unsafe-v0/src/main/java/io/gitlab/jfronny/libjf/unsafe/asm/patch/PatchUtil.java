package io.gitlab.jfronny.libjf.unsafe.asm.patch;

import io.gitlab.jfronny.libjf.unsafe.asm.AsmTransformer;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;

public class PatchUtil {
    public static String mapClassNameInternal(String className) {
        return mapClassName(className.replace('/', '.')).replace('.', '/');
    }

    public static String mapClassName(String className) {
        return AsmTransformer.MAPPING_RESOLVER.mapClassName(AsmTransformer.INTERMEDIARY, className);
    }

    public static String mapMethodName(String owner, String name, String descriptor) {
        return AsmTransformer.MAPPING_RESOLVER.mapMethodName(AsmTransformer.INTERMEDIARY, owner, name, descriptor);
    }

    public static String mapDescriptor(String descriptor) {
        return mapDescriptor(descriptor, 0, descriptor.length());
    }

    // Adapted from mapping-io
    public static String mapDescriptor(String desc, int start, int end) {
        StringBuilder ret = null;
        int searchStart = start;
        int clsStart;

        while ((clsStart = desc.indexOf('L', searchStart)) >= 0) {
            int clsEnd = desc.indexOf(';', clsStart + 1);
            if (clsEnd < 0) throw new IllegalArgumentException();

            String cls = desc.substring(clsStart + 1, clsEnd);
            String mappedCls = PatchUtil.mapClassNameInternal(cls);

            if (ret == null) ret = new StringBuilder(end - start);

            ret.append(desc, start, clsStart + 1);
            ret.append(mappedCls);
            start = clsEnd;

            searchStart = clsEnd + 1;
        }

        if (ret == null) return desc.substring(start, end);

        ret.append(desc, start, end);

        return ret.toString();
    }

    public static void redirectReturn(MethodNode method, String targetClass, String hookClass, String targetMethod, String targetType, String... extraParamTypes) {
        for (AbstractInsnNode node : method.instructions) {
            if (node.getOpcode() == Opcodes.ARETURN
                    || node.getOpcode() == Opcodes.IRETURN
                    || node.getOpcode() == Opcodes.DRETURN
                    || node.getOpcode() == Opcodes.FRETURN
                    || node.getOpcode() == Opcodes.LRETURN) {
                method.instructions.insertBefore(node, buildParamPassingInvoker(targetClass, hookClass, targetMethod, targetType, targetType, extraParamTypes));
            }
        }
    }

    public static void redirectExceptions(MethodNode method, String targetClass, String hookClass, String exceptionType, String returnTypeNormal, String targetMethod, String... extraParamTypes) {
        LabelNode start = new LabelNode();
        LabelNode end = new LabelNode();
        LabelNode handler = new LabelNode();
        method.instructions.insertBefore(method.instructions.getFirst(), start);
        method.instructions.insertBefore(method.instructions.getLast(), end);
        method.instructions.add(handler);
        method.instructions.add(buildParamPassingInvoker(targetClass,
                hookClass,
                targetMethod,
                "L" + exceptionType + ";",
                "L" + returnTypeNormal + ";",
                extraParamTypes));
        method.instructions.add(new InsnNode(Opcodes.ARETURN));
        method.tryCatchBlocks.add(new TryCatchBlockNode(start, end, handler, exceptionType));
    }

    public static InsnList buildParamPassingInvoker(String targetClass, String hookClass, String targetMethod, String inType, String outType, String... extraParamTypes) {
        InsnList instructions = new InsnList();
        instructions.add(new VarInsnNode(Opcodes.ALOAD, 0));
        StringBuilder descriptor = new StringBuilder("(");
        if (inType != null) descriptor.append(mapDescriptor(inType));
        descriptor.append('L').append(mapClassNameInternal(targetClass)).append(';');
        for (int i = 0; i < extraParamTypes.length; i++) {
            String param = extraParamTypes[i];
            instructions.add(new VarInsnNode(switch (param) {
                case "I" -> Opcodes.ILOAD;
                case "D" -> Opcodes.DLOAD;
                case "F" -> Opcodes.FLOAD;
                case "L" -> Opcodes.LLOAD;
                default -> Opcodes.ALOAD;
            }, i + 1));
            while (param.charAt(0) == '[') {
                descriptor.append('[');
                param = param.substring(1);
            }
            if (param.length() == 1) descriptor.append(param);
            else descriptor.append('L').append(mapClassNameInternal(param)).append(';');
        }
        descriptor.append(")").append(mapDescriptor(outType));
        instructions.add(new MethodInsnNode(Opcodes.INVOKESTATIC, hookClass, targetMethod, descriptor.toString()));
        return instructions;
    }
}
