package io.gitlab.jfronny.libjf.unsafe.test;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.unsafe.UltraEarlyInit;

public class UnsafeEntryTest implements UltraEarlyInit {
    @Override
    public void init() {
        LibJf.LOGGER.info("Successfully executed code before that should be possible\n" +
                "'||'  '|' '||    .                   '||''''|                  '||           \n" +
                " ||    |   ||  .||.  ... ..   ....    ||  .     ....   ... ..   ||  .... ... \n" +
                " ||    |   ||   ||    ||' '' '' .||   ||''|    '' .||   ||' ''  ||   '|.  |  \n" +
                " ||    |   ||   ||    ||     .|' ||   ||       .|' ||   ||      ||    '|.|   \n" +
                "  '|..'   .||.  '|.' .||.    '|..'|' .||.....| '|..'|' .||.    .||.    '|    \n" +
                "                                                                    .. |     \n" +
                "                                                                     ''      ");
    }
}
