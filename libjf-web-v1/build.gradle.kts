import io.gitlab.jfronny.scripts.*

plugins {
    id("jfmod.module")
}

base {
    archivesName = "libjf-web-v1"
}

dependencies {
    api(devProject(":libjf-base"))
    api(devProject(":libjf-config-core-v2"))
    api(devProject(":libjf-mainhttp-v0"))
    include(modImplementation("net.fabricmc.fabric-api:fabric-command-api-v2")!!)

    annotationProcessor(project(":libjf-config-compiler-plugin-v2"))
}

tasks.compileJava {
    options.compilerArgs.add("-AmodId=" + base.archivesName.get())
}
