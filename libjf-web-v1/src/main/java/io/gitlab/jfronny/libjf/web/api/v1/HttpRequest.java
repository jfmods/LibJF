package io.gitlab.jfronny.libjf.web.api.v1;

import io.gitlab.jfronny.libjf.web.impl.util.HttpRequestImpl;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Set;

public interface HttpRequest {
    static HttpRequest read(InputStream in) throws IOException {
        return HttpRequestImpl.read(in);
    }

    default HttpResponse createResponse(HttpStatusCode statusCode) {
        return HttpResponse.create(statusCode);
    }

    String getMethod();
    String getAddress();
    String getVersion();
    Map<String, Set<String>> getHeader();
    Map<String, Set<String>> getLowercaseHeader();
    Set<String> getHeader(String key);
    Set<String> getLowercaseHeader(String key);
    String getPath();
    Map<String, String> getQuery();
    String getQueryString();
    InputStream getData();
}
