package io.gitlab.jfronny.libjf.web.api.v1;

import io.gitlab.jfronny.libjf.web.impl.util.HttpResponseImpl;

import java.io.*;
import java.util.Map;
import java.util.Set;

public interface HttpResponse extends Closeable {
    static HttpResponse create(HttpStatusCode statusCode) {
        return new HttpResponseImpl(statusCode);
    }

    HttpResponse addHeader(String key, String value);
    HttpResponse removeHeader(String key, String value);
    HttpResponse setData(InputStream data);
    HttpResponse setData(String data);
    void write(OutputStream out) throws IOException;

    HttpStatusCode getStatusCode();
    String getVersion();
    Map<String, Set<String>> getHeader();
    Set<String> getHeader(String key);
}
