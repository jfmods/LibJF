package io.gitlab.jfronny.libjf.web.api.v1;

import org.jetbrains.annotations.Nullable;

import java.util.*;

public record PathSegment(String segment, @Nullable PathSegment next) {
    public PathSegment(String segment, @Nullable PathSegment next) {
        if (segment.isEmpty() || segment.equals(".") || segment.contains("/")) {
            PathSegment from = Objects.requireNonNull(of(segment, next));
            this.segment = from.segment;
            this.next = from.next;
        } else {
            this.segment = segment;
            this.next = next;
        }
    }

    public PathSegment(String path) {
        this(path, null);
    }

    @Override
    public String toString() {
        return next == null ? segment : segment + "/" + next;
    }

    public PathSegment concat(@Nullable PathSegment seg) {
        return seg == null ? this : new PathSegment(segment, next == null ? seg : next.concat(seg));
    }

    public static @Nullable PathSegment concat(@Nullable PathSegment seg1, @Nullable PathSegment seg2) {
        return seg1 == null ? seg2 : seg1.concat(seg2);
    }

    public static @Nullable PathSegment of(String path) {
        return of(path, null);
    }

    private static @Nullable PathSegment of(String path, PathSegment next) {
        Deque<String> segmentStack = new LinkedList<>();
        for (String s : path.split("/")) {
            if (s.isEmpty() || s.equals(".")) continue;
            if (s.equals("..")) segmentStack.pop();
            segmentStack.push(s);
        }
        for (String s : segmentStack) next = new PathSegment(s, next);
        return next;
    }
}
