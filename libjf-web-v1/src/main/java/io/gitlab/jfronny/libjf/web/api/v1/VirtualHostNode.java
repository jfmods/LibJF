package io.gitlab.jfronny.libjf.web.api.v1;

import org.jetbrains.annotations.Nullable;

import java.io.IOException;

@FunctionalInterface
public interface VirtualHostNode {
    HttpResponse handle(HttpRequest request, @Nullable PathSegment path) throws IOException;
}
