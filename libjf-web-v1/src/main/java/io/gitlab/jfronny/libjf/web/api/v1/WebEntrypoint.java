package io.gitlab.jfronny.libjf.web.api.v1;

public interface WebEntrypoint {
    void register(WebServer api);
}
