package io.gitlab.jfronny.libjf.web.api.v1;

import io.gitlab.jfronny.libjf.web.impl.JfWeb;

import java.io.IOException;
import java.nio.file.Path;

public interface WebServer {
    String register(PathSegment path, VirtualHostNode provider);
    default String register(PathSegment path, HttpRequestHandler provider) {
        return register(path, provider.asHostNode());
    }
    String registerFile(PathSegment path, Path file, boolean readOnSend) throws IOException;
    String registerFile(PathSegment path, byte[] data, String contentType);
    String registerDir(PathSegment path, Path dir, boolean readOnSend) throws IOException;
    String getServerRoot();
    void onStart(Runnable listener);
    void onStop(Runnable listener);
    void stop();
    void queueRestart(Runnable callback);
    boolean isActive();

    static WebServer getInstance() {
        return JfWeb.SERVER;
    }
}
