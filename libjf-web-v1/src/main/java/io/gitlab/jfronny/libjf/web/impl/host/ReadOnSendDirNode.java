package io.gitlab.jfronny.libjf.web.impl.host;

import io.gitlab.jfronny.libjf.web.api.v1.*;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

public record ReadOnSendDirNode(Path fsPath) implements VirtualHostNode {
    @Override
    public HttpResponse handle(HttpRequest request, @Nullable PathSegment path) throws IOException {
        return handle(request, path, fsPath);
    }

    private static HttpResponse handle(HttpRequest request, @Nullable PathSegment path, Path fsPath) throws IOException {
        if (path == null) {
            if (Files.isRegularFile(fsPath) && Files.isReadable(fsPath)) {
                HttpResponse resp = request.createResponse(HttpStatusCode.OK);
                resp.addHeader("Content-Type", Files.probeContentType(fsPath));
                resp.addHeader("Content-Length", String.valueOf(Files.size(fsPath)));
                resp.setData(Files.newInputStream(fsPath));
                return resp;
            } else return request.createResponse(HttpStatusCode.NOT_FOUND);
        } else {
            if (!Files.isDirectory(fsPath)) return request.createResponse(HttpStatusCode.NOT_FOUND);
            try (Stream<Path> s = Files.list(fsPath)) {
                for (Path sub : s.toList()) {
                    if (path.segment().equals(sub.getFileName().toString())) {
                        return handle(request, path.next(), sub);
                    }
                }
            }
            return request.createResponse(HttpStatusCode.NOT_FOUND);
        }
    }
}
