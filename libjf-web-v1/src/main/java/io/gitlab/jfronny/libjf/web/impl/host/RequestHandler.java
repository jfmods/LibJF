package io.gitlab.jfronny.libjf.web.impl.host;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.web.api.v1.*;
import io.gitlab.jfronny.libjf.web.api.v1.HttpRequestHandler;

public class RequestHandler extends VirtualHostBranch implements HttpRequestHandler {
    @Override
    public HttpResponse handle(HttpRequest request) {
        HttpResponse resp;
        try {
            PathSegment path = PathSegment.of(request.getPath());
            if (path == null) path = new PathSegment("index.html");
            resp = handle(request, path);
        } catch (Throwable e) {
            LibJf.LOGGER.error("Caught error while sending", e);
            resp = request.createResponse(HttpStatusCode.INTERNAL_SERVER_ERROR);
        }
        if (resp.getHeader("Cache-Control").isEmpty())
            resp.addHeader("Cache-Control", "no-cache");
        return resp;
    }

    public void clear() {
        children.clear();
    }
}
