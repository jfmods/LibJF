package io.gitlab.jfronny.libjf.web.impl.host;

import io.gitlab.jfronny.libjf.web.api.v1.*;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.*;

public class VirtualHostBranch implements VirtualHostNode {
    protected final Map<String, VirtualHostNode> children = new HashMap<>();
    protected HttpRequestHandler content;

    @Override
    public HttpResponse handle(HttpRequest request, @Nullable PathSegment path) throws IOException {
        if (path == null) {
            if (content != null) return content.handle(request);
            else return request.createResponse(HttpStatusCode.NOT_FOUND);
        } else {
            VirtualHostNode child = children.get(path.segment());
            if (child == null) return request.createResponse(HttpStatusCode.NOT_FOUND);
            else return child.handle(request, path.next());
        }
    }

    public void register(PathSegment path, VirtualHostNode node) {
        Objects.requireNonNull(path);
        if (path.next() == null) {
            if (children.containsKey(path.segment())) {
                VirtualHostNode childNode = children.get(path.segment());
                if (node instanceof VirtualHostBranch next
                        && childNode instanceof VirtualHostBranch source) {
                    if (next.content != null) source.setContent(next.content);
                    next.children.forEach((k, v) -> source.register(PathSegment.of(k), v));
                } else throw new UnsupportedOperationException("VirtualHostNode already exists for path and merging is unsupported for " + childNode.getClass() + " and " + node.getClass());
            } else children.put(path.segment(), node);
        } else {
            VirtualHostNode child = children.computeIfAbsent(path.segment(), s -> new VirtualHostBranch());
            if (child instanceof VirtualHostBranch source) source.register(path.next(), node);
            else throw new UnsupportedOperationException("Cannot register VirtualHostNode to non-branch VirtualHostNode " + child.getClass());
        }
    }

    public void setContent(HttpRequestHandler content) {
        if (this.content != null) throw new UnsupportedOperationException("Content already set for virtual host node");
        this.content = Objects.requireNonNull(content);
    }
}
