package io.gitlab.jfronny.libjf.web.impl.util;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;

public class RunnableEvent {
    public static Event<Runnable> create() {
        return EventFactory.createArrayBacked(Runnable.class, listeners -> () -> {
            for (Runnable listener : listeners) listener.run();
        });
    }
}
