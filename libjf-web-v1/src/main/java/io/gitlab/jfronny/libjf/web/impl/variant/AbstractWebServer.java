package io.gitlab.jfronny.libjf.web.impl.variant;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.web.api.v1.*;
import io.gitlab.jfronny.libjf.web.impl.JfWebConfig;
import io.gitlab.jfronny.libjf.web.impl.host.*;
import io.gitlab.jfronny.libjf.web.impl.util.RunnableEvent;
import io.gitlab.jfronny.libjf.web.impl.util.WebPaths;
import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.loader.api.FabricLoader;
import org.jetbrains.annotations.ApiStatus;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

public abstract class AbstractWebServer implements WebServer {
    protected final RequestHandler handler;
    private final DefaultFileHost dfh = new DefaultFileHost();

    protected AbstractWebServer(RequestHandler handler) {
        this.handler = handler;
    }

    @ApiStatus.Internal
    public RequestHandler getHandler() {
        return handler;
    }

    @Override
    public String register(PathSegment path, VirtualHostNode provider) {
        handler.register(path, provider);
        return toUrl(path);
    }

    @Override
    public String registerFile(PathSegment path, Path file, boolean readOnSend) throws IOException {
        if (readOnSend) {
            if (!Files.exists(file)) throw new FileNotFoundException();
            return register(path, s -> {
                HttpResponse resp = HttpResponse.create(HttpStatusCode.OK);
                resp.addHeader("Content-Type", Files.probeContentType(file));
                resp.addHeader("Content-Length", String.valueOf(Files.size(file)));
                FileInputStream fs = new FileInputStream(file.toFile());
                resp.setData(fs);
                return resp;
            });
        } else {
            return registerFile(path, Files.readAllBytes(file), Files.probeContentType(file));
        }
    }

    @Override
    public String registerFile(PathSegment path, byte[] data, String contentType) {
        return register(path, s -> {
            HttpResponse resp = HttpResponse.create(HttpStatusCode.OK);
            resp.addHeader("Content-Type", contentType);
            resp.addHeader("Content-Length", String.valueOf(data.length));
            ByteArrayInputStream fs = new ByteArrayInputStream(data);
            resp.setData(fs);
            return resp;
        });
    }

    @Override
    public String registerDir(PathSegment path, Path dir, boolean readOnSend) throws IOException {
        if (readOnSend) return register(path, new ReadOnSendDirNode(dir));
        else {
            try (Stream<Path> contentPath = Files.walk(dir)) {
                contentPath.filter(Files::isRegularFile)
                        .filter(Files::isReadable)
                        .forEach(s -> {
                            Path p = dir.toAbsolutePath().normalize().relativize(s.toAbsolutePath().normalize());
                            PathSegment subPath = path.concat(PathSegment.of(p.toString()));
                            try {
                                registerFile(subPath, s, false);
                            } catch (IOException e) {
                                LibJf.LOGGER.error("Could not register static file", e);
                            }
                        });
            }
            return toUrl(path);
        }
    }

    private String toUrl(PathSegment path) {
        return WebPaths.concat(getServerRoot(), path.toString());
    }

    private final Event<Runnable> onStart = RunnableEvent.create();
    @Override
    public void onStart(Runnable listener) {
        onStart.register(listener);
    }

    protected void emitStart() {
        onStart.invoker().run();
    }

    private final Event<Runnable> onStop = RunnableEvent.create();
    @Override
    public void onStop(Runnable listener) {
        onStop.register(listener);
    }

    protected void emitStop() {
        onStop.invoker().run();
    }

    protected void performRegistrations() {
        if (JfWebConfig.enableFileHost) dfh.register(this);
        FabricLoader.getInstance().getEntrypointContainers(LibJf.MOD_ID + ":web", WebEntrypoint.class).forEach(entrypoint -> {
            WebEntrypoint init = entrypoint.getEntrypoint();
            init.register(this);
        });
    }

    protected String getServerRoot(int hostedPort) {
        return WebPaths.getHttp(JfWebConfig.serverIp) + ":"
                + (JfWebConfig.portOverride != -1 ? JfWebConfig.portOverride : hostedPort);
    }
}
