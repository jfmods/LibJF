package io.gitlab.jfronny.libjf.web.impl.variant.hosted;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.web.impl.*;
import io.gitlab.jfronny.libjf.web.impl.host.RequestHandler;
import io.gitlab.jfronny.libjf.web.impl.variant.AbstractWebServer;

public class HostedWebServer extends AbstractWebServer {
    private HttpServer server = null;
    private final int port;
    private final int maxConnections;

    public HostedWebServer(RequestHandler handler, int port, int maxConnections) {
        super(handler);
        this.port = port;
        this.maxConnections = maxConnections;
    }

    @Override
    public String getServerRoot() {
        return getServerRoot(server.getPort());
    }

    @Override
    public synchronized void stop() {
        emitStop();
        if (server != null) {
            try {
                server.close();
                server.join();
            }
            catch (InterruptedException e) {
                //It is most likely already dead
            }
        }
    }

    @Override
    public void queueRestart(Runnable callback) {
        int tmpPort = port;
        if (server != null) {
            tmpPort = server.getPort();
            stop();
        } else if (tmpPort == 0) tmpPort = JfWebConfig.findAvailablePort();
        handler.clear();
        server = new HttpServer(null, tmpPort, maxConnections, handler, this::performRegistrations);
        server.start();
        try {
            server.waitUntilReady();
        } catch (InterruptedException e) {
            stop();
            LibJf.LOGGER.error("Server could not be readied", e);
        }
        emitStart();
        callback.run();
    }

    @Override
    public boolean isActive() {
        return server != null && server.isAlive();
    }
}
