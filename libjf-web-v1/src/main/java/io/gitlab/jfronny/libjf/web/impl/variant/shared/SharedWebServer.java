package io.gitlab.jfronny.libjf.web.impl.variant.shared;

import io.gitlab.jfronny.libjf.mainhttp.api.v0.ServerState;
import io.gitlab.jfronny.libjf.web.impl.host.RequestHandler;
import io.gitlab.jfronny.libjf.web.impl.variant.AbstractWebServer;

import java.util.*;

public class SharedWebServer extends AbstractWebServer {
    public static final Set<Runnable> onActive = new LinkedHashSet<>();

    public static void emitActive() {
        for (Iterator<Runnable> iterator = onActive.iterator(); iterator.hasNext(); iterator.remove()) {
            Runnable runnable = iterator.next();
            runnable.run();
        }
    }

    public SharedWebServer(RequestHandler handler) {
        super(handler);
    }

    @Override
    public String getServerRoot() {
        if (!ServerState.isActive()) throw new UnsupportedOperationException("Attempted to get server root on unhosted server");
        else return getServerRoot(ServerState.getPort());
    }

    @Override
    public void stop() {
        throw new UnsupportedOperationException("A shared server cannot be stopped");
    }

    @Override
    public void queueRestart(Runnable callback) {
        onActive.add(() -> {
            emitStop();
            handler.clear();
            performRegistrations();
            emitStart();
            callback.run();
        });
        if (isActive()) emitActive();
    }

    @Override
    public boolean isActive() {
        return ServerState.isActive();
    }
}
