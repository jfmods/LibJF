package io.gitlab.jfronny.libjf.web.test;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.web.api.v1.*;
import net.fabricmc.loader.api.FabricLoader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class WebTest implements WebEntrypoint {
    @Override
    public void register(WebServer api) {
        Path sourcePath = FabricLoader.getInstance()
                .getModContainer("libjf-web-v1-testmod")
                .flatMap(modContainer -> modContainer.findPath("test.html"))
                .orElseThrow();
        LibJf.LOGGER.info(api.register(PathSegment.of("test/0.html"), request -> request.createResponse(HttpStatusCode.OK).setData(Files.readString(sourcePath))));
        try {
            LibJf.LOGGER.info(api.registerFile(PathSegment.of("test/1.html"), sourcePath, false));
            LibJf.LOGGER.info(api.registerFile(PathSegment.of("test/2.html"), sourcePath, true));
        } catch (IOException e) {
            throw new RuntimeException("Could not register hosted files", e);
        }
    }
}
