pluginManagement {
    repositories {
        maven("https://maven.frohnmeyer-wds.de/mirrors")
        maven("https://maven.neoforged.net/releases")
        gradlePluginPortal()
    }
    plugins {
        id("jfmod") version("1.7-SNAPSHOT")
        id("jfmod.module")
    }
}

rootProject.name = "libjf"

include("libjf-base")

include("libjf-config-core-v2")
include("libjf-config-commands")
include("libjf-config-network-v0")
include("libjf-config-ui-tiny")
include("libjf-config-compiler-plugin-v2")

include("libjf-data-v0")
include("libjf-data-manipulation-v0")

include("libjf-devutil")

include("libjf-translate-v1")

include("libjf-unsafe-v0")

include("libjf-mainhttp-v0")
include("libjf-web-v1")

include("libjf-resource-pack-entry-widgets-v0")

include("libjf-bom")
include("libjf-catalog")
